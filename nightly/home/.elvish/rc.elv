# Location: ~/.config/elvish/lib
use init
use interactive
use ghost
use utils
use str

ghost:secret

fn n { e:nnn }
fn t { e:tmux }
fn pj {|@a| e:pijul $@a}
fn pi {|@a| $E:PY_BIN -m pip install $@a}
fn se { exec elvish }
fn e {|@a| e:nvim $@a}
fn ng { utils:skrg ~/m/vim }
fn nf { utils:skopen ~/m/vim }
fn vg { sh ~/.config/nnn/plugins/skrg }
fn vf { sh ~/.config/nnn/plugins/skopen }
fn yx {|a| e:youtube-dl -x --no-playlist $a}
fn yv {|a| e:youtube-dl --no-playlist $a}
fn yp {|a| e:youtube-dl $a}
fn nc { e:ncmpcpp }
fn red { e:redshift -O 4200K }
fn ac { e:nvim ~/.config/alacritty/alacritty.yml }
fn tsh { e:nvim ~/.ssh/config }
fn ts { e:eval ~/.elvish/rc.elv }
fn th { e:nvim ~/m/hax/elvish/hash.elv }
fn hh { e:elvish ~/m/hax/elvish/hash.elv }
fn np { e:nvim ~/.config/nvim/profiles.lua }
fn ta { e:nvim ~/.elvish/rc.elv }
fn ti { e:nvim ~/.config/elvish/lib/init.elv }
fn tn { e:nnn ~/.config/elvish/lib/ }
fn te { e:nvim ~/m/vim/elvish.org }
fn td { e:nvim ~/m/vim/org/todo.org }
fn l {|@a| e:lsd $@a }
fn la {|@a| e:lsd -la $@a }
fn pns { pnpm start }
fn pna { |@deps| pnpm add $deps}
fn gww { cd (git worktree list | awk "{print $1}" | sk) }
fn gwl { e:git worktree list }
fn gwa { |br| git worktree add $br }
fn gtl { e:git log --oneline --decorate --graph --all }
fn gts { e:git status }
fn gtr { e:git remote -v }
fn gtb { e:git branch -vva }
fn gta { e:git add . }
fn gtc {|msg| e:git commit -m $msg }
fn lab {|repo| e:git clone "git@gitlab.com:icew4ll/"$repo }
fn gtp { e:git push}
fn ree { reflex -r ".elv" -- elvish "{}" }
fn reb { reflex -r ".sh" -- bash "{}" }
fn sc { utils:sk_bash ~/.screenlayout }
fn snip { nvim $E:HOME/.config/nvim_profiles/main/lua/modules/snips.lua }
fn vm { bash ~/m/hax/bash/qemu/spice.sh }

fn sv {
  bash ~/m/hax/bash/ssh/root.sh
}

fn ssf {
  var dir = /mnt/sshfs
  # fusermount -u $dir >/dev/null 2>&1
  sshfs -o reconnect arch:/ $dir
  # sshfs -o reconnect root:/ $dir
  cd $dir
}

fn acp {
  # var dir = /mnt/sshfs
  var file = ~/m/hax/bash/arch/init.sh
  # cp $file $dir/root
  # scp -r ./* 'root@someipaddy.com:/var/www/html/epic'
  scp -r $file 'arch:/root'
}

fn wow {
  var ex = $E:HOME"/Downloads/World of Warcraft 5.4.8/Wow-64.exe"
  bash ~/.screenlayout/wow.sh
  echo $E:STORM | xclip -selection c
  wine $ex
  bash ~/.screenlayout/main.sh
}

fn emoji {
emoji-fzf preview | fzf --preview 'emoji-fzf get --name {1}' | awk '{print $1 }' | emoji-fzf get
}

fn vb {
  var dir = $E:HOME"/m/hax/elvish/vbox"
  fd . $dir -x basename | sk
}

fn mks {
  |file|
	echo "#!/bin/bash" > $file.sh
	chmod +x $file.sh
}

fn sch {
  shotgun (str:split " " (hacksaw -f "-i %i -g %g"))
  # shotgun (str:split " " (hacksaw -f "-i %i -g %g")) /tmp/snip
}

fn scl {
  shotgun (str:split " " (slop -f "-i %i -g %g"))
}

fn giti {
  |repo|
  git init --initial-branch=main
  git remote add origin "git@gitlab.com:icew4ll/"$repo".git"
  git add .
  git commit -m "Initial commit"
  git push -u origin main
}

fn rcs {
  utils:yellow "Saving..."
  var dir = Pictures
  var local = ~/$dir
  var remote = mega:$dir
  rclone sync $local $remote
  utils:green "Save Complete"
}

fn rcl {
  utils:yellow "Loading..."
  var dir = Pictures
  var local = ~/$dir
  var remote = mega:$dir
  rclone sync $remote $local
  utils:green "Load Complete"
}

fn upn {
  cd ~/m/py/tool
  pipenv run python nerd.py repo nvim
}

fn ups {
  cd ~/m/py/tool
  pipenv run python nerd.py sys
}

fn save {
  cd ~/m/py/tool
  pipenv run python nerd.py save
  rcs
}

fn haz { curl icanhazip.com }

fn vpn {
  var dir = ~/.config/openvpn
  var file = $dir/us_seattle.ovpn
  curl icanhazip.com
  sudo openvpn $file
}

fn vpn {
  var dir = ~/.config/openvpn
  var file = $dir/us_seattle.ovpn
  curl icanhazip.com
  sudo openvpn $file
}

fn bare {
  var bare = ".bare"
  git init --initial-branch=main --bare $bare
  echo "gitdir: ./"$bare > .git
  var name = "main"
  git clone $bare $name
  cd $name
}

fn tt {
  # fd . $E:BOOKMARKS_DIR -x readlink -f
  # fd . $E:BOOKMARKS_DIR -x elvish -c "echo {}"
  # fd . $E:BOOKMARKS_DIR -x elvish -c 'printf "%10s -> %s\n" (basename {}) (readlink -f {})'
  elvish ~/m/hax/elvish/hash.elv
  var dir = (fd . $E:BOOKMARKS_DIR -x elvish -c 'printf "%10s -> %s\n" (basename {}) (readlink -f {})' | sk | awk '{print $3}')
  if ?(test -d $dir) {
    cd $dir
  }
}

# Arrange for [alt-e] and [alt-v] to edit the current command buffer using my
# prefered external editor.
set edit:insert:binding[Alt-e] = $interactive:external-edit-command~
set edit:insert:binding[Alt-v] = $interactive:external-edit-command~
