# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

checker() {
	file=$1
	[[ -f $file ]] && source $file
}

arr=(
	~/.config/zsh/nnn.sh
	~/.config/zsh/basic.sh
	~/.config/zsh/alias.sh
	~/.config/zsh/fzf.sh
	~/.config/zsh/key-bindings.zsh
	~/.config/zsh/completion.zsh
	~/.ghost.sh
  ~/.p10k.zsh
	~/.config/zsh/func.sh
)

### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

checker "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit
### End of Zinit's installer chunk
zinit ice depth=1; zinit light romkatv/powerlevel10k
# zinit ice lucit wait"0" as"program" from"gh-r" pick"bit"
# zinit light "chriswalz/bit"

zinit light-mode for \
	zsh-users/zsh-autosuggestions \
	softmoth/zsh-vim-mode \
	wfxr/forgit

for file in "${arr[@]}"; do
  checker $file
done

# skywind3000/z.lua
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
# unalias zi
eval "$(zoxide init zsh)"
checker ~/.fzf.zsh
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /home/ice/bin/build/.go/bin/bit bit
