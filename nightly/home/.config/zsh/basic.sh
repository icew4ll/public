#!/bin/bash

# rate increase
if [ -x "$(command -v xset)" ]; then

	# xset r rate 300 50
	xset r rate 200 100
fi

# vim bindings
bindkey -v

export MANPAGER='nvim +Man!'
export NEXT_TELEMETRY_DEBUG=1
export XDG_USER_CONFIG_DIR=$HOME/.config
export XDG_CONFIG_HOME=$HOME/.config

# PATH SOURCE TOP
export PATH="/usr/lib/freecad-daily/lib:$HOME/.cargo/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:$HOME/.local/bin:$HOME/n/bin:$GOPATH/bin:$GOROOT/bin:$N_PREFIX/bin:$HOME/.yarn/bin:/snap/bin:$HOME/.pyenv/bin:/opt:$HOME/v:$HOME/.emacs.d/bin:$HOME/bin:$HOME/build/.go/bin:$HOME/.volta/bin:$HOME/.pyenv/versions/3.9.1/bin:/usr/sbin"

# pyenv
export PYENV_ROOT="$HOME/.pyenv"

# history
export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
setopt appendhistory

e=$(which nvim)
export EDITOR="$e"
export VISUAL_EDITOR="$e"
export VISUAL="$e"
# xdg-mime default vim.desktop text/plain
export COLORTERM=truecolor

# GO
build=~/bin/build
# mkdir -p $build/.go/{bin,src,pkg}
export GOPATH=$build/.go
export GOBIN=~/bin
export GOROOT=$build/go

# DOCKER
export DOCKER_ID_USER=icew4ll

# RUST
# export RUST_SRC_PATH="$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src"
# export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/library/std/src"
export RUST_SRC_PATH="$(rustc --print sysroot)/lib/rustlib/src/rust/library"

export N_PREFIX="$HOME/n"
[[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin" # Added by n-install (see http://git.io/n-install-repo).
export DISABLE_MAGIC_FUNCTIONS=true
export TERM='xterm-256color'
# [[ $TMUX = "" ]] && export TERM="xterm-256color"
# export TERM='screen-256color'

# export VOLTA_HOME="$HOME/.volta"
# export PATH="$VOLTA_HOME/bin:$PATH"
# autoload -U +X bashcompinit && bashcompinit
# complete -o nospace -C /home/ice/build/.go/bin/bitcomplete bit
