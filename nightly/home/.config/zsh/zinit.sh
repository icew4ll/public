#!/bin/bash

### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    tj/git-extras \
    softmoth/zsh-vim-mode \
    skywind3000/z.lua \
    wfxr/forgit \
    zinit-zsh/z-a-rust \
    zinit-zsh/z-a-as-monitor \
    zinit-zsh/z-a-patch-dl \
    zinit-zsh/z-a-bin-gem-node

# Load OMZ Git library
zinit snippet OMZL::git.zsh

# Load Git plugin from OMZ
zinit snippet OMZP::git
zinit cdclear -q # <- forget completions provided up to this moment

setopt promptsubst

# Load theme from OMZ
zinit snippet OMZT::amuse
#zinit snippet OMZT::avit
#zinit snippet OMZT::gallois
#zinit snippet OMZT::gnzh
#zinit snippet OMZT::rkj-repos
#zinit snippet OMZT::tjkirch
#zinit snippet OMZT::tonotdo
#zinit snippet OMZT::wedisagree

# Load normal GitHub plugin with theme depending on OMZ Git library
#zinit light NicoSantangelo/Alpharized

### End of Zinit's installer chunk
