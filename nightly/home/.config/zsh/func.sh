#!/bin/bash

ffv() {
  nvim ~/m/vim/"$1"
}

abi() {
	ssh pspinc@"$AMBK10" nping -c 1 "$1"
}

abn() {
	ssh pspinc@"$AMBK10" nmap "$1"
}

bak() {
	cp "$1" "$2"/"$(date '+%Y-%m-%d')"."$1"
}

ins() {
	url="$1"
	user=$ins_user
	id=$(echo "$url" | sed -e 's|.*p/\(.*\)/.*|\1|')
	instaloader -l "$user" -- -"$id"
}

jp() {
	odir="$(zoxide query -i --)" && cd "$odir" || exit
}

fid() {
	cd "$(zoxide query -i --)" || exit
}

cit() {
	(cd ~/m/py/cli && pipenv run python db_cli.py | sk --bind 'enter:execute(test.sh {})+abort')
}

skg() {
	(
		if [ $# -ge 1 ]; then
			cd "$1" && nvim "$(sk --ansi -i -c 'rg --color=always --line-number "{}"' | perl -pe 's;(.*?):.*;\1;')"
		else
			nvim "$(sk --ansi -i -c 'rg --color=always --line-number "{}"' | perl -pe 's;(.*?):.*;\1;')"
		fi
	)
}

skf() {
	(cd "$1" && sk --bind 'enter:execute(nvim {})+abort')
}

zqi() {
	cd "$(zoxide query -i)" || exit
}

mks() {
	file=$1.sh
	echo '#!/bin/bash' >$file
	chmod +x $file
}

mkm() {
	file=$1.mjs
	echo '#!/usr/bin/env zx' >$file
	chmod +x $file
}

rcl() {
	dir=Pictures
	remote=mega:$dir
	local=~/$dir
	rclone sync $remote $local
}
rcs() {
	dir=Pictures
	remote=mega:$dir
	local=~/$dir
	rclone sync $local $remote
}
files() {
	repo=$1
	tag=latest
	url="https://api.github.com/repos/$repo/releases/$tag"
	curl -s $url | grep "browser_download_url.*" | grep -v "zsync" | awk '{print $2}' | tr -d \" | awk -F "/" '{print $NF}'
}

gic() {
	git add -A
	git commit -m "$1"
	git push
}

geb() {
	echo '#!/bin/bash' >$1
	echo 'echo test' >>$1
	chmod +x $1
}

dtags() {
	local image="${1}"
	wget -q https://registry.hub.docker.com/v1/repositories/"${image}"/tags -O - | tr -d '[]" ' | tr '}' '\n' | awk -F: '{print $3}'
}

function pretty_csv() {
	column -t -s, -n "$@" | less -F -S -X -K
}

yi() {
	echo "Yarn Info of $1"
	yarn info $1 --json | jq ".data.versions"
}

# find-in-file - usage: fif <SEARCH_TERM>
fif() {
	if [ ! "$#" -gt 0 ]; then
		echo "Need a string to search for!"
		return 1
	fi
	rg --files-with-matches --no-messages "$1" | fzf $FZF_PREVIEW_WINDOW --preview "rg --ignore-case --pretty --context 10 '$1' {}"
}
# Select a docker container to start and attach to
dca() {
	local cid
	cid=$(docker ps -a | sed 1d | fzf -1 -q "$1" | awk '{print $1}')

	[ -n "$cid" ] && docker start "$cid" && docker attach "$cid"
}
# Select a running docker container to stop
dcs() {
	local cid
	cid=$(docker ps | sed 1d | fzf -q "$1" | awk '{print $1}')

	[ -n "$cid" ] && docker stop "$cid"
}
# Select a docker container to remove
dcr() {
	local cid
	cid=$(docker ps -a | sed 1d | fzf -q "$1" | awk '{print $1}')

	[ -n "$cid" ] && docker rm "$cid"
}
