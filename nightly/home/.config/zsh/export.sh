export ZSH="$HOME/.oh-my-zsh"
export N_PREFIX="$HOME/n"
[[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin" # Added by n-install (see http://git.io/n-install-repo).
export TERM='xterm-256color'
# export TERM='screen-256color'
#--preview-window=:hidden
#alias fv='$e -o `fd . "$HOME/m/vim" -d 1 -t f | fzf --preview "bat --color always {}" --preview-window=right:20%`'
# EDITORS
e="$HOME/bin/nvim"
export EDITOR="$e"
export VISUAL_EDITOR="$e"
export VISUAL="$e"
export COLORTERM=truecolor

# go config
export GOPATH="$HOME/go"
export GOBIN="$GOPATH/bin"
export GOROOT="/usr/local/gofish/Barrel/go/1.14.6/go"
export RUST_SRC_PATH="$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src"
export PATH="/usr/lib/freecad-daily/lib:/home/$USER/.cargo/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/home/$USER/.local/bin:/home/$USER/n/bin:$GOPATH/bin:$GOROOT/bin:$N_PREFIX/bin:$HOME/.yarn/bin:/snap/bin:$HOME/.pyenv/bin:/opt:$HOME/v:$HOME/.emacs.d/bin:$HOME/bin"
