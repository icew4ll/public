#!/bin/bash

# CTRL-R - Paste the selected command from history into the command line
# bind '"\C-r": " \C-e\C-u\C-y\ey\C-u`__skim_history__`\e\C-e\er\e^"'

# export SKIM_DEFAULT_COMMAND="fd --type f || git ls-tree -r --name-only HEAD || rg --files || find ."
export SKIM_DEFAULT_COMMAND="rg --files --no-ignore-vcs --hidden"
export SKIM_DEFAULT_OPTIONS="
--layout=reverse
--height=80%
--multi
--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
--prompt='🐧'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all'
--bind 'ctrl-y:execute-silent(echo {+} | pbcopy)'
--bind 'ctrl-e:execute(echo {+} | xargs -o vim)'
--bind 'ctrl-v:execute(code {+})'
"

export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden'
#export FZF_DEFAULT_COMMAND="fd . $HOME"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd -t d . $HOME"
export FZF_DEFAULT_OPTS="
--layout=reverse
--height=80%
--multi
--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
--pointer='🔮'
--marker='✓'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all'
--bind 'ctrl-y:execute-silent(echo {+} | pbcopy)'
--bind 'ctrl-e:execute(echo {+} | xargs -o vim)'
--bind 'ctrl-v:execute(code {+})'
"
# preview windows
# export FZF_DEFAULT_OPTS="
# --layout=reverse
# --height=80%
# --multi
# --preview-window=right:20%
# --preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (lsd --tree {} | less)) || echo {} 2> /dev/null | head -200'
# --color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
# --prompt='∼ ' --pointer='▶' --marker='✓'
# --bind '?:toggle-preview'
# --bind 'ctrl-a:select-all'
# --bind 'ctrl-y:execute-silent(echo {+} | pbcopy)'
# --bind 'ctrl-e:execute(echo {+} | xargs -o vim)'
# --bind 'ctrl-v:execute(code {+})'
# "

# FZF_TAB_COMMAND=(
# 	fzf
# 	--ansi # Enable ANSI color support, necessary for showing groups
# 	--expect='$continuous_trigger,$print_query' # For continuous completion and print query
# 	'--color=hl:$(( $#headers == 0 ? 108 : 255 ))'
# 	--nth=2,3 --delimiter='\x00' # Don't search prefix
# 	--layout=reverse --height='${FZF_TMUX_HEIGHT:=75%}'
# 	--tiebreak=begin -m --bind=tab:down,btab:up,change:top,ctrl-space:toggle --cycle
# 	'--query=$query' # $query will be expanded to query string at runtime.
# 	'--header-lines=$#headers' # $#headers will be expanded to lines of headers at runtime
# 	--print-query
# )
# zstyle ':fzf-tab:*' command $FZF_TAB_COMMAND
