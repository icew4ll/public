# setup {{{
export ZSH="$HOME/.oh-my-zsh"

plugins=(zsh-autosuggestions zsh-syntax-highlighting)

DISABLE_MAGIC_FUNCTIONS=true
source $ZSH/oh-my-zsh.sh
# }}}
# exports {{{
#export FZF_DEFAULT_COMMAND='fd . $HOME'
#export FZF_CTRL_T_COMMAND='fd -t d . $HOME'
#export FZF_ALT_C_COMMAND="fd -t d . $HOME"
#export BAT_THEME="TwoDark"
#export FZF_COMPLETION_OPTS="--preview '(bat --color=always {} || cat {} || tree -C {}) 2> /dev/null | head -200'"
#export FZF_CTRL_T_OPTS="$FZF_COMPLETION_OPTS"
#export FZF_DEFAULT_COMMAND='rg --files --hidden --follow --glob "!.git/*" --glob "!node_modules/*"'
#export FZF_DEFAULT_COMMAND="fd . $HOME"
#export VISUAL="/usr/local/bin/nvim.appimage"
#export MYVIMRC='$HOME/.config/nvim/init.vim'
#export PYENV_ROOT='$HOME/.pyenv'
# n install
export N_PREFIX="$HOME/n"; [[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin"  # Added by n-install (see http://git.io/n-install-repo).
export NNN_PLUG='f:finder;o:fzopen;p:mocplay;d:diffs;t:nmount;v:imgview'
export NNN_USE_EDITOR=1
export TERM='xterm-256color'
# [[ $TMUX = "" ]] && export TERM="xterm-256color"
# export TERM='screen-256color'
export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden'
#export FZF_DEFAULT_COMMAND="fd . $HOME"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd -t d . $HOME"
export FZF_DEFAULT_OPTS="
--layout=reverse
--info=inline
--height=80%
--multi
--preview-window=right:60%
--preview '([[ -f {} ]] && (bat --style=numbers --color=always {} || cat {})) || ([[ -d {} ]] && (lsd --tree {} | less)) || echo {} 2> /dev/null | head -200'
--color='hl:148,hl+:154,pointer:032,marker:010,bg+:237,gutter:008'
--prompt='∼ ' --pointer='▶' --marker='✓'
--bind '?:toggle-preview'
--bind 'ctrl-a:select-all'
--bind 'ctrl-y:execute-silent(echo {+} | pbcopy)'
--bind 'ctrl-e:execute(echo {+} | xargs -o vim)'
--bind 'ctrl-v:execute(code {+})'
"
#--preview-window=:hidden
#alias fv='$e -o `fd . "$HOME/m/vim" -d 1 -t f | fzf --preview "bat --color always {}" --preview-window=right:20%`'
# EDITORS
e="$HOME/bin/nvim"
export EDITOR="$e"
export VISUAL_EDITOR="$e"
export VISUAL="$e"
# xdg-mime default vim.desktop text/plain
export COLORTERM=truecolor

# go config
export GOPATH="$HOME/go"
export GOBIN="$GOPATH/bin"
export GOROOT="/usr/local/gofish/Barrel/go/1.14.6/go"
export RUST_SRC_PATH="$HOME/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/src"
export PATH="/usr/lib/freecad-daily/lib:/home/$USER/.cargo/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/home/$USER/.local/bin:/home/$USER/n/bin:$GOPATH/bin:$GOROOT/bin:$N_PREFIX/bin:$HOME/.yarn/bin:/snap/bin:$HOME/.pyenv/bin:/opt:$HOME/v:$HOME/.emacs.d/bin:$HOME/bin"

# vim bindings
bindkey -v

# alias
source ~/Documents/psp/secret.zsh

# zplug
#source ~/.zplug/init.zsh
#zplug "zsh-users/zsh-autosuggestions"
#zplug 'wfxr/forgit'
#zplug "skywind3000/z.lua"
#zplug "softmoth/zsh-vim-mode"
#zplug "zdharma/fast-syntax-highlighting", defer:2

# Disabled
#zplug "ajeetdsouza/zoxide"
#zplug "zsh-users/zsh-syntax-highlighting", defer:2
#zplug 'dracula/zsh', as:theme

# Install plugins if there are plugins that have not been installed
#if ! zplug check --verbose; then
    #printf "Install? [y/N]: "
    #if read -q; then
        #echo; zplug install
    #fi
#fi

# Then, source plugins and add commands to $PATH
#zplug load --verbose

# caps lawk
#if pgrep -x "xcape" > /dev/null ; then pkill "xcape" ; fi
##if pgrep -x "xcape" > /dev/null ; then echo "null" ; fi
##setxkbmap -option
#setxkbmap -option "caps:ctrl_modifier" && xcape -e "Caps_Lock=Escape" &
xset r rate 300 50

# fix display
export dvi2=DVI-I-2-2
export dvi1=DVI-I-1-1
export edp1=eDP-1

# }}}
# alias {{{
alias e='$e'
alias m='emacs -nw'
alias t='~/m/dot/tmux'
alias i='~/m/morph/target/release/morph ~/Documents/psp/list'
alias nn='nnn'
alias push='~/m/dot/dot.sh push'
alias pull='~/m/dot/dot.sh pull'
alias rustscan='docker run -it --rm --name rustscan rustscan/rustscan:alpine'
alias we='watchexec'
alias grd='gridsome develop'
alias grb='gridsome build'
alias grc='gridsome create'
alias est='$e ~/m/st/config.h'
alias bst='bash -c "cd ~/m/st/ ; sudo make clean install"'
alias edw='$e ~/m/dwm/config.h'
alias bdw='bash -c "cd ~/m/dwm/ ; sudo make clean install"'
alias en='$e ~/Documents/psp/note.sh'
alias eo='$e ~/m/vim/org.org'
alias up='sudo apt update && sudo apt upgrade -f && sudo apt autoremove && n latest && yarn global upgrade && rustup self update && rustup update && $e -E -c "call dein#update()" -c q && python3.8 -m pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 python3.8 -m pip install -U'
alias ee='$e ~/.config/nvim/init.vim'
alias eb='$e ~/.config/nvim/init.vim.bak'
alias ev='$e ~/.elvish/rc.elv'
alias ea='$e ~/m/dot/alias.zsh'
alias ez='$e ~/.zshrc'
alias sa='source ~/m/dot/alias.zsh'
alias sz='source ~/.zshrc'
alias ci='cat ~/Documents/psp/list | grep'
alias jb="$HOME/m/method/target/release/method jb 216.230.254.94 216.230.254.95"
alias start='$e ~/Documents/psp/start'
alias yx='youtube-dl -x --audio-format m4a'
alias yv='youtube-dl'
alias yxp='youtube-dl -x --audio-format m4a --no-playlist'
alias pi='python3.8 -m pip install'
alias alc='$e ~/.config/alacritty/alacritty.yml'
alias cb='cargo build --release'
alias adc='$e ~/m/dot/config.json'
alias adp='~/m/adam/target/release/adam push'
alias adu='~/m/adam/target/release/adam pull'
alias tc='$e ~/.tmux.conf.local'
alias ff='$e -o `fd -E "node_modules" -H -t f | fzf --preview "bat --color always {}" --preview-window=right:60%`'
alias fv='$e -o `fd . "$HOME/m/vim" -d 1 -t f | fzf --preview "bat --color always {}" --preview-window=right:60%`'
alias fm='$e -o `fd . "$HOME/m" -t f | fzf --preview "bat --color always {}" --preview-window=right:60%`'
alias ef='$e -o `fd -d 1 -t f | fzf --preview "bat --color always {}" --preview-window=right:60%`'
alias ma='/home/fish/m/mail/target/release/mail /home/fish/m/mail/config.json'
alias li='$e ~/Documents/psp/list'
alias lo='$e ~/Documents/psp/login'
alias el='$e ~/m/vim/links.org'
alias ek='$e ~/m/vim/kube.org'
alias et='$e ~/m/vim/traefik.org'
alias ed='$e ~/m/vim/docker.org'
alias em='$e ~/m/vim/minikube.org'
alias ej='$e ~/m/vim/job.org'
alias er='$e ~/m/vim/rust.org'
alias eh='$e /etc/hosts'
alias ss='ss'
alias sa='shiori add'
alias live='live-server'
alias mn='monolith'
alias ppa='$e /etc/apt/sources.list'
alias ppa2='cd /etc/apt/sources.list.d'
alias ro='redshift -O 3200K'
alias es='$e ~/Documents/psp/secret.zsh'
alias l='lsd'
alias la='lsd -la'
alias tree='lsd --tree'
alias emoj="emoji-fzf preview | fzf --preview 'emoji-fzf get --name {1}' | cut -d \" \" -f 1 | emoji-fzf get"
alias shot='shotgun $(slop -c 0,0,0,0.75 -l -f "-i %i -g %g")'
alias sl='amixer -D pulse set Master 0%,10%'
alias bf='$HOME/m/atp/target/release/atp'
alias ca='cargo add'
alias cn='cargo new'
alias cw='cargo watch -x run'
alias cr='cargo run --'
alias dol='dolphin -stylesheet ~/.config/qt5ct/qss/dark.qss'
alias ds='cd ~/Documents/sync'
alias wen='watchexec $HOME/n/bin/node'
alias node='$HOME/n/bin/node'
alias ac="$e ~/.config/alacritty/alacritty.yml"
alias j="joshuto"
alias ej="$e ~/.config/joshuto/mimetype.toml"
alias ge="git conflicted"
alias gc="git --no-pager diff --name-only --diff-filter=U"
alias snip="$e ~/m/dot/snip/javascript/javascript-react.snippets"
alias p="ping 8.8.8.8"
alias emoj="emoji-fzf preview | fzf --preview 'emoji-fzf get --name {1}' | cut -d \" \" -f 1 | emoji-fzf get"
alias yp="yarn production"
alias yb="yarn build"
alias yd="yarn dev"
alias r="ranger"
alias h="hunter"
alias p="emulsion"
alias font="fc-list | grep Nerd | awk -F':' '{print \$2}'"
#alias rp="rg --column --no-heading --line-number --color=always ~/m"
# }}}
# functions {{{
rcl() {
	dir=$1
  remote=mega:$dir
  local=~/$dir
  rclone copy $local $remote
  rclone copy $remote $local
}
files() {
	repo=$1
	tag=latest
	url="https://api.github.com/repos/$repo/releases/$tag"
	curl -s $url | grep "browser_download_url.*" | grep -v "zsync" | awk '{print $2}' | tr -d \" | awk -F "/" '{print $NF}'
}

gic () {
  git add -A
  git commit -m "$1"
  git push
}

geb () {
    echo '#!/bin/bash' > $1
    echo 'echo test' >> $1
    chmod +x $1
}

dtags () {
    local image="${1}"
    wget -q https://registry.hub.docker.com/v1/repositories/"${image}"/tags -O - | tr -d '[]" ' | tr '}' '\n' | awk -F: '{print $3}'
}

function pretty_csv {
    column -t -s, -n "$@" | less -F -S -X -K
}

yi () {
  echo "Yarn Info of $1"
  yarn info $1 --json | jq ".data.versions"
}

# find-in-file - usage: fif <SEARCH_TERM>
fif() {
  if [ ! "$#" -gt 0 ]; then
    echo "Need a string to search for!";
    return 1;
  fi
  rg --files-with-matches --no-messages "$1" | fzf $FZF_PREVIEW_WINDOW --preview "rg --ignore-case --pretty --context 10 '$1' {}"
}
# Select a docker container to start and attach to
function da() {
  local cid
  cid=$(docker ps -a | sed 1d | fzf -1 -q "$1" | awk '{print $1}')

  [ -n "$cid" ] && docker start "$cid" && docker attach "$cid"
}
# Select a running docker container to stop
function ds() {
  local cid
  cid=$(docker ps | sed 1d | fzf -q "$1" | awk '{print $1}')

  [ -n "$cid" ] && docker stop "$cid"
}
# Select a docker container to remove
function drm() {
  local cid
  cid=$(docker ps -a | sed 1d | fzf -q "$1" | awk '{print $1}')

  [ -n "$cid" ] && docker rm "$cid"
}
# }}}
# zinit {{{
### Added by Zinit's installer
if [[ ! -f $HOME/.zinit/bin/zinit.zsh ]]; then
    print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
    command mkdir -p "$HOME/.zinit" && command chmod g-rwX "$HOME/.zinit"
    command git clone https://github.com/zdharma/zinit "$HOME/.zinit/bin" && \
        print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
        print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$HOME/.zinit/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zinit light-mode for \
    tj/git-extras \
    softmoth/zsh-vim-mode \
    skywind3000/z.lua \
    wfxr/forgit \
    zinit-zsh/z-a-rust \
    zinit-zsh/z-a-as-monitor \
    zinit-zsh/z-a-patch-dl \
    zinit-zsh/z-a-bin-gem-node

# Load OMZ Git library
zinit snippet OMZL::git.zsh

# Load Git plugin from OMZ
zinit snippet OMZP::git
zinit cdclear -q # <- forget completions provided up to this moment

setopt promptsubst

# Load theme from OMZ
zinit snippet OMZT::amuse
#zinit snippet OMZT::avit
#zinit snippet OMZT::gallois
#zinit snippet OMZT::gnzh
#zinit snippet OMZT::rkj-repos
#zinit snippet OMZT::tjkirch
#zinit snippet OMZT::tonotdo
#zinit snippet OMZT::wedisagree

# Load normal GitHub plugin with theme depending on OMZ Git library
#zinit light NicoSantangelo/Alpharized

### End of Zinit's installer chunk
# }}}

# source fzf at end
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
#source /home/ice/.config/broot/launcher/bash/br
#eval "$(starship init zsh)"
#eval "$(zoxide init zsh)"
