fn emoji {
emoji-fzf preview | fzf --preview 'emoji-fzf get --name {1}' | awk '{print $1 }' | emoji-fzf get
}

fn vb {
  var dir = $E:HOME"/m/hax/elvish/vbox"
  fd . $dir -x basename | sk
}

fn qa {
  if ?(pgrep qemu) {
    kill -9 (pgrep VBoxHeadless)
  } else {
    elvish ~/m/hax/elvish/qemu/guix.elv &
  }
}

fn vda {
  if ?(pgrep VBoxHeadless) {
    vboxmanage controlvm $E:vm poweroff
    pgrep VBoxHeadless
  } else {
    VBoxHeadless -s $E:vm &
    VBoxManage showvminfo $E:vm | rg "(Rule|VRDE:)"
    ps aux | rg VBoxHeadless
  }
}

fn vdh {
  ssh-keygen -f $E:HOME"/.ssh/known_hosts" -R "[127.0.0.1]:3022"
  ssh -p 3022 root@127.0.0.1
}

fn vdx {
  # bash -c "xfreerdp /gdi:hw /gfx:RFX /v:127.0.0.1:5555 +compression +clipboard"
  bash -c "xfreerdp /smart-sizing /gdi:hw /gfx:RFX /v:127.0.0.1:5555 +compression +clipboard"
}

fn vds {
  vboxmanage list runningvms
}

fn mks {
  |file|
	echo "#!/bin/bash" > $file.sh
	chmod +x $file.sh
}

fn sch {
  shotgun (str:split " " (hacksaw -f "-i %i -g %g"))
}

fn scl {
  shotgun (str:split " " (slop -f "-i %i -g %g"))
}

fn giti {
  |repo|
  git init --initial-branch=main
  git remote add origin "git@gitlab.com:icew4ll/"$repo".git"
  git add .
  git commit -m "Initial commit"
  git push -u origin main
}

fn rcs {
  utils:yellow "Saving..."
  var dir = Pictures
  var local = ~/$dir
  var remote = mega:$dir
  rclone sync $local $remote
  utils:green "Save Complete"
}

fn rcl {
  utils:yellow "Loading..."
  var dir = Pictures
  var local = ~/$dir
  var remote = mega:$dir
  rclone sync $remote $local
  utils:green "Load Complete"
}

fn upn {
  cd ~/m/py/tool
  pipenv run python nerd.py repo nvim
}

fn ups {
  cd ~/m/py/tool
  pipenv run python nerd.py sys
}

fn save {
  cd ~/m/py/tool
  pipenv run python nerd.py save
  rcs
}

fn haz { curl icanhazip.com }

fn vpn {
  var dir = ~/.config/openvpn
  var file = $dir/us_seattle.ovpn
  curl icanhazip.com
  sudo openvpn $file
}

fn vpn {
  var dir = ~/.config/openvpn
  var file = $dir/us_seattle.ovpn
  curl icanhazip.com
  sudo openvpn $file
}

fn bare {
  var bare = ".bare"
  git init --initial-branch=main --bare $bare
  echo "gitdir: ./"$bare > .git
  var name = "main"
  git clone $bare $name
  cd $name
}

fn tt {
  # fd . $E:BOOKMARKS_DIR -x readlink -f
  # fd . $E:BOOKMARKS_DIR -x elvish -c "echo {}"
  # fd . $E:BOOKMARKS_DIR -x elvish -c 'printf "%10s -> %s\n" (basename {}) (readlink -f {})'
  elvish ~/m/hax/elvish/hash.elv
  var dir = (fd . $E:BOOKMARKS_DIR -x elvish -c 'printf "%10s -> %s\n" (basename {}) (readlink -f {})' | sk | awk '{print $3}')
  if ?(test -d $dir) {
    cd $dir
  }
}
