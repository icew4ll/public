use file
fn external-edit-command {
  var temp-file = (mktemp)
  print $edit:current-command > $temp-file
  try {
    # This assumes $E:EDITOR is an absolute path. If you prefer to use
    # just the bare command and have it resolved when this is run use
    # (external $E:EDITOR)
    $E:EDITOR $temp-file[name] </dev/tty >/dev/tty 2>&1
    set edit:current-command = (slurp < $temp-file[name])[..-1]
  } finally {
    file:close $temp-file
    rm $temp-file[name]
  }
}
