# alias
fn n { e:nnn }
fn t { e:tmux }
fn pj {|@a| e:pijul $@a}
fn se { exec elvish }
fn e {|@a| e:nvim $@a}
fn ng { utils:skrg ~/m/vim }
fn nf { utils:skopen ~/m/vim }
fn vg { sh ~/.config/nnn/plugins/skrg }
fn vf { sh ~/.config/nnn/plugins/skopen }
fn yx {|a| e:youtube-dl -x $a}
fn yv {|a| e:youtube-dl --no-playlist $a}
fn yp {|a| e:youtube-dl $a}
fn nc { e:ncmpcpp }
fn red { e:redshift -O 4200K }
fn ac { e:nvim ~/.config/alacritty/alacritty.yml }
fn tsh { e:nvim ~/.ssh/config }
fn th { e:nvim ~/m/hax/elvish/hash.elv }
fn hh { e:elvish ~/m/hax/elvish/hash.elv }
fn np { e:nvim ~/.config/nvim/profiles.lua }
fn ta { e:nvim ~/.elvish/rc.elv }
fn ti { e:nvim ~/.config/elvish/lib/init.elv }
fn tn { e:nnn ~/.config/elvish/lib/ }
fn te { e:nvim ~/m/vim/elvish.org }
fn td { e:nvim ~/m/vim/org/todo.org }
fn l {|@a| e:lsd $@a }
fn la {|@a| e:lsd -la $@a }
fn gtl { e:git log --oneline --decorate --graph --all }
fn gts { e:git status }
fn gtr { e:git remote -v }
fn gtb { e:git branch -vva }
fn gta { e:git add . }
fn gtc {|msg| e:git commit -m $msg }
fn gtp { e:git push}
fn ree { reflex -r ".elv" -- elvish "{}" }
fn reb { reflex -r ".sh" -- bash "{}" }
fn sc { utils:sk_bash ~/.screenlayout }
