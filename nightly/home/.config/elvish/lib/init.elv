xset r rate 200 140
# setxkbmap -option caps:swapescpe

var GUIX = ~/.guix-profile

# initial paths
set paths = [
  ~/.emacs.d/bin
  ~/.config/guix/current/bin
  ~/bin 
  ~/.guix-profile/bin
  ~/.cargo/bin
  ~/.volta/bin
  ~/bin/build/.go/bin
  ~/.pyenv/bin
  ~/.fzf/bin
  /usr/local/sbin
  /usr/local/bin
  /usr/sbin
  /usr/bin
  /sbin
  /bin
  /snap/bin
]

# environment variables
var build = ~/bin/build
var envs = [
  &NNN_PLUG="r:skrg;f:fzopen;p:preview-tui;i:imgview;x:xdgdefault"
  &NNN_FIFO=/tmp/nnn.fifo
  &BOOKMARKS_DIR="~/.config/nnn/bookmarks"
  &EDITOR="nvim"
  &NODE_OPTIONS="--openssl-legacy-provider"
  &VISUAL_EDITOR="nvim"
  &VISUAL="nvim"
  &TERM="xterm-256color"
  &GO111MODULE="on"
  &GOPATH=$build/.go
  &GOROOT=$build/go
  &GOBIN=(go env GOPATH)/bin
  &MANPAGER='nvim +Man!'
  &NEXT_TELEMETRY_DEBUG=1
  &PYENV_ROOT=~/.pyenv
  &XDG_CONFIG_HOME=~/.config
  &XDG_CACHE_HOME=~/.cache
  &BOOKMARKS_DIR=~/.config/nnn/bookmarks
  &PY_BIN=(pyenv which python)
  &PY_VER=(pyenv which python | perl -pe 's;^.*versions/(.*?)/.*;\1;')
  &RUST_SRC_PATH=(rustc --print sysroot)/lib/rustlib/src/rust/library
  &RG_PREFIX="rg --column --line-number --no-heading --color=always --smart-case"
  &FZF_DEFAULT_COMMAND="fd --type f"
  &GITPAGER="bat"
  # &GUIX_PROFILE=~/.config/guix/current
  # &GUIX_PROFILE=(echo ~root)/.config/guix/current
  &GUIX_PROFILE=$GUIX
  &INFOPATH=$GUIX"/share/info"
  &EMACSLOADPATH=$GUIX"/share/emacs/site-lisp"
  &GUILE_LOAD_COMPILED_PATH=$GUIX"/lib/guile/3.0/site-ccache:"$GUIX"/share/guile/site/3.0"
  &GUILE_LOAD_PATH=$GUIX"/share/guile/site/3.0"
  &vm="guix"
]
keys $envs | each {|k| set-env $k $envs[$k] }

# env based path
set paths = [
  ~/.pyenv/versions/$E:PY_VER/bin
  $@paths
]

# prompt
eval (starship init elvish --print-full-init | upgrade-scripts-for-0.17 | slurp)
eval (zoxide init elvish | upgrade-scripts-for-0.17 | slurp)
# prompt
# set edit:prompt = { tilde-abbr $pwd; put '🐚 ' }
# set edit:rprompt = (constantly (styled (whoami)📕(hostname) inverse))


# TODO locations and migrate to sk
# Filter the command history through the fzf program. This is normally bound
# to Ctrl-R.
fn history {||
  var new-cmd = (
    edit:command-history &dedup &newest-first &cmd-only |
    to-terminated "\x00" |
    try {
      fzf --no-sort --read0 --info=hidden --exact ^
        --query=$edit:current-command
    } except {
      # If the user presses [Escape] to cancel the fzf operation it will exit
      # with a non-zero status. Ignore that we ran this function in that case.
      return
    }
  )
  set edit:current-command = $new-cmd
}

set edit:insert:binding[Ctrl-R] = {|| history >/dev/tty 2>&1 }

