use utils
use str

fn green {
  |s|
  echo "\e[0;32m"$s"\e[0m"
}

fn yellow {
  |s|
  echo "\e[1;33m"$s"\e[0m"
}

fn get_links {
  |@url|
  curl -sL $@url | rg 'href' | perl -pe 's|.*href="(.*?)".*|\1|'
}

fn gapi {
  |url|
  var repo = (str:join "/" [(str:split / $url)][-2..])
  var url = "https://api.github.com/repos/"$repo"/releases"
  curl -sL $url | jq -r ".[].assets[].browser_download_url" | head -1
}

fn down_targz {
  |url dir|
  echo "Downloading..."
  echo $url
  echo $dir
  cd $dir
  curl -L $url | tar zxv
}

fn clean_dir {
  |dir|
  utils:yellow "Cleaning... "$dir
  if ?(test -d $dir) {
    rm -vrI $dir
    mkdir -p $dir
  } else {
    mkdir -p $dir
  }
  cd $dir
  utils:yellow "Working... "(pwd)
}

fn linky {
  |input output|
  utils:yellow "Linking... "$input" "$output
  ln -sf $input $output
  utils:green "Linked... "$input" "$output
}

fn skrg {
  |dir|
  cd $dir
  sh ~/.config/nnn/plugins/skrg
}

fn skopen {
  |dir|
  cd $dir
  sh ~/.config/nnn/plugins/skopen
}

fn sk_bash {
  |@dir|
  bash (fd . $@dir | sk)
}

fn form {
  |a|
  printf "%10s -> %s\n" $a $a
}
