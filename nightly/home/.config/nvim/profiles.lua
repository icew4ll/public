local base = "~/.config/nvim_profiles/"
local profiles = {
	doom = {
		base .. "doom-nvim",
		{
			plugins = "packer",
			preconfigure = "doom-nvim",
		},
	},
	null = {
		base .. "null",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	chad = {
		base .. "NvChad",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	org = {
		base .. "org",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	lunar = {
		base .. "LunarVim",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	test = {
		base .. "test",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	pr = {
		base .. "proto",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	basic = {
		base .. "basic",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	hyper = {
		base .. "hyper",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	minimal = {
		base .. "minimal",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	epsilon = {
		base .. "epsilon",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
	main = {
		base .. "main",
		{
			plugins = "packer",
			preconfigure = "packer:opt",
		},
	},
}

-- return "basic", profiles
return "main", profiles
-- return "minimal", profiles
-- return "hyper", profiles
-- return "epsilon", profiles
-- return "null", profiles
-- return "org", profiles
-- return "base", profiles
-- return "test", profiles
