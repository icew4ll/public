local util = require("lspconfig/util")
local mappings = require("lsp.custom_attach")

-- js

local prettier = {
    formatCommand = "./node_modules/.bin/prettier --stdin --stdin-filepath ${INPUT}",
    formatStdin = true
}

local gprettier = {
    formatCommand = "prettier --stdin --stdin-filepath ${INPUT}",
    formatStdin = true
}

local eslint_d = {
    lintCommand = "eslint_d -f unix --stdin --stdin-filename ${INPUT}",
    lintStdin = true,
    lintFormats = {"%f:%l:%c: %m"},
    lintIgnoreExitCode = true,
    formatCommand = "eslint_d --fix-to-stdout --stdin --stdin-filename=${INPUT}",
    formatStdin = true
}

-- python

local flake8 = {
    lintCommand = "flake8 --config ~/.config/efm-langserver/flake8.toml --stdin-display-name ${INPUT} -",
    lintStdin = true,
    lintFormats = {"%f:%l:%c: %m"}
}

local mypy = {
    lintCommand = "mypy",
    lintStdin = true,
    lintFormats = {"%f:%l:%c: %trror: %m", "%f:%l:%c: %tarning: %m", "%f:%l:%c: %tote: %m"}
}

local black = {
    formatCommand = "black -",
    formatStdin = true
}

local isort = {
    formatCommand = "isort --stdout -",
    formatStdin = true
}

-- rust

local rustfmt = {
    formatCommand = "cargo fmt -",
    formatStdin = true
}

local clippy = {
    lintCommand = "cargo clippy"
}

-- shell

local shellcheck = {
    lintCommand = "shellcheck -f gcc -x -",
    lintStdin = true,
    lintFormats = {"%f:%l:%c: %trror: %m", "%f:%l:%c: %tarning: %m", "%f:%l:%c: %tote: %m"}
}

local shfmt = {
    formatCommand = "shfmt -ci -s -bn",
    formatStdin = true
}

-- lua

local luafmt = {
    formatCommand = "luafmt -i 2 -l 80 --stdin",
    formatStdin = true
}

-- vlang

local vfmt = {
    formatCommand = "v fmt"
}

-- data

local markdownlint = {
    lintCommand = "markdownlint -s -c ~/.config/efm-langserver/markdown.yaml",
    lintStdin = true,
    lintFormats = {"%f:%l %m", "%f:%l:%c %m", "%f: %l: %m"},
}

local yamllint = {
    lintCommand = "yamllint -c ~/.config/efm-langserver/yamllint.yaml -f parsable -",
    lintStdin = true,
    lintFormats = {"%f:%l %m", "%f:%l:%c %m", "%f: %l: %m"}
}

local efmConfig = {
    root_dir = util.root_pattern(".git", vim.fn.getcwd()),
    on_attach = function(client)
        mappings(client)
        client.resolved_capabilities.document_formatting = true
    end,
    filetypes = {
        "javascript",
        "typescript",
        "typescriptreact",
        "javascriptreact",
        "css",
        "scss",
        "json",
        "html",
        "lua",
        "sh",
        "v",
        "rust",
        "markdown",
        "yaml",
        "toml",
        "python"
    },
    init_options = {documentFormatting = true},
    settings = {
        languages = {
            typescript = {prettier, eslint_d},
            javascript = {prettier, eslint_d},
            typescriptreact = {eslint_d, prettier},
            javascriptreact = {prettier, eslint_d},
            css = {prettier},
            scss = {prettier},
            json = {prettier},
            html = {prettier},
            lua = {luafmt},
            sh = {shfmt, shellcheck},
            v = {vfmt},
            rust = {rustfmt, clippy},
            markdown = {markdownlint},
            yaml = {yamllint, gprettier},
            toml = {gprettier},
            python = {flake8, mypy, black, isort}
        }
    }
}

return efmConfig
