local map = vim.api.nvim_set_keymap

local mapper = function(mode, key, result)
    map(mode, key, "<cmd>lua " .. result .. "<cr>", {noremap = true, silent = true})
end

local function custom_attach(client)
    -- local ft = vim.api.nvim_buf_get_option(0, "filetype")
    -- if ft ~= "lua" then
    --     mapper("n", "gk", "require('lspsaga.hover').render_hover_doc()")
    -- end
    -- mapper("n", "K", "require('lspsaga.hover').render_hover_doc()")
    if client.resolved_capabilities.hover then
        mapper("n", "<leader>k", "vim.lsp.buf.hover()")
        mapper("n", "<leader>[", "vim.lsp.diagnostic.goto_prev()")
        mapper("n", "<leader>]", "vim.lsp.diagnostic.goto_next()")
        mapper("n", "<leader>l", "vim.lsp.diagnostic.show_line_diagnostics()")
        -- mapper("n", "gk", "require('lspsaga.hover').render_hover_doc()")
        -- mapper("n", "g[", "require'lspsaga.diagnostic'.lsp_jump_diagnostic_next()")
        -- mapper("n", "g]", "require'lspsaga.diagnostic'.lsp_jump_diagnostic_prev()")
        -- mapper("n", "gp", "require'lspsaga.provider'.preview_definition()")
        -- mapper("n", "gi", "require'lspsaga.provider'.lsp_finder()")
        -- mapper("n", "gl", "require'lspsaga.diagnostic'.show_line_diagnostics()")
    end

    if client.resolved_capabilities.code_action then
        mapper("n", "<leader>h", "vim.lsp.buf.code_actions()")
        -- mapper("n", "ga", "require('lspsaga.codeaction').code_action()")
    end

    -- if client.resolved_capabilities.find_references then
    --     mapper("n", "gj", "require'lspsaga.provider'.lsp_finder()")
    -- end
    if client.resolved_capabilities.goto_definition then
        mapper("n", "<leader>j", "vim.lsp.buf.definition()")
    end

    if client.resolved_capabilities.document_formatting then
        mapper("n", "<leader>f", "vim.lsp.buf.formatting()")
    end

    if client.resolved_capabilities.rename then
        -- mapper("n", "gr", "require('lspsaga.rename').rename()")
        mapper("n", "<leader>r", "vim.lsp.buf.rename()")
    end

    --     if client.resolved_capabilities.implementation then
    --         mapper("n", "gl", "vim.lsp.buf.implementation()")
    --     end

    -- diagnostic
    -- map("n", "<leader>dd", ":LspDiagnostics 0<CR>", {noremap = true, silent = true})
    -- mapper("n", "<leader>dn", "vim.lsp.diagnostic.goto_next()")
    -- mapper("n", "<leader>dn", "vim.lsp.diagnostic.goto_next()")
    -- mapper("n", "<leader>dp", "vim.lsp.diagnostic.goto_prev()")
    -- mapper("n", "<leader>ds", "vim.lsp.diagnostic.show_line_diagnostics()")

end

return custom_attach
