local o = vim.o
local g = vim.g
local wo = vim.wo
local bo = vim.bo
local cmd = vim.cmd -- to execute Vim commands e.g. cmd('pwd')
-- local config = vim.fn.expand("$XDG_CONFIG_HOME")
local home = vim.fn.expand("$HOME")
local fn = vim.fn -- to call Vim functions e.g. fn.bufnr()
local execute = vim.api.nvim_command
local py = home .. "/.pyenv/versions/3.9.1/bin/python3.9"

-- create backup/swap/undo directories
local backup_dir = fn.stdpath("data") .. "/tmp/backup"
local swap_dir = fn.stdpath("data") .. "/tmp/swap"
local undo_dir = fn.stdpath("data") .. "/tmp/undo"

if fn.empty(fn.glob(backup_dir)) > 0 then
    execute("!mkdir -p" .. " " .. backup_dir)
end

if fn.empty(fn.glob(swap_dir)) > 0 then
    execute("!mkdir -p" .. " " .. swap_dir)
end

if fn.empty(fn.glob(undo_dir)) > 0 then
    execute("!mkdir -p" .. " " .. undo_dir)
end

g.python3_host_prog = py
-- require "py_lsp".setup {
--     -- This is optional, but allows to create virtual envs from nvim
--     host_python = py
-- }
g.node_host_prog = home .. "/.volta/tools/image/packages/neovim/bin/neovim-node-host"
-- lua print(vim.fn.expand("$XDG_CONFIG_HOME"))

-- g.mapleader = " "
g.mapleader = "t"
o.termguicolors = true
cmd "set noswapfile"
cmd "set wrap linebreak nolist"
-- wo.cursorline = true
wo.number = true
wo.relativenumber = true
-- wo.signcolumn = "number"
-- g.noswapfile = true
g.noshowmode = true
o.mouse = "a"
o.smartindent = true
o.tabstop = 2
o.shiftwidth = 2
o.expandtab = true
bo.expandtab = true
bo.tabstop = 2
bo.smartindent = true
bo.shiftwidth = 2

-- markdown
-- wo.conceallevel = 2
-- o.updatetime = 50
-- o.hidden = true
-- enable native markdown folding
-- g.markdown_folding = 1
g.markdown_fenced_languages = {
    "js=javascript",
    "javascript",
    "typescript",
    "html",
    "python",
    "bash"
}

-- o.wrap = true

o.splitbelow = true
o.splitright = true

o.ignorecase = true
o.smartcase = true

o.clipboard = "unnamedplus"

-- LSP
-- menuone: popup even when there's only one match
-- noinsert: Do not insert text until a selection is made
-- noselect: Do not select, force user to select one from the menu
o.completeopt = "menuone,noinsert,noselect"
-- Avoid showing message extra message when using completion
o.shortmess = "c"
-- Visualize diagnostics
g.diagnostic_enable_virtual_text = 1
g.diagnostic_trimmed_virtual_text = "40"
-- Don't show diagnostics while in insert mode
g.diagnostic_insert_delay = 1
o.updatetime = 300

-- FOLDS
o.foldlevel = 99

-- Backup, undo, swap options
bo.undofile = true
o.backup = true
o.writebackup = true
o.backupdir = backup_dir
o.directory = swap_dir .. o.directory
o.undodir = undo_dir
