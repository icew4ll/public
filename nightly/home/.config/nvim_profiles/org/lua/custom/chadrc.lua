local M = {}
M.options, M.ui, M.mappings, M.plugins = {}, {}, {}, {}

M.options = {
   relativenumber = true,
}

M.ui = {
   theme = "everforest",
}

M.plugins = {
   status = {
      nvimtree = false,
      telescope_media = true, -- media previews within telescope finders
      colorizer = true, -- media previews within telescope finders
   },
   options = {
      lspconfig = {
         setup_lspconf = "custom.plugins.lsp",
      },
   },
   default_plugin_config_replace = {
      nvim_treesitter = "custom.plugins.tree",
   },
}

return M
