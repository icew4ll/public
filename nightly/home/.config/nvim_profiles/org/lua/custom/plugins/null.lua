local null_ls = require "null-ls"

-- null-ls
local sources = {
   null_ls.builtins.formatting.stylua.with {
      filetypes = { "lua" },
   },
   -- null_ls.builtins.diagnostics.luacheck.with {
   --    filetypes = { "lua" },
   -- },
   null_ls.builtins.formatting.black.with {
      filetypes = { "python" },
   },
   null_ls.builtins.formatting.rustfmt.with {
      filetypes = { "rust" },
   },
   null_ls.builtins.formatting.prettier.with {
      filetypes = { "html", "json", "yaml", "markdown" },
   },
   null_ls.builtins.diagnostics.pylint.with {
      filetypes = { "python" },
   },
   null_ls.builtins.diagnostics.shellcheck.with {
      filetypes = { "sh", "bash" },
   },
   null_ls.builtins.diagnostics.yamllint.with {
      filetypes = { "yaml" },
   },
   null_ls.builtins.diagnostics.eslint,
   -- null_ls.builtins.code_actions.gitsigns,
}
null_ls.config { sources = sources }
