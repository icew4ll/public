local hooks = require "core.hooks"

hooks.add("install_plugins", function(use)
   use {
      "kristijanhusak/orgmode.nvim",
      requires = { "nvim-treesitter/nvim-treesitter" },
      config = function()
         require("orgmode").setup {}
      end,
   }
   use {
      "jose-elias-alvarez/null-ls.nvim",
      event = "BufRead",
      requires = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
   }
   use {
      "olambo/vi-viz",
      event = "BufEnter",
   }
   use {
      "christoomey/vim-tmux-runner",
      event = "BufEnter",
   }
   use {
      "voldikss/vim-floaterm",
      event = "BufEnter",
      config = function()
         local g = vim.g
         g.floaterm_width = 0.9
         g.floaterm_opener = "edit"
      end,
   }
   -- use {
   --    "JoosepAlviste/nvim-ts-context-commentstring",
   --    after = "nvim-treesitter",
   -- }
end)
