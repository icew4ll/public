local parser_config = require("nvim-treesitter.parsers").get_parser_configs()

parser_config.org = {
   install_info = {
      url = "https://github.com/milisims/tree-sitter-org",
      revision = "main",
      files = { "src/parser.c", "src/scanner.cc" },
   },
   filetype = "org",
}

require("nvim-treesitter.configs").setup {
   -- If TS highlights are not enabled at all, or disabled via `disable` prop, highlighting will fallback to default Vim syntax highlighting
   highlight = {
      enable = true,
      disable = { "org" }, -- Remove this to use TS highlighter for some of the highlights (Experimental)
      additional_vim_regex_highlighting = { "org" }, -- Required since TS highlighter doesn't support all syntax features (conceal)
   },
   ensure_installed = {
      "org",
      "json",
      "css",
      "typescript",
      "norg",
      "javascript",
      "html",
      "tsx",
      "yaml",
      "rust",
      "python",
      "bash",
      "lua",
      "regex",
      "vue",
      "graphql",
      "toml",
      "go",
   },
}

require("orgmode").setup {
   org_agenda_files = { "~/m/vim/agenda/**/*" },
   org_default_notes_file = "~/m/vim/default.org",
}
