local present, ts_config = pcall(require, "nvim-treesitter.configs")
if not present then
   return
end
ts_config.setup {
   ensure_installed = {
      "lua",
      "rust",
      "json",
      "css",
      "typescript",
      "javascript",
      "html",
      "tsx",
      "yaml",
      "rust",
      "python",
      "bash",
      "regex",
      "vue",
      "graphql",
      "toml",
      "go",
   },
   highlight = {
      enable = true,
      use_languagetree = true,
   },
}
