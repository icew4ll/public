-- local opt = vim.opt
local g = vim.g
local cmd = vim.cmd
local home = vim.fn.expand("$HOME")

cmd "set noswapfile"
g.python3_host_prog = vim.fn.expand("$PY_BIN")
g.node_host_prog = home .. "/.volta/tools/image/packages/neovim/bin/neovim-node-host"
