require "custom.plugins.null"
require "custom.plugins.signs"

-- require "_signs"
local M = {}
local home = vim.fn.expand "$HOME"
local build = home .. "/bin/build/lua-language-server"
local bin = build .. "/bin/Linux/lua-language-server"

M.setup_lsp = function(attach, capabilities)
   local lspconfig = require "lspconfig"

   lspconfig.sumneko_lua.setup {
      -- on_attach = function(client)
      -- 	custom_attach(client)
      -- end,
      globals = { "use" },
      cmd = { bin, "-E", build .. "/main.lua" },
      settings = {
         Lua = {
            diagnostics = {
               -- Get the language server to recognize the `vim` global
               enable = true,
               globals = { "vim" },
            },
            workspace = {
               -- Make the server aware of Neovim runtime files
               library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = { enable = false },
         },
      },
   }

   -- lspservers with default config
   local servers = {
      "pyright",
      "null-ls",
      "jsonls",
      "tsserver",
      "html",
      "rust_analyzer",
      "vimls",
      "bashls",
      "dockerls",
      "vuels",
      "cssls",
   }

   for _, lsp in ipairs(servers) do
      lspconfig[lsp].setup {
         on_attach = attach,
         capabilities = capabilities,
         -- root_dir = vim.loop.cwd,
         flags = {
            debounce_text_changes = 150,
         },
      }
   end
end
return M
