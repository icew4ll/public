local hooks = require "core.hooks"

hooks.add("setup_mappings", function(map)
   map("n", "tu", ":PackerUpdate<CR>", opt)
   map("n", "tc", ":PackerCompile<CR>", opt)
   map("n", "tq", ":q<CR>", opt)
   map("n", "ts", ":w<CR>", opt)
   map("n", "tf", ":lua vim.lsp.buf.formatting()<CR>", opt)
   map("n", "cli", ":LspInfo<CR>", opt)
   map("n", "gcc", ":CommentToggle<CR>", opt)
   map("v", "gcc", ":CommentToggle<CR>", opt)
   map("n", "t,", ":VtrAttachToPane<CR>", opt)
   map("n", "t.", ":VtrSendLinesToRunner<CR>", opt)
   map("v", "t.", ":VtrSendLinesToRunner<CR>", opt)
   map("v", "<", "<gv", opt)
   map("v", ">", ">gv", opt)

   -- vi-viz
   map("n", "<right>", "<cmd>lua require('vi-viz').vizInit()<CR>", opt)
   -- expand and contract
   map("x", "<right>", "<cmd>lua require('vi-viz').vizExpand()<CR>", opt)
   map("x", "<left>", "<cmd>lua require('vi-viz').vizContract()<CR>", opt)
   -- expand and contract by 1 char either side
   map("x", "=", "<cmd>lua require('vi-viz').vizExpand1Chr()<CR>", opt)
   map("x", "-", "<cmd>lua require('vi-viz').vizContract1Chr()<CR>", opt)
   -- good use for the r key in visual mode
   map("x", "r", "<cmd>lua require('vi-viz').vizPattern()<CR>", opt)
   -- nice to have to get dot repeat on single words
   map("x", "c", "<cmd>lua require('vi-viz').vizChange()<CR>", opt)
   -- nice to have to insert before and after
   map("x", "ii", "<cmd>lua require('vi-viz').vizInsert()<CR>", opt)
   map("x", "aa", "<cmd>lua require('vi-viz').vizAppend()<CR>", opt)
end)
