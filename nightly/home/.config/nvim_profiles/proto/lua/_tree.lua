local parser_config = require("nvim-treesitter.parsers").get_parser_configs()

parser_config.org = {
	install_info = {
		url = "https://github.com/milisims/tree-sitter-org",
		revision = "main",
		files = { "src/parser.c", "src/scanner.cc" },
	},
	filetype = "org",
}
-- parser_configs.norg = {
--     install_info = {
--         url = "https://github.com/vhyrro/tree-sitter-norg",
--         files = {"src/parser.c", "src/scanner.cc"},
--         branch = "main"
--     }
-- }
require("nvim-treesitter.configs").setup({
	ensure_installed = {
		-- "norg",
		"org",
		"json",
		"css",
		"typescript",
		"javascript",
		"html",
		"tsx",
		"yaml",
		"rust",
		"python",
		"bash",
		"lua",
		"regex",
		"vue",
		"graphql",
		"toml",
		"go",
	},
	highlight = {
		enable = true,
		disable = { "org" }, -- Remove this to use TS highlighter for some of the highlights (Experimental)
		additional_vim_regex_highlighting = { "org" }, -- Required since TS highlighter doesn't support all syntax features (conceal)
	},
})

require("orgmode").setup({
	org_todo_keyword_faces = {
		WAITING = ":foreground blue :weight bold",
		DELEGATED = ":background #FFFFFF :slant italic :underline on",
		TODO = ":background #000000 :foreground red", -- overrides builtin color for `TODO` keyword
	},
	org_agenda_files = { "~/m/vim" },
	org_default_notes_file = "~/m/vim/default.org",
	mappings = {
		global = {
			org_agenda = "ge",
			org_capture = "gt",
		},
	},
})
