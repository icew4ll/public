local colors = {
	base00 = "#32302f",
	-- base00 = "#2e3440",
	-- base00 = "#212121",
	-- base00 = "#282c34",
	base01 = "#cdd3de",
	base02 = "#a4b063",
	base03 = "#1c1c1c",
	base04 = "#565c64",
	base05 = "#c8ccd4",
	base06 = "#32302f",
	base07 = "#30343c",
	base08 = "#23272f",
	pink = "#b686ae",
	warn = "#f9929b",
	success = "#A3BE8C",
	error = "#BF616A",
	red = "#b40b11",
	orange = "#b4713d",
	soil = "#6b4e44",
	green = "#4a7533",
	cyan = "#5b9c90",
	blue = "#526f93",
	gold = "#fdbb12",
	purple = "#896a98",
	brown = "#9a806d",
	white = "#ffffff",
	light01 = "#E8EBF0",
}

colors.bg_active = colors.base00

return colors
