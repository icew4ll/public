return {
	sep = {
		-- left = "",
		-- right = "",
		-- left = "⚙️",
		-- right = "⚙️",
		left = " ",
		right = " ",
		space = " ",
	},
	diagnostic = {
		error = "🔮 ",
		warn = "🍯 ",
		info = "🌵 ",
	},
	diff = {
		add = " ",
		modified = " ",
		remove = " ",
	},
	ro = "🔐",
	edit = "🪶",
	-- git = ""
	-- git = " ",
	git = "🌱 ",
	-- lsp = "🖼️"
	lsp = "📖",
	-- lsp = "🦉"
	-- lsp = "⚡️"
}
