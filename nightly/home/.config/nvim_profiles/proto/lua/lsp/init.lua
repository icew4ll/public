-- require "lsp.diagnostic"
require("lsp.null")
require("lsp.signs")
local config = require("lspconfig")
local util = require("lspconfig/util")
-- local custom_attach = require("lsp.custom_attach")
local home = vim.fn.expand("$HOME")
local build = home .. "/bin/build/lua-language-server"
local bin = build .. "/bin/Linux/lua-language-server"

-- custom setup
config.sumneko_lua.setup({
	-- on_attach = function(client)
	-- 	custom_attach(client)
	-- end,
	globals = { "use" },
	cmd = { bin, "-E", build .. "/main.lua" },
	settings = {
		Lua = {
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				enable = true,
				globals = { "vim" },
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file("", true),
				checkThirdParty = false,
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = { enable = false },
		},
	},
})

config.jsonls.setup({
	cmd = { "vscode-json-languageserver", "--stdio" },
})

config.tsserver.setup({
	root_dir = util.root_pattern("package.json", "tsconfig.json", "jsconfig.json", ".git", vim.fn.getcwd()),
})

config.html.setup({
	filetypes = {
		"html",
		"jinja",
	},
})

-- config.pylsp.setup({
-- 	filetypes = {
-- 		"python",
-- 	},
-- })

config.pyright.setup({
	filetypes = {
		"python",
	},
})

config.rust_analyzer.setup({
	on_attach = function(client)
		client.resolved_capabilities.document_formatting = false
		client.resolved_capabilities.document_range_formatting = false
	end,
	settings = {
		["rust-analyzer"] = {
			-- cargo = {
			--     loadOutDirsFromCheck = true
			-- },
			-- procMacro = {
			--     enable = true
			-- },
			diagnostics = {
				disabled = {
					"macro-error",
					-- "unresolved-import"
					"unresolved-proc-macro",
				},
			},
		},
	},
})

-- local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
-- require("lspconfig")["null-ls"].setup({capabilities = capabilities})
-- config["null-ls"].setup({})
-- capabilities = require("cmp_nvim_lsp").update_capabilities(capabilities)
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

local servers = { "null-ls", "clangd", "vimls", "bashls", "dockerls", "vuels", "cssls" }

for _, lsp in ipairs(servers) do
	-- config[lsp].setup(vim.tbl_deep_extend("force", { capabilities = capabilities }, config))
	config[lsp].setup({
		-- on_attach = function(client)
		-- 	custom_attach(client)
		-- end,
	})
end
