local M = {}

local opts = { noremap = true, silent = false }

local generic_opts = {
	insert_mode = opts,
	normal_mode = opts,
	visual_mode = opts,
	visual_block_mode = opts,
	command_mode = opts,
	term_mode = { silent = false },
}

local mode_adapters = {
	insert_mode = "i",
	normal_mode = "n",
	term_mode = "t",
	visual_mode = "v",
	visual_block_mode = "x",
	command_mode = "c",
}

local keymappings = {
	insert_mode = {
		["jk"] = "<Esc>",
	},
	normal_mode = {
		["<Esc>"] = ":noh<CR>",
		["<Tab>"] = ":bnext<CR>",
		["<S-Tab>"] = ":bprevious<CR>",
		["tr"] = ":TroubleToggle<CR>",
		["tp"] = ":lua require('_peek').Peek('definition')<CR>",
		["ta"] = ":qa<CR>",
		["tl"] = ":Telescope<CR>",
		["tm"] = ":lua require('telescope').extensions.media_files.media_files()<CR>",
		["t,"] = ":VtrAttachToPane<CR>",
		["t."] = ":VtrSendLinesToRunner<CR>",
		["td"] = ":bdelete<CR>",
		["tn"] = ":Nnn<CR>",
		["tf"] = ":lua vim.lsp.buf.formatting()<CR>",
		["tu"] = ":PackerUpdate<CR>",
		["tc"] = ":PackerCompile<CR>",
		["tq"] = ":q<CR>",
		["ts"] = ":w<CR>",
	},
	visual_mode = {
		["t."] = ":VtrSendLinesToRunner<CR>",
		["<"] = "<gv",
		[">"] = ">gv",
		["J"] = ":m '>+1<CR>gv=gv",
		["K"] = ":m '<-2<CR>gv=gv",
	},
	term_mode = {},
	command_mode = {},
}

local lsp_keymappings = {
	normal_mode = {
		["gk"] = "<Cmd>lua vim.lsp.buf.hover()<CR>",
		["gl"] = "<Cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>",
		["gD"] = "<Cmd>lua vim.lsp.buf.declaration()<CR>",
		["gd"] = "<Cmd>lua vim.lsp.buf.definition()<CR>",
		["gi"] = "<Cmd>lua vim.lsp.buf.implementation()<CR>",
		["gr"] = "<Cmd>lua vim.lsp.buf.references()<CR>",
		["gm"] = "<Cmd>lua vim.lsp.buf.rename()<CR>",
		["ga"] = "<Cmd>lua vim.lsp.buf.code_actions()<CR>",
		["<C-k>"] = "<Cmd>lua vim.lsp.buf.signature_help()<CR>",
		["[d"] = "<Cmd>lua vim.lsp.diagnostic.goto_prev()<CR>",
		["]d"] = "<Cmd>lua vim.lsp.diagnostic.goto_next()<CR>",
		["[e"] = "<Cmd>Lspsaga diagnostic_jump_next<CR>",
		["]e"] = "<Cmd>Lspsaga diagnostic_jump_prev<CR>",
	},
}

function M.set_keymaps(mode, key, val)
	local opt = generic_opts[mode] and generic_opts[mode] or opts
	if type(val) == "table" then
		opt = val[2]
		val = val[1]
	end
	vim.api.nvim_set_keymap(mode, key, val, opt)
end

function M.map(mode, keymaps)
	mode = mode_adapters[mode] and mode_adapters[mode] or mode
	for k, v in pairs(keymaps) do
		M.set_keymaps(mode, k, v)
	end
end

function M.setup()
	for mode, mapping in pairs(keymappings) do
		M.map(mode, mapping)
	end
	for mode, mapping in pairs(lsp_keymappings) do
		M.map(mode, mapping)
	end
end

return M
