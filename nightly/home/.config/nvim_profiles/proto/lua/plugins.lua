local M = {}

function M.setup()
	local packer = require("packer")

	local conf = {
		compile_path = vim.fn.stdpath("config") .. "/lua/packer_compiled.lua",
	}

	local function plugins(use)
		-- Packer can manage itself as an optional plugin
		use({ "wbthomason/packer.nvim", opt = true })
		use({ "lewis6991/impatient.nvim" })
		use({ "voldikss/vim-floaterm", event = "BufEnter" })
		use({
			"eddyekofo94/gruvbox-flat.nvim",
			config = function()
				vim.api.nvim_command("colo gruvbox-flat")
			end,
		})
		use({
			"kristijanhusak/orgmode.nvim",
			config = function()
				require("orgmode").setup({})
			end,
		})
		use({
			"nvim-telescope/telescope.nvim",
			module = "telescope",
			cmd = "Telescope",
			requires = {
				{
					"nvim-telescope/telescope-fzf-native.nvim",
					run = "make",
				},
				{
					"nvim-telescope/telescope-media-files.nvim",
				},
			},
			config = function()
				require("_telescope")
			end,
		})
		use({
			"lukas-reineke/indent-blankline.nvim",
			event = "BufEnter",
		})
		use({
			"noib3/cokeline.nvim",
			requires = "kyazdani42/nvim-web-devicons", -- If you want devicons
			config = function()
				require("_coke")
			end,
		})
		use({
			"terrortylor/nvim-comment",
			config = function()
				require("nvim_comment").setup()
			end,
		})
		use({
			"onsails/lspkind-nvim",
		})
		use({
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate",
			config = function()
				require("_tree")
			end,
		})
		use({
			"hrsh7th/nvim-cmp",
			event = "BufRead",
			requires = {
				"hrsh7th/cmp-buffer",
				"hrsh7th/cmp-nvim-lsp",
				"hrsh7th/cmp-nvim-lua",
				"hrsh7th/cmp-path",
				"ray-x/cmp-treesitter",
				"hrsh7th/cmp-cmdline",
				"hrsh7th/cmp-nvim-lsp-document-symbol",
				-- "hrsh7th/cmp-calc",
				-- "f3fora/cmp-spell",
				-- "hrsh7th/cmp-emoji",
				-- "octaltree/cmp-look",
			},
			config = function()
				require("_cmp")
			end,
		})
		use({
			"kyazdani42/nvim-web-devicons",
		})
		use({
			"glepnir/galaxyline.nvim",
			requires = "kyazdani42/nvim-web-devicons", -- If you want devicons
			branch = "main",
			config = function()
				require("_galaxy")
			end,
		})
		use({ "folke/lua-dev.nvim", event = "VimEnter" })
		use({
			"jose-elias-alvarez/null-ls.nvim",
		})
		use({
			"neovim/nvim-lspconfig",
			opt = true,
			event = "BufReadPre",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("lsp")
				-- require("dap").setup()
			end,
		})
	end
	packer.init(conf)
	packer.startup(plugins)
end

return M
