local M = {}

function M.setup()
	local packer = require("packer")

	local conf = {
		compile_path = vim.fn.stdpath("config") .. "/lua/packer_compiled.lua",
	}
	local function plugins(use)
		-- Packer can manage itself as an optional plugin
		use({ "wbthomason/packer.nvim", opt = true })
		use({ "lewis6991/impatient.nvim" })
		use({
			"vmchale/ion-vim",
			ft = "ion",
			event = "BufEnter",
		})
		use({ "is0n/fm-nvim", event = "BufEnter" })
		-- use({
		-- 	"phaazon/hop.nvim",
		-- 	branch = "v1", -- optional but strongly recommended
		-- 	config = function()
		-- 		-- you can configure Hop the way you like here; see :h hop-config
		-- 		require("hop").setup({ keys = "etovxqpdygfblzhckisuran" })
		-- 	end,
		-- })
		-- use({
		-- 	"jghauser/kitty-runner.nvim",
		-- 	event = "BufEnter",
		-- 	config = function()
		-- 		require("kitty-runner").setup()
		-- 	end,
		-- })
		use({
			"ggandor/lightspeed.nvim",
			event = "BufReadPost",
			-- branch = "smart-autojump",
			config = function()
				require("_lightspeed")
			end,
		})
		use({
			"lukas-reineke/headlines.nvim",
			ft = "org",
			event = "BufEnter",
		})
		use({
			"folke/trouble.nvim",
			event = "BufReadPre",
			cmd = { "TroubleToggle", "Trouble" },
			requires = "kyazdani42/nvim-web-devicons",
		})
		use({
			"akinsho/org-bullets.nvim",
			ft = "org",
			event = "BufEnter",
			config = function()
				require("org-bullets").setup({
					symbols = { "◉", "○", "✸", "✿" },
				})
			end,
		})
		use({
			"eddyekofo94/gruvbox-flat.nvim",
			event = "BufEnter",
			config = function()
				vim.api.nvim_command("colo gruvbox-flat")
			end,
		})
		use({
			"rafamadriz/friendly-snippets",
			event = "InsertEnter",
		})
		use({
			"kristijanhusak/orgmode.nvim",
		})
		use({
			"christoomey/vim-tmux-runner",
			event = "BufEnter",
		})
		use({
			"nvim-telescope/telescope.nvim",
			requires = {
				{
					"nvim-telescope/telescope-fzf-native.nvim",
					run = "make",
				},
				{
					"nvim-telescope/telescope-media-files.nvim",
				},
			},
			config = function()
				require("_telescope")
			end,
		})
		use({
			"lukas-reineke/indent-blankline.nvim",
			event = "BufEnter",
		})
		use({
			"noib3/cokeline.nvim",
			event = "BufEnter",
			requires = "kyazdani42/nvim-web-devicons", -- If you want devicons
			config = function()
				require("_coke")
			end,
		})
		use({
			"terrortylor/nvim-comment",
			config = function()
				require("nvim_comment").setup()
			end,
		})
		use({
			"onsails/lspkind-nvim",
		})
		use({
			"nvim-treesitter/nvim-treesitter",
			run = ":TSUpdate",
			config = function()
				require("_tree")
			end,
		})
		use({
			"L3MON4D3/LuaSnip",
			-- event = "BufEnter",
			after = "nvim-cmp",
			wants = "friendly-snippets",
			config = function()
				require("_luasnip")
				require("_snips")
			end,
		})
		use({
			"saadparwaiz1/cmp_luasnip",
			after = "LuaSnip",
		})
		use({
			"hrsh7th/nvim-cmp",
			after = "friendly-snippets",
			event = "BufRead",
			requires = {
				"hrsh7th/cmp-buffer",
				"lukas-reineke/cmp-rg",
				"hrsh7th/cmp-nvim-lsp",
				"hrsh7th/cmp-nvim-lua",
				"hrsh7th/cmp-path",
				"ray-x/cmp-treesitter",
				"hrsh7th/cmp-cmdline",
				"hrsh7th/cmp-nvim-lsp-document-symbol",
				"hrsh7th/cmp-calc",
				"f3fora/cmp-spell",
				"hrsh7th/cmp-emoji",
				"octaltree/cmp-look",
			},
			-- config = function()
			-- 	require("_cmp")
			-- end,
			config = function()
				require("_cmp").setup()
			end,
		})
		use({
			"windwp/nvim-autopairs",
			event = "BufRead",
			run = "make",
			config = function()
				require("nvim-autopairs").setup({
					disable_filetype = { "TelescopePrompt", "vim" },
				})

				local cmp_autopairs = require("nvim-autopairs.completion.cmp")
				local cmp = require("cmp")
				cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))
			end,
		})
		use({
			"kyazdani42/nvim-web-devicons",
		})
		use({
			"glepnir/galaxyline.nvim",
			requires = "kyazdani42/nvim-web-devicons", -- If you want devicons
			branch = "main",
			config = function()
				require("_galaxy")
			end,
		})
		use({ "folke/lua-dev.nvim", event = "VimEnter" })
		use({
			"jose-elias-alvarez/null-ls.nvim",
		})
		use({
			"neovim/nvim-lspconfig",
			opt = true,
			event = "BufReadPre",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("lsp")
				-- require("dap").setup()
			end,
		})
	end
	packer.init(conf)
	packer.startup(plugins)
end

return M
