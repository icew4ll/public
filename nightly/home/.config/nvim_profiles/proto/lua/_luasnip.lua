local present, luasnip = pcall(require, "luasnip")
if not present then
	return
end

luasnip.config.set_config({
	history = true,
	updateevents = "TextChanged,TextChangedI",
})

require("luasnip/loaders/from_vscode").load()
-- https://github.com/L3MON4D3/Dotfiles
-- lua print(vim.o.runtimepath)
-- https://github.com/L3MON4D3/LuaSnip/issues/201
-- https://github.com/L3MON4D3/LuaSnip/blob/5b2c1dba655818d6d9724aaa945e397ae3d21b63/Examples/snippets.lua#L160
-- https://github.com/L3MON4D3/LuaSnip/issues/186
-- https://github.com/L3MON4D3/LuaSnip/issues/212
-- :luafile /tmp/snip/snip.lua
