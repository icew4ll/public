local M = {}

function M.setup()
	local cmp = require("cmp")

	cmp.setup({
		formatting = {
			format = require("lspkind").cmp_format({
				with_text = true,
				menu = {
					buffer = "[Buffer]",
					nvim_lsp = "[LSP]",
					nvim_lua = "[Lua]",
					path = "[Path]",
					treesitter = "[tree]",
					luasnip = "[LuaSnip]",
					look = "[Look]",
					spell = "[Spell]",
					calc = "[Calc]",
					emoji = "[Emoji]",
					rg = "[RG]",
					-- ultisnips = "[UltiSnips]",
					-- cmp_tabnine = "[TabNine]",
					-- neorg = "[Neorg]",
				},
			}),
		},
		mapping = {
			["<C-p>"] = cmp.mapping.select_prev_item(),
			["<C-n>"] = cmp.mapping.select_next_item(),
			["<C-d>"] = cmp.mapping.scroll_docs(-4),
			["<C-f>"] = cmp.mapping.scroll_docs(4),
			["<C-Space>"] = cmp.mapping.complete(),
			["<C-e>"] = cmp.mapping.close(),
			["<CR>"] = cmp.mapping.confirm({
				behavior = cmp.ConfirmBehavior.Replace,
				select = true,
			}),
			["<Tab>"] = function(fallback)
				if cmp.visible() then
					cmp.select_next_item()
				elseif require("luasnip").expand_or_jumpable() then
					vim.fn.feedkeys(
						vim.api.nvim_replace_termcodes("<Plug>luasnip-expand-or-jump", true, true, true),
						""
					)
				else
					fallback()
				end
			end,
			["<S-Tab>"] = function(fallback)
				if cmp.visible() then
					cmp.select_prev_item()
				elseif require("luasnip").jumpable(-1) then
					vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<Plug>luasnip-jump-prev", true, true, true), "")
				else
					fallback()
				end
			end,
		},
		snippet = {
			expand = function(args)
				-- vim.fn["UltiSnips#Anon"](args.body)
				require("luasnip").lsp_expand(args.body) -- For `luasnip` users.
			end,
		},
		sources = {
			{ name = "buffer", keyword_length = 3 },
			{ name = "nvim_lsp" },
			{ name = "nvim_lua" },
			{ name = "path" },
			{ name = "treesitter" },
			{ name = "luasnip" }, -- For luasnip users.
			{ name = "calc" },
			{ name = "spell" },
			{ name = "emoji" },
			{ name = "rg" },
			{ name = "look", keyword_length = 2, opts = { convert_case = true, loud = true } },
			-- { name = "look" },
			-- { name = "neorg" },
			-- { name = "crates" },
			-- { name = "ultisnips" },
			-- { name = "cmp_tabnine" },
		},
		completion = { completeopt = "menu,menuone,noinsert" },
		experimental = { native_menu = false, ghost_text = false },
	})

	-- If you want insert `(` after select function or method item
	local cmp_autopairs = require("nvim-autopairs.completion.cmp")
	local cmp = require("cmp")
	cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))

	-- TabNine
	-- local tabnine = require("cmp_tabnine.config")
	-- tabnine:setup({ max_lines = 1000, max_num_results = 20, sort = true })

	-- Use cmdline & path source for ':'.
	cmp.setup.cmdline(":", {
		sources = cmp.config.sources({
			{ name = "path" },
		}, {
			{ name = "cmdline" },
		}),
	})

	-- lsp_document_symbols
	cmp.setup.cmdline("/", {
		sources = cmp.config.sources({
			{ name = "nvim_lsp_document_symbol" },
		}, {
			{ name = "buffer" },
		}),
	})

	-- Database completion
	-- vim.api.nvim_exec(
	-- 	[[
	--         autocmd FileType sql,mysql,plsql lua require('cmp').setup.buffer({ sources = {{ name = 'vim-dadbod-completion' }} })
	--     ]],
	-- 	false
	-- )
end

return M
