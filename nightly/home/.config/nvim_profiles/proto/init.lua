local fn = vim.fn
local execute = vim.api.nvim_command
local base = vim.fn.stdpath("config")
local backup_dir = base .. "/tmp/backup"
local swap_dir = base .. "/tmp/swap"
local undo_dir = base .. "/tmp/undo"
local cmd = vim.cmd
local o = vim.o
local opt = vim.opt

local function undo()
	if fn.empty(fn.glob(backup_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. backup_dir)
	end

	if fn.empty(fn.glob(swap_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. swap_dir)
	end

	if fn.empty(fn.glob(undo_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. undo_dir)
	end

	opt.swapfile = false
	o.undofile = true
	o.backup = true
	o.writebackup = true
	o.backupdir = backup_dir
	o.undodir = undo_dir
end

local function post()
	vim.cmd([[
		rshada!
		doautocmd BufRead
		syntax on
		filetype on
		filetype plugin indent on
		PackerLoad impatient.nvim
	]])
	opt.spell = true
	opt.spelllang = { "en_us" }
end

local function prepare()
	vim.cmd([[
        syntax off
        filetype off
        filetype plugin indent off
    ]])
	vim.g.loaded_gzip = false
	vim.g.loaded_matchit = false
	-- vim.g.loaded_netrwPlugin = false -- needed to open files in org mode
	vim.g.loaded_tarPlugin = false
	vim.g.loaded_zipPlugin = false
	vim.g.loaded_man = false
	vim.g.loaded_2html_plugin = false
	vim.g.loaded_remote_plugins = false
end

local function packer_init()
	local install_path = fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"
	if fn.empty(fn.glob(install_path)) > 0 then
		execute("!git clone https://github.com/wbthomason/packer.nvim " .. install_path)
	end
	vim.cmd([[packadd! packer.nvim]])
	vim.cmd("autocmd BufWritePost plugins.lua PackerCompile")
end

local function sys_init()
	-- Performance
	require("impatient")
end

----- Start loading ----------

sys_init()
require("packer_compiled")
packer_init()
prepare()

vim.defer_fn(function()
	require("_defaults").setup()
	require("_settings").setup()
	require("_mappings").setup()
	require("_plugins").setup()
	post()
end, 0)

undo()
----- End loading ----------
