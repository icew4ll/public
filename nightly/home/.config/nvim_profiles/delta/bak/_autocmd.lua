local utils = require("utils")

local autocmds = {
    core = {}
}

-- Set relative numbers
table.insert(
    autocmds["core"],
    {
        "BufEnter,WinEnter",
        "*",
        "if &nu | set rnu | endif"
    }
)

-- Enable highlight on yank
table.insert(
    autocmds["core"],
    {
        {
            "TextYankPost",
            "*",
            "lua require('vim.highlight').on_yank({higroup = 'Search', timeout = 200})"
        }
    }
)

-- Install plugins on launch
vim.defer_fn(
    function()
        -- Check if there is only packer installed so we can decide if we should
        -- use PackerInstall or PackerSync, useful for generating the
        -- `plugin/packer_compiled.lua` on first doom launch
        if vim.tbl_count(vim.fn.globpath(vim.fn.stdpath("data") .. "/site/pack/packer/opt", "*", 0, 1)) == 1 then
            vim.cmd("PackerSync")
        else
            -- Clean disabled plugins
            vim.cmd("PackerClean")
            -- Install the plugins
            vim.cmd("PackerInstall")
        end
    end,
    200
)

-- Preserve last editing position
table.insert(
    autocmds["core"],
    {
        "BufReadPost",
        "*",
        [[
            if line("'\"") > 1 && line("'\"") <= line("$") |
                exe "normal! g'\"" |
            endif
        ]]
    }
)

-- Create augroups
utils.create_augroups(autocmds)
