local packer_path = vim.fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"

if vim.fn.empty(vim.fn.glob(packer_path)) > 0 then
    vim.fn.system(
        {
            "git",
            "clone",
            "https://github.com/wbthomason/packer.nvim",
            packer_path
        }
    )
end

-- Load packer
vim.cmd([[ packadd packer.nvim ]])
local packer = require("packer")

-- Change some defaults
packer.init(
    {
        git = {
            clone_timeout = 300 -- 5 mins
        },
        profile = {
            enable = true
        }
    }
)

packer.startup(
    function(use)
        -- Plugins manager
        use(
            {
                "wbthomason/packer.nvim",
                opt = true
            }
        )

        -- Tree-Sitter
        use(
            {
                "nvim-treesitter/nvim-treesitter",
                opt = true,
                run = ":TSUpdate",
                config = function()
                    require "nvim-treesitter.configs".setup {
                        ensure_installed = {
                            "json",
                            "css",
                            "typescript",
                            --"javascript",
                            "html",
                            "tsx",
                            "yaml",
                            "rust",
                            "python",
                            "bash",
                            "lua",
                            "regex",
                            "vue",
                            "graphql",
                            "toml",
                            "go"
                        },
                        highlight = {
                            enable = true
                        }
                    }
                end
            }
        )

        -- Neorg
        use(
            {
                "vhyrro/neorg",
                branch = "unstable",
                after = {"nvim-treesitter"}
            }
        )

        -- Development icons
        use(
            {
                "kyazdani42/nvim-web-devicons",
                module = "nvim-web-devicons"
            }
        )

        -- File tree
        use(
            {
                "kyazdani42/nvim-tree.lua",
                requires = "nvim-web-devicons",
                cmd = {
                    "NvimTreeClipboard",
                    "NvimTreeClose",
                    "NvimTreeFindFile",
                    "NvimTreeOpen",
                    "NvimTreeRefresh",
                    "NvimTreeToggle"
                }
            }
        )

        -- Statusline
        -- can be disabled to use your own statusline
        use(
            {
                "glepnir/galaxyline.nvim",
                requires = "nvim-web-devicons",
                branch = "main",
                config = function()
                    require("_galaxy")
                end
            }
        )

        -- Tabline
        use(
            {
                "akinsho/nvim-bufferline.lua",
                event = {"VimEnter *"},
                config = function()
                    require("_bufferline")
                end
            }
        )

        -- Viewer & finder for LSP symbols and tags
        use(
            {
                "simrat39/symbols-outline.nvim",
                cmd = {
                    "SymbolsOutline",
                    "SymbolsOutlineOpen",
                    "SymbolsOutlineClose"
                }
            }
        )
        use(
            {
                "nvim-lua/plenary.nvim",
                module = "plenary"
            }
        )
        use(
            {
                "nvim-lua/popup.nvim",
                module = "popup"
            }
        )

        use(
            {
                "nvim-telescope/telescope.nvim",
                cmd = "Telescope",
                module = "telescope",
                requires = {
                    "popup.nvim",
                    "plenary.nvim"
                },
                config = function()
                    require("_telescope")
                end
            }
        )

        use(
            {
                "lewis6991/gitsigns.nvim",
                requires = "plenary.nvim",
                event = "BufRead"
            }
        )

        -- LazyGit integration
        use(
            {
                "kdheepak/lazygit.nvim",
                requires = "plenary.nvim",
                cmd = {"LazyGit", "LazyGitConfig"}
            }
        )

        use(
            {
                "neovim/nvim-lspconfig",
                config = function()
                    require("lsp")
                end
            }
        )

        -- Built-in LSP Config
        use(
            {
                "onsails/lspkind-nvim",
                event = "ColorScheme",
                config = function()
                    require("lspkind").init()
                end
            }
        )

        -- Completion plugin
        -- can be disabled to use your own completion plugin
        use(
            {
                "hrsh7th/nvim-compe",
                requires = {
                    {
                        "ray-x/lsp_signature.nvim"
                    }
                },
                config = function()
                    require("_compe")
                end,
                after = "nvim-lspconfig"
            }
        )

        -- Snippets
        use(
            {
                "L3MON4D3/LuaSnip",
                requires = {"rafamadriz/friendly-snippets"},
                event = "BufWinEnter"
            }
        )

        -- install lsp saga
        use(
            {
                "glepnir/lspsaga.nvim",
                opt = true,
                after = "nvim-lspconfig"
            }
        )

        use(
            {
                "lambdalisue/suda.vim",
                cmd = {"SudaRead", "SudaWrite"}
            }
        )

        -- Autopairs
        -- can be disabled to use your own autopairs
        use(
            {
                "windwp/nvim-autopairs",
                event = "InsertEnter",
                setup = function()
                    vim.cmd [[packadd nvim-autopairs]]
                    require "nvim-autopairs".setup()
                end
            }
        )

        -- Surround
        -- can be disabled to use your own autopairs
        use(
            {
                "tpope/vim-surround"
            }
        )

        -- ion
        use(
            {
                "vmchale/ion-vim"
            }
        )

        -- emmett
        use(
            {
                "mattn/emmet-vim"
            }
        )

        -- markdown
        use(
            {
                "masukomi/vim-markdown-folding"
            }
        )
        use(
            {
                "tpope/vim-markdown"
            }
        )
        use(
            {
                "monaqa/dial.nvim"
            }
        )
        use(
            {
                "chmp/mdnav"
            }
        )

        -- vlang
        use(
            {
                "cheap-glitch/vim-v"
            }
        )

        -- closetag
        use(
            {
                "alvan/vim-closetag",
                setup = function()
                    local g = vim.g
                    g.closetag_filenames = "*.html,*.tsx"
                end
            }
        )

        -- Indent Lines
        use(
            {
                "lukas-reineke/indent-blankline.nvim",
                event = "BufEnter"
            }
        )

        -- Comment
        use(
            {
                "terrortylor/nvim-comment",
                event = "BufEnter",
                config = function()
                    require("nvim_comment").setup(
                        {
                            -- Linters prefer comment and line to hae a space in between
                            left_marker_padding = true,
                            -- should comment out empty or whitespace only lines
                            comment_empty = true,
                            -- Should key mappings be created
                            create_mappings = true,
                            -- Normal mode mapping left hand side
                            line_mapping = "gcc",
                            -- Visual/Operator mapping left hand side
                            operator_mapping = "gc"
                        }
                    )
                end
            }
        )

        -- floaterm
        use(
            {
                "voldikss/vim-floaterm",
                config = function()
                    local g = vim.g
                    g.floaterm_width = 0.9
                    g.floaterm_opener = "edit"
                    vim.api.nvim_set_keymap(
                        "n",
                        "<leader>n",
                        ":FloatermNew nnn -e<CR>",
                        {noremap = true, silent = true}
                    )
                end
            }
        )

        -- tmux
        use(
            {
                "christoomey/vim-tmux-runner"
            }
        )

        -- nlua
        use(
            {
                "tjdevries/nlua.nvim",
                requires = {
                    "euclidianAce/BetterLua.vim"
                }
            }
        )

        -- gruvbox
        use(
            {
                "eddyekofo94/gruvbox-flat.nvim",
                config = function()
                    vim.api.nvim_command("colo gruvbox-flat")
                end
            }
        )

        -- Fastest colorizer without external dependencies!
        use(
            {
                "norcalli/nvim-colorizer.lua",
                event = "ColorScheme",
                config = function()
                    require "colorizer".setup {
                        css = {rgb_fn = true},
                        scss = {rgb_fn = true},
                        sass = {rgb_fn = true},
                        stylus = {rgb_fn = true},
                        vim = {names = true},
                        tmux = {names = false},
                        "javascript",
                        "javascriptreact",
                        "typescript",
                        "typescriptreact",
                        "conf",
                        lua = {rgb_fn = true}
                    }
                end
            }
        )
    end
)

require "_settings"
require "_mapping"
require "_autocmd"
