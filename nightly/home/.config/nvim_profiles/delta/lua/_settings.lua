local o = vim.o
local g = vim.g
local wo = vim.wo
local bo = vim.bo
-- local config = vim.fn.expand("$XDG_CONFIG_HOME")
local home = vim.fn.expand("$HOME")
local fn = vim.fn -- to call Vim functions e.g. fn.bufnr()
local cmd = vim.cmd
local py = home .. "/.pyenv/versions/3.9.1/bin/python3.9"

-- create backup/swap/undo directories
local base = vim.fn.stdpath('config') .. "/tmp/"
local backup_dir = base .. "backup"
local swap_dir = base .. "swap"
local undo_dir = base .. "undo"

if fn.empty(fn.glob(backup_dir)) > 0 then
    cmd("!mkdir -p " .. " " .. backup_dir)
end

if fn.empty(fn.glob(swap_dir)) > 0 then
    cmd("!mkdir -p" .. " " .. swap_dir)
end

if fn.empty(fn.glob(undo_dir)) > 0 then
    cmd("!mkdir -p" .. " " .. undo_dir)
end

g.python3_host_prog = py
g.node_host_prog = home .. "/.volta/tools/image/packages/neovim/bin/neovim-node-host"

-- g.mapleader = " "
-- g.mapleader = "t"
o.termguicolors = true
cmd "set noswapfile"
cmd "set wrap linebreak nolist"

-- markdown
-- wo.conceallevel = 2
-- o.updatetime = 50
-- o.hidden = true
-- enable native markdown folding
-- g.markdown_folding = 1
g.markdown_fenced_languages = {
    "js=javascript",
    "javascript",
    "typescript",
    "html",
    "python",
    "bash"
}

-- basic
o.clipboard = "unnamedplus"
wo.number = true
wo.relativenumber = true
g.noshowmode = true
o.tabstop = 2
o.shiftwidth = 2
o.expandtab = true
bo.expandtab = true
bo.tabstop = 2
o.smartindent = true
bo.smartindent = true
bo.shiftwidth = 2
o.splitbelow = true
o.splitright = true
o.ignorecase = true
o.smartcase = true
g.diagnostic_enable_virtual_text = 1
g.diagnostic_trimmed_virtual_text = "40"
g.diagnostic_insert_delay = 1
o.updatetime = 300
o.foldlevel = 99

-- Backup, undo, swap options
-- bo.undofile = true
-- vim.opt.undofile = true
o.backup = true
o.writebackup = true
o.backupdir = backup_dir
o.directory = swap_dir .. o.directory
o.undodir = undo_dir

-- Global options
vim.opt.hidden = true
vim.opt.updatetime = 200
vim.opt.timeoutlen = 500
vim.opt.completeopt = {
    "menu",
    "menuone",
    "preview",
    "noinsert",
    "noselect"
}
vim.opt.shortmess:append("atsc")
-- vim.opt.shortmess = "a"
vim.opt.inccommand = "split"
vim.opt.path = "**"
vim.opt.signcolumn = "yes"

-- Buffer options
vim.opt.copyindent = true
vim.opt.preserveindent = true

-- vim.opt.cursorline = true
vim.opt.mouse = "a"
