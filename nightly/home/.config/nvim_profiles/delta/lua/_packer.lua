local fn = vim.fn -- to call Vim functions e.g. fn.bufnr()

-- Install packer
local execute = vim.api.nvim_command
local install_path = fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"

if fn.empty(fn.glob(install_path)) > 0 then
    execute("!git clone https://github.com/wbthomason/packer.nvim" .. " " .. install_path)
end

-- Only required if you have packer in your `opt` pack
vim.cmd [[packadd packer.nvim]]

local use = require("packer").use
require("packer").startup(
    function()
        -- Packer can manage itself as an optional plugin
        use {"wbthomason/packer.nvim", opt = true}

        -- navigation
        use {"phaazon/hop.nvim"}

        -- statusbar
        use {
            "glepnir/galaxyline.nvim",
            requires = "kyazdani42/nvim-web-devicons",
            branch = "main",
            config = function()
                require("_galaxy")
            end
        }
        -- indent guides
        use {"lukas-reineke/indent-blankline.nvim"}

        -- buffers
        use {
            "akinsho/nvim-bufferline.lua",
            event = {"VimEnter *"},
            requires = "kyazdani42/nvim-web-devicons",
            config = function()
                require("_bufferline")
            end
        }

        -- use "HallerPatrick/py_lsp.nvim"
        use {
            "folke/lsp-colors.nvim",
            opt = true,
            config = function()
                require("lsp-colors").setup(
                    {
                        Error = "#db4b4b",
                        Warning = "#e0af68",
                        Information = "#0db9d7",
                        Hint = "#10B981"
                    }
                )
            end
        }
        use {
            "folke/trouble.nvim"
        }

        -- lsp
        use {
            "neovim/nvim-lspconfig",
            config = function()
                require("lsp")
            end
        }
        use "nvim-lua/lsp_extensions.nvim"
        use {
            "onsails/lspkind-nvim",
            config = function()
                require("lspkind").init()
            end
        }
        use {
            "tjdevries/nlua.nvim",
            requires = {
                "euclidianAce/BetterLua.vim"
            }
        }

        -- completion
        use {
            "hrsh7th/nvim-compe",
            config = function()
                require("_compe")
            end,
            requires = {
                "hrsh7th/vim-vsnip",
                "hrsh7th/vim-vsnip-integ"
            }
        }

        -- fzf
        use {
            "ibhagwan/fzf-lua",
            -- config = function()
            --     require("_compe")
            -- end,
            requires = {
                "vijaymarupudi/nvim-fzf",
                "vijaymarupudi/nvim-fzf-commands"
            }
        }

        use {
            "nvim-treesitter/nvim-treesitter",
            run = ":TSUpdate",
            -- event = {"BufRead *"},
            config = function()
                require "nvim-treesitter.configs".setup {
                    ensure_installed = {
                        "json",
                        "css",
                        "typescript",
                        --"javascript",
                        "html",
                        "tsx",
                        "yaml",
                        "rust",
                        "python",
                        "bash",
                        "lua",
                        "regex",
                        "vue",
                        "graphql",
                        "toml",
                        "go"
                    },
                    highlight = {
                        enable = true
                    }
                }
            end
        }

        -- markdown
        use "masukomi/vim-markdown-folding"
        use "tpope/vim-markdown"
        use "monaqa/dial.nvim"
        use "chmp/mdnav"

        -- commenting
        use {
            "terrortylor/nvim-comment",
            config = function()
                require("nvim_comment").setup(
                    {
                        -- Linters prefer comment and line to hae a space in between
                        left_marker_padding = true,
                        -- should comment out empty or whitespace only lines
                        comment_empty = true,
                        -- Should key mappings be created
                        create_mappings = true,
                        -- Normal mode mapping left hand side
                        line_mapping = "gcc",
                        -- Visual/Operator mapping left hand side
                        operator_mapping = "gc"
                    }
                )
            end
        }

        -- syntax
        use "vmchale/ion-vim"
        use "mattn/emmet-vim"
        use "tpope/vim-surround"
        use {
            "liuchengxu/vista.vim",
            config = function()
                vim.g.vista_icon_indent = {"╰─ ", "├─ "}
            end
        }
        use "cheap-glitch/vim-v"

        -- autopairs
        use {
            "windwp/nvim-autopairs",
            event = {"BufRead *"},
            setup = function()
                vim.cmd [[packadd nvim-autopairs]]
                require "nvim-autopairs".setup()
            end
        }
        use {
            "alvan/vim-closetag",
            setup = function()
                local g = vim.g
                g.closetag_filenames = "*.html,*.tsx"
            end
        }

        -- floaterm
        use {
            "voldikss/vim-floaterm",
            config = function()
                local g = vim.g
                g.floaterm_width = 0.9
                g.floaterm_opener = "edit"
                vim.api.nvim_set_keymap("n", "<leader>n", ":FloatermNew nnn -e<CR>", {noremap = true, silent = true})
            end
        }

        -- tmux
        use "christoomey/vim-tmux-runner"

        -- theming, ui
        use {
            "norcalli/nvim-colorizer.lua",
            config = function()
                require "colorizer".setup {
                    css = {rgb_fn = true},
                    scss = {rgb_fn = true},
                    sass = {rgb_fn = true},
                    stylus = {rgb_fn = true},
                    vim = {names = true},
                    tmux = {names = false},
                    "javascript",
                    "javascriptreact",
                    "typescript",
                    "typescriptreact",
                    "conf",
                    lua = {rgb_fn = true},
                    html = {
                        mode = "foreground"
                    }
                }
            end
        }

        use {
            "eddyekofo94/gruvbox-flat.nvim",
            config = function()
                vim.api.nvim_command("colo gruvbox-flat")
            end
        }
        -- use {
        --     "Mofiqul/dracula.nvim",
        --     config = function()
        --         vim.api.nvim_command("colo dracula")
        --     end
        -- }
    end
)
