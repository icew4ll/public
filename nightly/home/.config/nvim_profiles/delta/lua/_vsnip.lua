local cmd = vim.cmd

cmd "let g:vsnip_filetypes = {}"
cmd "let g:vsnip_filetypes.javascriptreact = ['javascript']"
cmd "let g:vsnip_filetypes.typescriptreact = ['typescript']"

cmd [[imap <expr> <C-k>   vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-l>']]
cmd [[smap <expr> <C-k>   vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-l>']]
cmd [[imap <expr> <Tab> pumvisible() ? "\<C-n>" : vsnip#jumpable(1)   ?  '<Plug>(vsnip-jump-next)'      : '<Tab>']]
cmd [[smap <expr> <Tab> pumvisible() ? "\<C-n>" : vsnip#jumpable(1)   ?  '<Plug>(vsnip-jump-next)'      : '<Tab>']]
cmd [[imap <expr> <S-Tab> pumvisible() ? "\<C-p>" : vsnip#jumpable(-1)  ?  '<Plug>(vsnip-jump-prev)'      : '<S-Tab>']]
cmd [[smap <expr> <S-Tab> pumvisible() ? "\<C-p>" : vsnip#jumpable(-1)  ?  '<Plug>(vsnip-jump-prev)'      : '<S-Tab>']]
cmd [[nmap        s   <Plug>(vsnip-select-text)]]
cmd [[xmap        s   <Plug>(vsnip-select-text)]]
cmd [[nmap        S   <Plug>(vsnip-cut-text)]]
cmd [[xmap        S   <Plug>(vsnip-cut-text)]]
