local utils = require("utils")
local opts = {silent = true}

utils.map("n", "<Space>", ":WhichKey <leader><CR>", opts)

local lsp_opts = vim.tbl_extend("force", opts, {expr = true})

-- quick
utils.map("n", "ts", ":w<CR>", opts)
utils.map("n", "tt", ":ToggleTerm<CR>", opts)
utils.map("n", "tq", ":q<CR>", opts)
utils.map("n", "ta", ":qa<CR>", opts)
utils.map("n", "tn", ":FloatermNew nnn -e<CR>", opts)
utils.map("n", "tc", ":PackerCompile<CR>", opts)
utils.map("n", "tu", ":PackerUpdate<cr>", opts)
utils.map("n", "tf", ":lua vim.lsp.buf.formatting()<cr>", opts)
utils.map("n", "to", ":VsnipOpen<cr>", opts)
utils.map("n", "gd", ":lua vim.lsp.buf.definition()<CR>", opts) -- gd: jump to definition
utils.map("n", "gr", ":lua vim.lsp.buf.references()<CR>", opts) -- gr: go to reference
utils.map("n", "gi", ":lua vim.lsp.buf.implementation()<CR>", opts) -- gi: buf implementation

-- tmux
utils.map("n", "t,", ":VtrAttachToPane<CR>", {})
utils.map("n", "t.", ":VtrSendLinesToRunner<CR>", {})
utils.map("v", "t.", ":VtrSendLinesToRunner<CR>", {})

-- https://github.com/hrsh7th/nvim-compe#mappings
utils.map("i", "<C-Space>", "compe#complete()", lsp_opts)
utils.map("i", "<CR>", 'compe#confirm("<CR>")', lsp_opts)
utils.map("i", "<C-e>", 'compe#close("<C-e>")', lsp_opts)
utils.map("i", "<C-f>", 'compe#scroll({ "delta": +4 })', lsp_opts)
utils.map("i", "<C-d>", 'compe#scroll({ "delta": -4 })', lsp_opts)

-- TAB to cycle buffers too, why not?
utils.map("n", "<Tab>", ":bnext<CR>", opts)
utils.map("n", "<S-Tab>", ":bprevious<CR>", opts)
utils.map("n", "z,", "za", {})
utils.map("v", "z,", "za", {})
utils.map("n", "zm", "zM", {})
utils.map("v", "zm", "zM", {})
utils.map("n", "zn", "zR", {})
utils.map("v", "zn", "zR", {})

-- ESC to turn off search highlighting
utils.map("n", "<esc>", ":noh<CR>", opts)

--- F<n> keybindings
utils.map("n", "<F2>", ":SymbolsOutline<CR>", opts)
utils.map("n", "<F3>", ":NvimTreeToggle<CR>", opts)
utils.map("n", "<F5>", ":MinimapToggle<CR>", opts)
utils.map("n", "<F6>", ":TZAtaraxis<CR>", opts)
utils.map("n", "<F7>", ":<Plug>RestNvim<CR>", opts)

utils.map("n", "<C-h>", "<C-w>h", opts)
utils.map("n", "<C-j>", "<C-w>j", opts)
utils.map("n", "<C-k>", "<C-w>k", opts)
utils.map("n", "<C-l>", "<C-w>l", opts)

utils.map("x", "K", ":move '<-2<CR>gv-gv", opts)
utils.map("x", "J", ":move '>+1<CR>gv-gv", opts)

vim.cmd("tnoremap <Esc> <C-\\><C-n>")

vim.cmd(
    [[
  nnoremap <silent> <C-Up>    :resize +2<CR>
  nnoremap <silent> <C-Down>  :resize -2<CR>
  nnoremap <silent> <C-Right> :vertical resize -2<CR>
  nnoremap <silent> <C-Left>  :vertical resize +2<CR>
]]
)

utils.map("n", "<c-z>", "<Nop>", opts)

-- Disable accidentally pressing ctrl-z and suspending
utils.map("n", "<c-z>", "<Nop>", opts)

-- Disable ex mode
utils.map("n", "Q", "<Nop>", opts)

-- Misc
utils.map("n", "<leader>`", "<cmd>e #<CR>", opts)
utils.map("n", "<leader><space>", "<cmd>Telescope find_files<CR>", opts)
utils.map("n", "<leader>.", "<cmd>Telescope file_browser<CR>", opts)
utils.map("n", "<leader>,", "<cmd>Telescope buffers show_all_buffers=true<CR>", opts)
utils.map("n", "<leader>/", "<cmd>Telescope live_grep<CR>", opts)
utils.map("n", "<leader>:", "<cmd>Telescope command_history<CR>", opts)

-- Buffers
utils.map("n", "<leader>bc", '<cmd>lua require("bufferline").handle_close_buffer(vim.fn.bufnr("%"))<CR>', opts)
utils.map("n", "<leader>bb", "<cmd>e #<CR>", opts)
utils.map("n", "<leader>b]", '<cmd>lua require("bufferline").cycle(1)<CR>', opts)
utils.map("n", "<leader>bn", '<cmd>lua require("bufferline").cycle(1)<CR>', opts)
utils.map("n", "<leader>bg", '<cmd>lua require("bufferline").pick_buffer()<CR>', opts)
utils.map("n", "<leader>b[", '<cmd>lua require("bufferline").cycle(-1)<CR>', opts)
utils.map("n", "<leader>bp", '<cmd>lua require("bufferline").cycle(-1)<CR>', opts)
utils.map("n", "<leader>bf", "<cmd>FormatWrite<CR>", opts)

-- Plugins
utils.map("n", "<leader>ps", "<cmd>PackerSync<CR>", opts)
utils.map("n", "<leader>pi", "<cmd>PackerInstall<CR>", opts)
utils.map("n", "<leader>pc", "<cmd>PackerClean<CR>", opts)
utils.map("n", "<leader>pC", "<cmd>PackerCompile<CR>", opts)
utils.map("n", "<leader>pS", "<cmd>PackerStatus<CR>", opts)
utils.map("n", "<leader>pp", "<cmd>PackerProfile<CR>", opts)

-- files
utils.map("n", "<leader>fc", "<cmd>e $MYVIMRC<CR>", opts)
utils.map("n", "<leader>ff", "<cmd>Telescope find_files<CR>", opts)

utils.map("n", "<leader>fr", "<cmd>Telescope oldfiles<CR>", opts)
utils.map("n", "<leader>ft", "<cmd>Telescope help_tags<CR>", opts)
utils.map("n", "<leader>fR", "<cmd>SudaRead<CR>", opts)
utils.map("n", "<leader>fw", "<cmd>SudaWrite<CR>", opts)

-- search
utils.map("n", "<leader>sg", "<cmd>Telescope live_grep<CR>", opts)
utils.map("n", "<leader>sb", "<cmd>Telescope current_buffer_fuzzy_find<CR>", opts)
utils.map("n", "<leader>ss", "<cmd>Telescope lsp_document_symbols<CR>", opts)
utils.map("n", "<leader>sh", "<cmd>Telescope command_history<CR>", opts)
utils.map("n", "<leader>sm", "<cmd>Telescope marks<CR>", opts)

-- windows
utils.map("n", "<leader>ww", "<C-W>p", opts)
utils.map("n", "<leader>wd", "<C-W>c", opts)
utils.map("n", "<leader>w-", "<C-W>s", opts)
utils.map("n", "<leader>w|", "<C-W>v", opts)
utils.map("n", "<leader>w2", "<C-W>v", opts)
utils.map("n", "<leader>wh", "<C-W>h", opts)
utils.map("n", "<leader>wj", "<C-W>j", opts)
utils.map("n", "<leader>wl", "<C-W>l", opts)
utils.map("n", "<leader>wk", "<C-W>k", opts)
utils.map("n", "<leader>wH", "<C-W>5<", opts)
utils.map("n", "<leader>wJ", "<cmd>resize +5<CR>", opts)
utils.map("n", "<leader>wL", "<C-W>5>", opts)
utils.map("n", "<leader>wK", "<cmd>resize -5<CR>", opts)
utils.map("n", "<leader>w=", "<C-W>=", opts)
utils.map("n", "<leader>ws", "<C-W>s", opts)
utils.map("n", "<leader>wv", "<C-W>v", opts)

-- quit / sessions
utils.map("n", "<leader>qq", "<cmd>q<CR>", opts)
utils.map("n", "<leader>qw", "<cmd>w<CR>", opts)
utils.map("n", "<leader>qs", "<cmd>SaveSession<CR>", opts)
utils.map("n", "<leader>qr", "<cmd>RestoreSession<CR>", opts)
utils.map("n", "<leader>ql", "<cmd>Telescope session-lens search_session<CR>", opts)

-- toggle
utils.map("n", "<leader>od", "<cmd>Dashboard<CR>", opts)
utils.map("n", "<leader>oe", "<cmd>NvimTreeToggle<CR>", opts)
utils.map("n", "<leader>om", "<cmd>MinimapToggle<CR>", opts)
utils.map("n", "<leader>os", "<cmd>SymbolsOutline<CR>", opts)
utils.map("n", "<leader>ot", "<cmd>ToggleTerm<CR>", opts)

-- git
utils.map("n", "<leader>go", "<cmd>LazyGit<CR>", opts)
utils.map("n", "<leader>gl", '<cmd>TermExec cmd="git pull"<CR>', opts)
utils.map("n", "<leader>gp", '<cmd>TermExec cmd="git push"<CR>', opts)
utils.map("n", "<leader>gs", "<cmd>Telescope git_status<CR>", opts)
utils.map("n", "<leader>gB", "<cmd>Telescope git_branches<CR>", opts)
utils.map("n", "<leader>gc", "<cmd>Telescope git_commits<CR>", opts)

-- lsp
-- utils.map("n", "<leader>cla", "<cmd>Lspsaga code_action<CR>", opts)
utils.map("n", "<leader>cli", "<cmd>LspInfo<CR>", opts)
-- utils.map("n", "<leader>cld", "<cmd>lua vim.lsp.buf.type_definition()<CR>", opts)
-- utils.map("n", "<leader>cll", "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>", opts)
-- utils.map("n", "<leader>clL", "<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>", opts)
