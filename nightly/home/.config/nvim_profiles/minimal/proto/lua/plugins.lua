local M = {}

function M.setup()
	local packer = require("packer")

	local conf = {
		compile_path = vim.fn.stdpath("config") .. "/lua/packer_compiled.lua",
	}

	local function plugins(use)
		-- Packer can manage itself as an optional plugin
		use({ "wbthomason/packer.nvim", opt = true })
		use({ "lewis6991/impatient.nvim" })
		use({ "voldikss/vim-floaterm", event = "BufEnter" })
		use({
			"terrortylor/nvim-comment",
			config = function()
				require("nvim_comment").setup()
			end,
		})
		-- use {
		--   "onsails/lspkind-nvim",
		--   config = function()
		--     require("lspkind").init()
		--   end,
		-- }
		-- Completion - use either one of this
		-- use {
		--   "hrsh7th/nvim-cmp",
		--   --event = "BufRead",
		--   requires = {
		--     "hrsh7th/cmp-buffer",
		--     "hrsh7th/cmp-nvim-lsp",
		--     "quangnguyen30192/cmp-nvim-ultisnips",
		--     "hrsh7th/cmp-nvim-lua",
		--     "octaltree/cmp-look",
		--     "hrsh7th/cmp-path",
		--     "hrsh7th/cmp-calc",
		--     "f3fora/cmp-spell",
		--     "hrsh7th/cmp-emoji",
		--     "ray-x/cmp-treesitter",
		--     "hrsh7th/cmp-cmdline",
		--     "hrsh7th/cmp-nvim-lsp-document-symbol",
		--   },
		--   config = function()
		--     require("cmp").setup()
		--   end,
		-- }
		use({ "folke/lua-dev.nvim", event = "VimEnter" })
		-- use { "williamboman/nvim-lsp-installer" }
		use({
			"jose-elias-alvarez/null-ls.nvim",
		})
		use({
			"neovim/nvim-lspconfig",
			opt = true,
			event = "BufReadPre",
			requires = "nvim-lua/plenary.nvim",
			config = function()
				require("lsp")
				-- require("dap").setup()
			end,
		})
	end
	packer.init(conf)
	packer.startup(plugins)
end

return M
