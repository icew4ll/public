local fn = vim.fn
local execute = vim.api.nvim_command

local function post()
	vim.cmd([[
		rshada!
		doautocmd BufRead
		syntax on
		filetype on
		filetype plugin indent on
		PackerLoad impatient.nvim
	]])
end

local function prepare()
    vim.cmd([[
        syntax off
        filetype off
        filetype plugin indent off
    ]])
    vim.g.loaded_gzip = false
    vim.g.loaded_matchit = false
    vim.g.loaded_netrwPlugin = false
    vim.g.loaded_tarPlugin = false
    vim.g.loaded_zipPlugin = false
    vim.g.loaded_man = false
    vim.g.loaded_2html_plugin = false
    vim.g.loaded_remote_plugins = false
end

local function packer_init()
	local install_path = fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"
	if fn.empty(fn.glob(install_path)) > 0 then
		execute("!git clone https://github.com/wbthomason/packer.nvim " .. install_path)
	end
	vim.cmd([[packadd! packer.nvim]])
	vim.cmd("autocmd BufWritePost plugins.lua PackerCompile")
end

local function sys_init()
	-- Performance
	require("impatient")
end

----- Start loading ----------

sys_init()
require("packer_compiled")
packer_init()
prepare()

vim.defer_fn(function()
    require("defaults").setup()
    require("settings").setup()
    require("mappings").setup()
	require("plugins").setup()
    post()
end, 0)

----- End loading ----------
