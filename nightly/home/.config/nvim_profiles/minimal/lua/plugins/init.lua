local present, packer = pcall(require, "plugins.packer_init")

if not present then
	return false
end

local use = packer.use

return packer.startup(function()
	-- this is arranged on the basis of when a plugin starts

	-- this is the nvchad core repo containing utilities for some features like theme swticher, no need to lazy load
	use({
		"Nvchad/extensions",
	})

	use({
		"nvim-lua/plenary.nvim",
	})

	use({
		"wbthomason/packer.nvim",
		event = "VimEnter",
	})
	use({
		"eddyekofo94/gruvbox-flat.nvim",
		config = function()
			vim.api.nvim_command("colo gruvbox-flat")
		end,
	})
end)
