local M = {}
local fn = vim.fn
local base = vim.fn.stdpath("config")
local backup_dir = base .. "/tmp/backup"
local swap_dir = base .. "/tmp/swap"
local undo_dir = base .. "/tmp/undo"
local cmd = vim.cmd
local o = vim.o
local opt = vim.opt

function M.setup()
	if fn.empty(fn.glob(backup_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. backup_dir)
	end

	if fn.empty(fn.glob(swap_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. swap_dir)
	end

	if fn.empty(fn.glob(undo_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. undo_dir)
	end

	opt.swapfile = false
	o.undofile = true
	o.backup = true
	o.writebackup = true
	o.backupdir = backup_dir
	o.undodir = undo_dir
end

return M
