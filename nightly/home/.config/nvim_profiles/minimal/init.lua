-- INIT.LUA
-- We simply bootstrap packer and Aniseed here.

local execute = vim.api.nvim_command
local fn = vim.fn
local pack_path = fn.stdpath("data") .. "/site/pack"
local fmt = string.format

local function ensure(user, repo)
	-- Ensures a given github.com/USER/REPO is cloned in the pack/packer/start directory.
	local install_path = fmt("%s/packer/start/%s", pack_path, repo)
	if fn.empty(fn.glob(install_path)) > 0 then
		execute(fmt("!git clone https://github.com/%s/%s %s", user, repo, install_path))
		execute(fmt("packadd %s", repo))
	end
end

-- Bootstrap essential plugins required for installing and loading the rest.
ensure("wbthomason", "packer.nvim")
ensure("lewis6991", "impatient.nvim")

-- Load impatient which pre-compiles and caches Lua modules.
-- require("impatient")
do
	-- https://github.com/lewis6991/impatient.nvim
	local ok, _ = pcall(require, "impatient")

	if not ok then
		vim.notify("impatient.nvim not installed", vim.log.levels.WARN)
	end
end

local core_modules = {
	"_plugins",
}

for _, module in ipairs(core_modules) do
	local ok, err = pcall(require, module)
	if not ok then
		error("Error loading " .. module .. "\n\n" .. err)
	end
end
