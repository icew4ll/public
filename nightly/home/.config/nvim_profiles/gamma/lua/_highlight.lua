
local function high_clear(group)
	vim.api.nvim_command('hi! clear ' .. group)
end

local function high_link(group, link)
	vim.api.nvim_command('hi! link ' .. group .. ' ' .. link)
end

high_link('GitSignsAdd', 'DiffAddedGutter')
high_link('GitSignsChange', 'DiffModifiedGutter')
high_link('GitSignsDelete', 'DiffRemovedGutter')
high_link('GitSignsChangeDelete', 'DiffModifiedGutter')
