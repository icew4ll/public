local map = require("utils").map
local opts = {noremap = true, silent = true}

-- Quick Quit
map("n", "tq", ":q<CR>", {})
map("n", "ta", ":qa<CR>", {})

-- Quick Save
map("n", "ts", ":w<CR>", {noremap = true, silent = true})

-- navigation
-- map("n", "pw", ":HopWord<CR>", {})
-- map("n", "pl", ":HopLine<CR>", {})

-- packer
map("n", "tc", ":PackerCompile<CR>", {})
map("n", "tu", ":PackerUpdate<CR>", {})

-- Trouble
-- map("n", "pt", "<cmd>Trouble lsp_document_diagnostics<cr>", {silent = true, noremap = true})

-- fzf
vim.cmd [[command! -nargs=1 Rg call luaeval('require("fzf-commands").rg(_A)', <f-args>)]]
map("n", "gf", ":lua require('fzf-commands').files()<cr>", opts)
map("n", "gh", ":lua require('fzf-commands').helptags()<cr>", opts)
map("n", "gb", ":lua require('fzf-commands').bufferpicker()<cr>", opts)
map("n", "gm", ":lua require('fzf-commands').manpicker()<cr>", opts)
map("n", "gr", ":<c-u>Rg<space>", opts)
map("n", "gc", ":lua require('fzf-commands').colorschemes()<cr>", opts)

-- buffers
map("n", "gd", ":bd<cr>", opts)
map("n", "gn", ":BufferLineCycleNext<cr>", opts)
map("n", "gp", ":BufferLineCyclePrev<cr>", opts)

-- bufferline
-- map("n", "px", [[<Cmd>bdelete<CR>]], opts) -- close tab
-- map("n", "pn", [[<Cmd>BufferLineCycleNext<CR>]], opts)
-- map("n", "pb", [[<Cmd>BufferLineCyclePrev<CR>]], opts)

-- snippets
map("n", "<leader>o", ":VsnipOpen<CR>", {})

-- vista
map("n", "<leader>v", ":Vista!!<CR>", {})

-- lua tree
map(
    "n",
    "<Leader>r",
    ":NvimTreeToggle<CR>",
    {
        noremap = true,
        silent = true
    }
)

-- markdown preview
map("n", "<leader>p", ":set spell<CR>", {})

-- Better indenting
map("v", "<", "<gv", {})
map("v", ">", ">gv", {})

-- clear all the highlighted text from the previous search
map(
    "n",
    "<esc>",
    ":noh<CR>",
    {
        silent = true
    }
)

-- Easier Moving between splits
map("n", "<C-J>", "<C-W><C-J>", {})
map("n", "<C-K>", "<C-W><C-K>", {})
map("n", "<C-L>", "<C-W><C-L>", {})
map("n", "<C-H>", "<C-W><C-H>", {})

-- dial
map("n", "<M-]>", ':execute "normal \\<Plug>(dial-increment)"<CR>', {})
map("n", "<M-[>", ':execute "normal \\<Plug>(dial-decrement)"<CR>', {})

-- Sizing window
map("n", "<Left>", "<C-W>5<", {})
map("n", "<Right>", "<C-W>5>", {})
-- map("n", "<A-t>", "<C-W>+", {})
-- map("n", "<A-s>", "<C-W>-", {})

-- Folds
map("n", "zt", ":FoldToggle", {})
map("n", "<TAB>", "za", {})
map("v", "<TAB>", "za", {})
map("n", "zm", "zM", {})
map("v", "zm", "zM", {})
map("n", "zn", "zR", {})
map("v", "zn", "zR", {})

-- diffget
map("n", "<leader>w", ":diffget //2<CR>", {})
map("n", "<leader>r", ":diffget //3<CR>", {})
map("n", "<leader>e", ":Gwrite<CR>", {})

-- tmux
map("n", "<leader>,", ":VtrAttachToPane<CR>", {})
map("n", "<leader>.", ":VtrSendLinesToRunner<CR>", {})
map("v", "<leader>.", ":VtrSendLinesToRunner<CR>", {})

-- completion
-- map("i", "<Tab>", [[ pumvisible() ? "\<C-n>" : "\<Tab>" ]], {expr = true})
-- map("i", "<S-Tab>", [[ pumvisible() ? "\<C-p>" : "\<S-Tab>" ]], {expr = true})
-- map("i", "<C-k>", [[ <Plug>(neosnippet_expand_or_jump) ]], {expr = true})
-- map("i", "<Tab>", [[ pumvisible() : neosnippet#expandable_or_jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<Tab>" ]], {expr = true})
-- map("i", "<S-Tab>", [[ pumvisible() : "\<S-Tab>" ]], {expr = true})
