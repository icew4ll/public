local packer_path = vim.fn.stdpath("data") .. "/site/pack/packer/opt/packer.nvim"

if vim.fn.empty(vim.fn.glob(packer_path)) > 0 then
	vim.fn.system({
		"git",
		"clone",
		"https://github.com/wbthomason/packer.nvim",
		packer_path,
	})
end

-- Load packer
vim.cmd([[ packadd packer.nvim ]])
local packer = require("packer")

-- Change some defaults
packer.init({
	git = {
		clone_timeout = 300, -- 5 mins
	},
	profile = {
		enable = true,
	},
})

packer.startup(function(use)
	-- Plugins manager
	use({
		"wbthomason/packer.nvim",
		opt = true,
	})

	-- Tree-Sitter
	use({
		"nvim-treesitter/nvim-treesitter",
		opt = true,
		run = ":TSUpdate",
		config = function()
			require("_treesitter")
		end,
	})
	use("David-Kunz/treesitter-unit")
	use({
		"nvim-lua/plenary.nvim",
		module = "plenary",
	})
	-- Neorg
	use({
		"nvim-neorg/neorg",
		-- ft = "norg",
		branch = "unstable",
		-- branch = "main",
		after = { "nvim-treesitter" },
		config = function()
			require("_neorg")
		end,
		requires = "nvim-lua/plenary.nvim",
	})

	-- nlua
	use({
		"tjdevries/nlua.nvim",
		requires = {
			"euclidianAce/BetterLua.vim",
		},
	})

	-- lsp
	use({
		"jose-elias-alvarez/null-ls.nvim",
		config = function()
			require("lsp")
		end,
		requires = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
	})
	-- completion
	use({ "onsails/lspkind-nvim" })
	use({
		"hrsh7th/nvim-cmp",
		-- requires = {
		--     {
		--         "ray-x/lsp_signature.nvim"
		--     }
		-- },
		requires = {
			-- LSP source for nvim-cmp
			-- { "onsails/lspkind-nvim", after = "nvim-cmp" },
			{ "hrsh7th/cmp-nvim-lsp", after = "nvim-cmp" },
			{ "hrsh7th/cmp-nvim-lua", after = "nvim-cmp" },

			-- Snippets source for nvim-cmp
			{ "hrsh7th/cmp-vsnip", after = "nvim-cmp" },
			{ "hrsh7th/vim-vsnip", after = "nvim-cmp" },
			{ "hrsh7th/vim-vsnip-integ", after = "nvim-cmp" },
			{ "rafamadriz/friendly-snippets", after = "nvim-cmp" },

			-- path source for nvim-cmp
			{ "hrsh7th/cmp-path", after = "nvim-cmp" },
			{ "hrsh7th/cmp-cmdline", after = "nvim-cmp" },
			-- lua source for nvim-cmp
			{ "hrsh7th/cmp-buffer", after = "nvim-cmp" },
		},
		config = function()
			require("_cmp")
		end,
		after = "nvim-lspconfig",
	})

	-- Comment
	use({
		"terrortylor/nvim-comment",
		event = "BufEnter",
		config = function()
			require("nvim_comment").setup({
				-- Linters prefer comment and line to hae a space in between
				left_marker_padding = true,
				-- should comment out empty or whitespace only lines
				comment_empty = true,
				-- Should key mappings be created
				create_mappings = true,
				-- Normal mode mapping left hand side
				line_mapping = "gcc",
				-- Visual/Operator mapping left hand side
				operator_mapping = "gc",
			})
		end,
	})

	-- Better terminal
	use({
		"akinsho/nvim-toggleterm.lua",
		config = function()
			require("_term")
		end,
		module = { "toggleterm", "toggleterm.terminal" },
		cmd = { "ToggleTerm", "TermExec" },
		keys = { "n", "<F4>" },
	})

	-- Development icons
	use({
		"kyazdani42/nvim-web-devicons",
		module = "nvim-web-devicons",
	})
	-- Statusline
	-- can be disabled to use your own statusline
	use({
		"glepnir/galaxyline.nvim",
		requires = "nvim-web-devicons",
		branch = "main",
		config = function()
			require("_galaxy")
		end,
	})

	-- Tabline
	use({
		"akinsho/nvim-bufferline.lua",
		event = { "VimEnter *" },
		config = function()
			require("_bufferline")
		end,
	})

	-- Viewer & finder for LSP symbols and tags
	use({
		"simrat39/symbols-outline.nvim",
		cmd = {
			"SymbolsOutline",
			"SymbolsOutlineOpen",
			"SymbolsOutlineClose",
		},
	})

	use({
		"nvim-lua/popup.nvim",
		module = "popup",
	})

	use({
		"nvim-telescope/telescope.nvim",
		cmd = "Telescope",
		module = "telescope",
		requires = {
			"popup.nvim",
			"plenary.nvim",
		},
		config = function()
			require("_telescope")
		end,
	})

	-- use(
	--     {
	--         "lewis6991/gitsigns.nvim",
	--         requires = "plenary.nvim",
	--         event = "BufRead",
	--         config = function()
	--             require("_gitsigns")
	--         end
	--     }
	-- )

	-- LazyGit integration
	use({
		"kdheepak/lazygit.nvim",
		requires = "plenary.nvim",
		cmd = { "LazyGit", "LazyGitConfig" },
	})

	use({
		"folke/which-key.nvim",
		event = "BufWinEnter",
		config = require("_whichkey"),
	})

	use({
		"lambdalisue/suda.vim",
		cmd = { "SudaRead", "SudaWrite" },
	})

	use({
		"phaazon/hop.nvim",
		branch = "v1", -- optional but strongly recommended
		config = function()
			-- you can configure Hop the way you like here; see :h hop-config
			require("hop").setup({ keys = "etovxqpdygfblzhckisuran" })
		end,
	})

	use({
		"olambo/vi-viz",
	})

	use({ "michaelb/sniprun", run = "bash ./install.sh" })

	-- Autopairs
	use({
		"windwp/nvim-autopairs",
		event = "InsertEnter",
		setup = function()
			vim.cmd([[packadd nvim-autopairs]])
			require("nvim-autopairs").setup()
		end,
	})

	-- closetag
	use({
		"alvan/vim-closetag",
		setup = function()
			local g = vim.g
			g.closetag_filenames = "*.html,*.tsx"
		end,
	})

	-- ion
	use({
		"vmchale/ion-vim",
	})

	-- emmett
	use({
		"mattn/emmet-vim",
	})

	-- vlang
	use({
		"cheap-glitch/vim-v",
	})

	-- markdown
	use({
		"masukomi/vim-markdown-folding",
	})
	use({
		"tpope/vim-markdown",
	})

	use({
		"chmp/mdnav",
	})

	-- Indent Lines
	use({
		"lukas-reineke/indent-blankline.nvim",
		event = "BufEnter",
	})

	-- floaterm
	use({
		"voldikss/vim-floaterm",
		config = function()
			local g = vim.g
			g.floaterm_width = 0.9
			g.floaterm_opener = "edit"
		end,
	})

	-- tmux
	use({
		"christoomey/vim-tmux-runner",
	})

	-- 	use({
	-- 		"Pocco81/Catppuccino.nvim",
	-- 		config = function()
	-- 			vim.api.nvim_command("colo dark_catppuccino")
	-- 			-- vim.api.nvim_command("colo catppuccino")
	-- 			local catppuccino = require("catppuccino")
	--
	-- 			-- configure it
	-- 			catppuccino.setup({
	-- 				colorscheme = "dark_catppuccino",
	-- 				transparency = false,
	-- 				term_colors = false,
	-- 				styles = {
	-- 					comments = "italic",
	-- 					functions = "italic",
	-- 					keywords = "italic",
	-- 					strings = "NONE",
	-- 					variables = "NONE",
	-- 				},
	-- 				integrations = {
	-- 					treesitter = true,
	-- 					native_lsp = {
	-- 						enabled = true,
	-- 						virtual_text = {
	-- 							errors = "italic",
	-- 							hints = "italic",
	-- 							warnings = "italic",
	-- 							information = "italic",
	-- 						},
	-- 						underlines = {
	-- 							errors = "underline",
	-- 							hints = "underline",
	-- 							warnings = "underline",
	-- 							information = "underline",
	-- 						},
	-- 					},
	-- 					lsp_trouble = false,
	-- 					lsp_saga = false,
	-- 					gitgutter = false,
	-- 					gitsigns = false,
	-- 					telescope = false,
	-- 					nvimtree = {
	-- 						enabled = false,
	-- 						show_root = false,
	-- 					},
	-- 					which_key = false,
	-- 					indent_blankline = {
	-- 						enabled = false,
	-- 						colored_indent_levels = false,
	-- 					},
	-- 					dashboard = false,
	-- 					neogit = false,
	-- 					vim_sneak = false,
	-- 					fern = false,
	-- 					barbar = false,
	-- 					bufferline = false,
	-- 					markdown = false,
	-- 					lightspeed = false,
	-- 					ts_rainbow = false,
	-- 					hop = false,
	-- 				},
	-- 			})
	-- 		end,
	-- 	})
	-- use({
	-- 	"folke/tokyonight.nvim",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo tokyonight")
	-- 		-- vim.g.tokyonight_style = "night"
	-- 		vim.g.tokyonight_style = "storm"
	-- 		-- vim.g.tokyonight_style = "day"
	-- 		vim.g.tokyonight_italic_functions = true
	-- 		-- vim.g.tokyonight_colors = { hint = "orange", error = "#ff0000" }
	-- 	end,
	-- })
	-- 	use({
	-- 		"rose-pine/neovim",
	-- 		as = "rose-pine",
	-- 		config = function()
	-- 			-- Options (see available options below)
	-- 			vim.g.rose_pine_variant = "base"
	-- 			-- vim.g.rose_pine_variant = "moon"
	-- 			-- vim.g.rose_pine_variant = "dawn"
	--
	-- 			-- Load colorscheme after options
	-- 			vim.cmd("colorscheme rose-pine")
	-- 		end,
	-- 	})
	use({
		"ful1e5/onedark.nvim",
		config = function()
			vim.api.nvim_command("colo onedark")
		end,
	})
	-- use({
	-- 	"EdenEast/nightfox.nvim",
	-- 	requires = "rktjmp/lush.nvim",
	-- 	config = function()
	-- 		-- vim.api.nvim_command("colo nightfox")
	-- 		-- vim.api.nvim_command("colo nordfox")
	-- 		vim.api.nvim_command("colo palefox")
	-- 		-- vim.api.nvim_command("colo dayfox")
	-- 		-- vim.api.nvim_command("colo dawnfox")
	-- 		-- vim.api.nvim_command("colo duskfox")
	-- 	end,
	-- })
	-- use({
	-- 	"mastertinner/nvim-quantum",
	-- 	config = function()
	-- 		require("quantum").setup()
	-- 	end,
	-- })
	-- use({
	-- 	"eddyekofo94/gruvbox-flat.nvim",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo gruvbox-flat")
	-- 	end,
	-- })

	-- use({
	-- 	"metalelf0/jellybeans-nvim",
	-- 	requires = "rktjmp/lush.nvim",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo jellybeans-nvim")
	-- 	end,
	-- })
	-- use({
	-- 	"rmehri01/onenord.nvim",
	-- 	config = function()
	-- 		vim.api.nvim_command("colo onenord")
	-- 	end,
	-- })

	-- Fastest colorizer without external dependencies!
	use({
		"norcalli/nvim-colorizer.lua",
		config = function()
			require("colorizer").setup({
				"*", -- enable on all files
				-- css = { rgb_fn = true },
				-- scss = { rgb_fn = true },
				-- sass = { rgb_fn = true },
				-- stylus = { rgb_fn = true },
				-- vim = { names = true },
				-- tmux = { names = false },
				-- "javascript",
				-- "javascriptreact",
				-- "typescript",
				-- "typescriptreact",
				-- "conf",
				-- lua = { rgb_fn = true },
			})
		end,
	})
end)
