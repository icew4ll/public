-- require("null-ls").config({
--     -- you must define at least one source for the plugin to work
--     sources = { require("null-ls").builtins.formatting.stylua }
-- })
local null_ls = require("null-ls")

-- null_ls.config({
--     debug = true
-- })

-- register any number of sources simultaneously
local sources = {
    -- enable sources for filetypes only
    null_ls.builtins.formatting.black.with({
        filetypes = { "py"},
    }),
    -- null_ls.builtins.formatting.prettier.with({
    --     filetypes = { "html", "json", "yaml", "markdown" },
    -- }),
    null_ls.builtins.diagnostics.write_good,
    null_ls.builtins.code_actions.gitsigns,

    -- example: conditional source registration
    -- null_ls.builtins.formatting.stylua.with({
    --     condition = function(utils)
    --         return utils.root_has_file("stylua.toml")
    --     end,
    -- }),
    --
    -- example: diag
    -- null_ls.builtins.diagnostics.shellcheck.with({
    --     diagnostics_format = "[#{c}] #{m} (#{s})"
    -- }),
    --
    -- example: extra args
    -- null_ls.builtins.formatting.shfmt.with({
    --     extra_args = { "-i", "2", "-ci" }
    --   })
    
    -- example: config
    -- null_ls.builtins.formatting.stylua.with({
    --     extra_args = { "--config-path", vim.fn.expand("~/.config/stylua.toml") },
    -- }),
}

null_ls.config({ sources = sources })

require("lspconfig")["null-ls"].setup({})
-- require("lspconfig")["null-ls"].setup({
--     -- see the nvim-lspconfig documentation for available configuration options
--     on_attach = my_custom_on_attach
-- })
