local map = function(mode, key, result)
	vim.api.nvim_set_keymap(mode, key, "<cmd>lua " .. result .. "<cr>", { noremap = true, silent = true })
end

local function custom_attach(client, bufnr)
	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

	if client.resolved_capabilities.hover then
		-- map("n", "<leader>clh", "vim.lsp.buf.hover()")
		map("n", "clh", "vim.lsp.buf.hover()")
	end

	if client.resolved_capabilities.find_references then
		map("n", "<leader>cla", "vim.lsp.buf.references()")
	end

	if client.resolved_capabilities.code_action then
		map("n", "<leader>cla", "vim.lsp.buf.code_actions()")
		map("n", "<leader>cla", "vim.lsp.buf.range_code_actions()")
	end

	if client.resolved_capabilities.declaration then
		map("n", "<leader>cld", vim.lsp.buf.declaration)
	end

	if client.resolved_capabilities.goto_definition then
		map("n", "<leader>cln", "vim.lsp.buf.definition()")
	end

	if client.resolved_capabilities.document_formatting then
		map("n", "<leader>clf", "vim.lsp.buf.formatting()")
	end

	if client.resolved_capabilities.rename then
		map("n", "<leader>clr", "vim.lsp.buf.rename()")
	end

	if client.resolved_capabilities.implementation then
		map("n", "<leader>clm", "vim.lsp.buf.implementation()")
	end

	map("n", "<leader>cl[", "vim.lsp.diagnostic.goto_prev()")
	map("n", "<leader>cl]", "vim.lsp.diagnostic.goto_next()")
	map("n", "<leader>cll", "vim.lsp.diagnostic.show_line_diagnostics()")
end

return custom_attach
