-- define some colors
local colors = require("_colors")

require("bufferline").setup({
	options = {
		offsets = { { filetype = "NvimTree", text = "Explorer" } },
		buffer_close_icon = "",
		modified_icon = "",
		close_icon = " ",
		left_trunc_marker = "",
		right_trunc_marker = "",
		max_name_length = 14,
		max_prefix_length = 13,
		tab_size = 15,
		show_tab_indicators = true,
		enforce_regular_tabs = false,
		view = "multiwindow",
		show_buffer_close_icons = true,
		separator_style = "thin",
		-- mappings = "true"
	},
	-- bar colors!!
	highlights = {
		fill = {
			guifg = colors.base04,
			guibg = colors.base03,
		},
		background = {
			guifg = colors.base04,
			guibg = colors.base03,
		},
		-- buffer
		buffer_selected = {
			guifg = colors.base05,
			guibg = colors.base06,
			gui = "bold",
		},
		buffer_visible = {
			guifg = colors.base00,
			guibg = colors.base03,
		},
		-- tabs over right
		tab = {
			guifg = colors.base00,
			guibg = colors.base07,
		},
		tab_selected = {
			guifg = colors.base07,
			guibg = colors.base00,
		},
		tab_close = {
			guifg = colors.warn,
			guibg = colors.base03,
		},
		-- buffer separators
		separator = {
			guifg = colors.base03,
			guibg = colors.base03,
		},
		separator_selected = {
			guifg = colors.base06,
			guibg = colors.base06,
		},
		separator_visible = {
			guifg = colors.base03,
			guibg = colors.base03,
		},
		indicator_selected = {
			guifg = colors.base03,
			guibg = colors.base03,
		},
		-- modified files (but not saved)
		modified_selected = {
			guifg = colors.success,
			guibg = colors.base06,
		},
		modified_visible = {
			guifg = colors.error,
			guibg = colors.base08,
		},
	},
})
