local o = vim.o
local fn = vim.fn -- to call Vim functions e.g. fn.bufnr()
local cmd = vim.cmd

local base = vim.fn.stdpath("config")
-- create backup/swap/undo directories
local backup_dir = base .. "/tmp/backup"
local swap_dir = base .. "/tmp/swap"
local undo_dir = base .. "/tmp/undo"

if fn.empty(fn.glob(backup_dir)) > 0 then
    cmd("!mkdir -p" .. " " .. backup_dir)
end

if fn.empty(fn.glob(swap_dir)) > 0 then
    cmd("!mkdir -p" .. " " .. swap_dir)
end

if fn.empty(fn.glob(undo_dir)) > 0 then
    cmd("!mkdir -p" .. " " .. undo_dir)
end

-- Backup, undo, swap options
o.undofile = true
o.backup = true
o.writebackup = true
o.backupdir = backup_dir
o.directory = swap_dir .. o.directory
o.undodir = undo_dir
