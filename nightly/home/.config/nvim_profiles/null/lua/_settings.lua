local o = vim.o
local g = vim.g
local wo = vim.wo
local bo = vim.bo
local cmd = vim.cmd -- to execute Vim commands e.g. cmd('pwd')
-- local config = vim.fn.expand("$XDG_CONFIG_HOME")
local home = vim.fn.expand("$HOME")
local py = home .. "/.pyenv/versions/3.9.1/bin/python3.9"
-- tab / indent level
local tab = 2

g.python3_host_prog = py
-- require "py_lsp".setup {
--     -- This is optional, but allows to create virtual envs from nvim
--     host_python = py
-- }
g.node_host_prog = home .. "/.volta/tools/image/packages/neovim/bin/neovim-node-host"
-- lua print(vim.fn.expand("$XDG_CONFIG_HOME"))

-- g.mapleader = " "
-- g.mapleader = "t"
o.termguicolors = true
cmd "set noswapfile"
cmd "set wrap linebreak nolist"
-- wo.cursorline = true
wo.number = true
wo.relativenumber = true
-- wo.signcolumn = "number"
-- g.noswapfile = true
g.noshowmode = true
o.mouse = "a"
o.tabstop = tab
o.shiftwidth = tab
o.softtabstop = tab
o.expandtab = true
o.smartindent = true
o.preserveindent = true
-- o.colorcolumn = "80"

-- markdown
-- wo.conceallevel = 2
-- o.updatetime = 50
-- o.hidden = true
-- enable native markdown folding
-- g.markdown_folding = 1
g.markdown_fenced_languages = {
    "js=javascript",
    "javascript",
    "typescript",
    "html",
    "python",
    "bash"
}

-- o.wrap = true

o.splitbelow = true
o.splitright = true

o.ignorecase = true
o.smartcase = true

o.clipboard = "unnamedplus"

g.markdown_fenced_languages = {
    "js=javascript",
    "javascript",
    "typescript",
    "html",
    "python",
    "bash"
}

-- LSP
-- menuone: popup even when there's only one match
-- noinsert: Do not insert text until a selection is made
-- noselect: Do not select, force user to select one from the menu
o.completeopt = "menuone,noinsert,noselect"
-- Avoid showing message extra message when using completion
o.shortmess = "c"
-- Visualize diagnostics
g.diagnostic_enable_virtual_text = 1
g.diagnostic_trimmed_virtual_text = "40"
-- Don't show diagnostics while in insert mode
g.diagnostic_insert_delay = 1
o.updatetime = 300

-- FOLDS
o.foldlevel = 99


-- Global options
vim.opt.hidden = true
vim.opt.timeoutlen = 500
vim.opt.completeopt = {
    "menu",
    "menuone",
    "preview",
    "noinsert",
    "noselect"
}
vim.opt.shortmess:append("atsc")
vim.opt.inccommand = "split"
vim.opt.path = "**"
-- vim.opt.signcolumn = "yes"
