local command = vim.api.nvim_command

-- command(
--   "au BufNewFile,BufRead *.svelte setf svelte"
-- )

command(
  "au BufNewFile,BufRead *.nu setf bash"
)

-- start completion on buffer enter
-- command(
--   "au BufEnter * lua require'completion'.on_attach()"
-- )

-- center buffer around cursor when opening files
command(
  "au BufRead * normal zz"
)

-- close folds
-- command(
--   "au BufRead * normal zM"
-- )

-- nest folds
command(
  "au BufRead * set foldexpr=NestedMarkdownFolds()"
)

-- command(
--   "au BufEnter,BufWinEnter,TabEnter *.rs lua require'lsp_extensions'.inlay_hints{}"
-- )

command(
  [[autocmd BufReadPost * if line("'\"") > 0 && line ("'\"") <= line("$") | exe "normal! g'\"" |  endif]]
)

-- auto start inlay hints on rust files
-- autocmd BufEnter,BufWinEnter,TabEnter *.rs :lua require'lsp_extensions'.inlay_hints{}

-- command(
--   "au BufRead * Vista"
-- )

-- command(
--   "au BufNewFile,BufRead * set wrap"
-- )
