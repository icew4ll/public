require("neorg").setup({
	-- Tell Neorg what modules to load
	load = {
		["core.defaults"] = {}, -- Load all the default modules
		["core.keybinds"] = {
			-- Configure core.keybinds
			-- gtd: done
			-- gtu: undone
			-- gtp: pending
			config = {
				default_keybinds = true, -- Generate the default keybinds
				neorg_leader = "gt", -- This is the default if unspecified
			},
		},
		["core.norg.concealer"] = {}, -- Allows for use of icons
		["core.norg.completion"] = {
			config = {
				engine = "nvim-cmp", -- We current support nvim-compe and nvim-cmp only
			},
		},
		["core.norg.dirman"] = {
			-- Manage your directories with Neorg
			config = {
				workspaces = {
					default_workspace = "~/neorg",
				},
			},
		},
	},
})
