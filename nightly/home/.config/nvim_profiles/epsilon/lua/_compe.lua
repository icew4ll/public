require "compe".setup {
    enabled = true,
    debug = false,
    min_length = 1,
    preselect = "enable" or "disable" or "always",
    allow_prefix_unmatch = false,
    -- throttle_time = ... number ...;
    -- source_timeout = ... number ...;
    -- incomplete_delay = ... number ...;

    source = {
        tags = true,
        spell = true,
        neorg = true,
        path = true,
        buffer = true,
        vsnip = true,
        nvim_lsp = true,
        nvim_lua = true
    }
}
