local lspconfig = require("lspconfig")
local nlua = require("nlua.lsp.nvim")
local util = require("lspconfig/util")
local custom_attach = require("lsp.custom_attach")
local efmConfig = require("lsp.efm")
local home = vim.fn.expand("$HOME")
local build = home .. "/bin/build/lua-language-server"
local bin = build .. "/bin/Linux/lua-language-server"

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
-- vim.lsp.set_log_level("debug")

nlua.setup(lspconfig, {
	on_attach = function(client)
		custom_attach(client)
	end,
	globals = { "use" },
	cmd = { bin, "-E", build .. "/main.lua" },
	settings = {
		Lua = {
			runtime = {
				version = "LuaJIT",
				path = vim.split(package.path, ";"),
			},
			completion = {
				keywordSnippet = "Disable",
			},
			diagnostics = {
				enable = true,
				globals = {
					"vim",
				},
			},
			workspace = {
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true,
				},
			},
		},
	},
})

local servers = {}

servers.bashls = {
	on_attach = function(client)
		custom_attach(client)
	end,
}

servers.vimls = {
	on_attach = function(client)
		custom_attach(client)
	end,
}

servers.jsonls = {
	cmd = { "vscode-json-languageserver", "--stdio" },
	on_attach = function(client)
		custom_attach(client)
		client.resolved_capabilities.document_formatting = false
	end,
}

-- servers.clangd = {
--     on_attach = function(client)
--         custom_attach(client)
--     end
-- }

-- servers.svelte = {
--     on_attach = function(client)
--         custom_attach(client)
--     end
-- }

-- servers.intelephense = {
--     on_attach = function(client)
--         custom_attach(client)
--     end
-- }

-- servers.pyright = {
--     on_attach = function(client)
--         custom_attach(client)
--     end
-- }
--
servers.pylsp = {
	on_attach = function(client)
		custom_attach(client)
	end,
}

servers.dockerls = {
	on_attach = function(client)
		custom_attach(client)
	end,
}

servers.html = {
	on_attach = function(client)
		custom_attach(client)
		client.resolved_capabilities.document_formatting = false
	end,
	filetypes = {
		"html",
		"jinja",
	},
}

servers.rust_analyzer = {
	on_attach = function(client)
		custom_attach(client)
	end,
	settings = {
		["rust-analyzer"] = {
			-- cargo = {
			--     loadOutDirsFromCheck = true
			-- },
			-- procMacro = {
			--     enable = true
			-- },
			diagnostics = {
				disabled = {
					"macro-error",
					-- "unresolved-import"
					"unresolved-proc-macro",
				},
			},
		},
	},
}

servers.vuels = {
	on_attach = function(client)
		custom_attach(client)
	end,
}

servers.cssls = {
	on_attach = function(client)
		custom_attach(client)
		-- client.resolved_capabilities.document_formatting = true
	end,
}

servers.gopls = {
	cmd = { "gopls", "serve" },
	settings = { gopls = { analyses = { unusedparams = true }, staticcheck = true } },
	on_attach = function(client)
		custom_attach(client)
	end,
}

servers.zeta_note = {
	cmd = { home .. "/bin/zeta" },
	on_attach = function(client)
		custom_attach(client)
	end,
}

servers.tsserver = {
	root_dir = util.root_pattern("package.json", "tsconfig.json", "jsconfig.json", ".git", vim.fn.getcwd()),
	on_attach = function(client)
		custom_attach(client)
		-- client.resolved_capabilities.document_formatting = false
	end,
	-- filetypes = {
	--     "typescript",
	--     "javascr",
	-- }
}

servers.efm = efmConfig

for server, config in pairs(servers) do
	lspconfig[server].setup(vim.tbl_deep_extend("force", { capabilities = capabilities }, config))
end
