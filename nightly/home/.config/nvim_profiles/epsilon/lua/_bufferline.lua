-- define some colors

local bar_fg = "#565c64"
local activeBuffer_fg = "#c8ccd4"
local emphasis = "#32302f"
-- local emphasis = "#1e222a"
local color_1 = "#9298a0"
local color_2 = "#282626"
-- local color_2 = "#252931"
local color_3 = "#30343c"
local color_4 = "#f9929b"
local color_5 = "#A3BE8C"
local color_6 = "#BF616A"
local color_7 = "#23272f"

require "bufferline".setup {
    options = {
        offsets = {{filetype = "NvimTree", text = "Explorer"}},
        buffer_close_icon = "",
        modified_icon = "",
        close_icon = " ",
        left_trunc_marker = "",
        right_trunc_marker = "",
        max_name_length = 14,
        max_prefix_length = 13,
        tab_size = 20,
        show_tab_indicators = true,
        enforce_regular_tabs = false,
        view = "multiwindow",
        show_buffer_close_icons = true,
        separator_style = "thin",
        -- mappings = "true"
    },
    -- bar colors!!
    highlights = {
        fill = {
            guifg = bar_fg,
            guibg = color_2
        },
        background = {
            guifg = bar_fg,
            guibg = color_2
        },
        -- buffer
        buffer_selected = {
            guifg = activeBuffer_fg,
            guibg = emphasis,
            gui = "bold"
        },
        buffer_visible = {
            guifg = color_1,
            guibg = color_2
        },
        -- tabs over right
        tab = {
            guifg = color_1,
            guibg = color_3
        },
        tab_selected = {
            guifg = color_3,
            guibg = color_1
        },
        tab_close = {
            guifg = color_4,
            guibg = color_2
        },
        -- buffer separators
        separator = {
            guifg = color_2,
            guibg = color_2
        },
        separator_selected = {
            guifg = emphasis,
            guibg = emphasis
        },
        separator_visible = {
            guifg = color_2,
            guibg = color_2
        },
        indicator_selected = {
            guifg = color_2,
            guibg = color_2
        },
        -- modified files (but not saved)
        modified_selected = {
            guifg = color_5,
            guibg = emphasis
        },
        modified_visible = {
            guifg = color_6,
            guibg = color_7
        }
    }
}
