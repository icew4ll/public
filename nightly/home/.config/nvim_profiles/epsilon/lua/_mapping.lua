local map = require("utils").map
local opts = {silent = true}

map("n", "<Space>", ":WhichKey <leader><CR>", opts)

local lsp_opts = vim.tbl_extend("force", opts, {expr = true})

-- quick
map("n", "ts", ":w<CR>", opts)
map("n", "te", ":ToggleTerm<CR>", opts)
map("n", "tq", ":q<CR>", opts)
map("n", "ta", ":qa<CR>", opts)
map("n", "tn", ":FloatermNew nnn -e<CR>", opts)
map("n", "tc", ":PackerCompile<CR>", opts)
map("n", "tu", ":PackerUpdate<cr>", opts)
map("n", "ti", ':lua require"treesitter-unit".select()<CR>', opts)
map("n", "tf", ":lua vim.lsp.buf.formatting()<cr>", opts)
map("n", "to", ":VsnipOpen<cr>", opts)
map("n", "gd", ":lua vim.lsp.buf.definition()<CR>", opts) -- gd: jump to definition
map("n", "gr", ":lua vim.lsp.buf.references()<CR>", opts) -- gr: go to reference
map("n", "gi", ":lua vim.lsp.buf.implementation()<CR>", opts) -- gi: buf implementation

-- tree unit
--

-- tmux
map("n", "t,", ":VtrAttachToPane<CR>", {})
map("n", "t.", ":VtrSendLinesToRunner<CR>", {})
map("v", "t.", ":VtrSendLinesToRunner<CR>", {})

-- https://github.com/hrsh7th/nvim-compe#mappings
map("i", "<C-Space>", "compe#complete()", lsp_opts)
map("i", "<CR>", 'compe#confirm("<CR>")', lsp_opts)
map("i", "<C-e>", 'compe#close("<C-e>")', lsp_opts)
map("i", "<C-f>", 'compe#scroll({ "delta": +4 })', lsp_opts)
map("i", "<C-d>", 'compe#scroll({ "delta": -4 })', lsp_opts)

-- TAB to cycle buffers too, why not?
map("n", "<Tab>", ":bnext<CR>", opts)
map("n", "<S-Tab>", ":bprevious<CR>", opts)
map("n", "z,", "za", {})
map("v", "z,", "za", {})
map("n", "zn", "zM", {})
map("v", "zn", "zM", {})
map("n", "zm", "zR", {})
map("v", "zm", "zR", {})

-- ESC to turn off search highlighting
map("n", "<esc>", ":noh<CR>", opts)

--- F<n> keybindings
map("n", "<F2>", ":SymbolsOutline<CR>", opts)
map("n", "<F3>", ":NvimTreeToggle<CR>", opts)
map("n", "<F5>", ":MinimapToggle<CR>", opts)
map("n", "<F6>", ":TZAtaraxis<CR>", opts)
map("n", "<F7>", ":<Plug>RestNvim<CR>", opts)

map("n", "<C-h>", "<C-w>h", opts)
map("n", "<C-j>", "<C-w>j", opts)
map("n", "<C-k>", "<C-w>k", opts)
map("n", "<C-l>", "<C-w>l", opts)

map("x", "K", ":move '<-2<CR>gv-gv", opts)
map("x", "J", ":move '>+1<CR>gv-gv", opts)

vim.cmd("tnoremap <Esc> <C-\\><C-n>")

vim.cmd(
    [[
  nnoremap <silent> <C-Up>    :resize +2<CR>
  nnoremap <silent> <C-Down>  :resize -2<CR>
  nnoremap <silent> <C-Right> :vertical resize -2<CR>
  nnoremap <silent> <C-Left>  :vertical resize +2<CR>
]]
)

map("n", "<c-z>", "<Nop>", opts)

-- Disable accidentally pressing ctrl-z and suspending
map("n", "<c-z>", "<Nop>", opts)

-- Disable ex mode
map("n", "Q", "<Nop>", opts)

-- Misc
map("n", "<leader>`", "<cmd>e #<CR>", opts)
map("n", "<leader><space>", "<cmd>Telescope find_files<CR>", opts)
map("n", "<leader>.", "<cmd>Telescope file_browser<CR>", opts)
map("n", "<leader>,", "<cmd>Telescope buffers show_all_buffers=true<CR>", opts)
map("n", "<leader>/", "<cmd>Telescope live_grep<CR>", opts)
map("n", "<leader>:", "<cmd>Telescope command_history<CR>", opts)

-- Buffers
map("n", "<leader>bc", '<cmd>lua require("bufferline").handle_close_buffer(vim.fn.bufnr("%"))<CR>', opts)
map("n", "<leader>bb", "<cmd>e #<CR>", opts)
map("n", "<leader>b]", '<cmd>lua require("bufferline").cycle(1)<CR>', opts)
map("n", "<leader>bn", '<cmd>lua require("bufferline").cycle(1)<CR>', opts)
map("n", "<leader>bg", '<cmd>lua require("bufferline").pick_buffer()<CR>', opts)
map("n", "<leader>b[", '<cmd>lua require("bufferline").cycle(-1)<CR>', opts)
map("n", "<leader>bp", '<cmd>lua require("bufferline").cycle(-1)<CR>', opts)
map("n", "<leader>bf", "<cmd>FormatWrite<CR>", opts)

-- Plugins
map("n", "<leader>ps", "<cmd>PackerSync<CR>", opts)
map("n", "<leader>pi", "<cmd>PackerInstall<CR>", opts)
map("n", "<leader>pc", "<cmd>PackerClean<CR>", opts)
map("n", "<leader>pC", "<cmd>PackerCompile<CR>", opts)
map("n", "<leader>pS", "<cmd>PackerStatus<CR>", opts)
map("n", "<leader>pp", "<cmd>PackerProfile<CR>", opts)

-- files
map("n", "<leader>fc", "<cmd>e $MYVIMRC<CR>", opts)
map("n", "<leader>ff", "<cmd>Telescope find_files<CR>", opts)

map("n", "<leader>fr", "<cmd>Telescope oldfiles<CR>", opts)
map("n", "<leader>ft", "<cmd>Telescope help_tags<CR>", opts)
map("n", "<leader>fR", "<cmd>SudaRead<CR>", opts)
map("n", "<leader>fw", "<cmd>SudaWrite<CR>", opts)

-- search
map("n", "<leader>sg", "<cmd>Telescope live_grep<CR>", opts)
map("n", "<leader>sb", "<cmd>Telescope current_buffer_fuzzy_find<CR>", opts)
map("n", "<leader>ss", "<cmd>Telescope lsp_document_symbols<CR>", opts)
map("n", "<leader>sh", "<cmd>Telescope command_history<CR>", opts)
map("n", "<leader>sm", "<cmd>Telescope marks<CR>", opts)

-- windows
map("n", "<leader>ww", "<C-W>p", opts)
map("n", "<leader>wd", "<C-W>c", opts)
map("n", "<leader>w-", "<C-W>s", opts)
map("n", "<leader>w|", "<C-W>v", opts)
map("n", "<leader>w2", "<C-W>v", opts)
map("n", "<leader>wh", "<C-W>h", opts)
map("n", "<leader>wj", "<C-W>j", opts)
map("n", "<leader>wl", "<C-W>l", opts)
map("n", "<leader>wk", "<C-W>k", opts)
map("n", "<leader>wH", "<C-W>5<", opts)
map("n", "<leader>wJ", "<cmd>resize +5<CR>", opts)
map("n", "<leader>wL", "<C-W>5>", opts)
map("n", "<leader>wK", "<cmd>resize -5<CR>", opts)
map("n", "<leader>w=", "<C-W>=", opts)
map("n", "<leader>ws", "<C-W>s", opts)
map("n", "<leader>wv", "<C-W>v", opts)

-- Better indenting
map("v", "<", "<gv", {})
map("v", ">", ">gv", {})

-- quit / sessions
map("n", "<leader>qq", "<cmd>q<CR>", opts)
map("n", "<leader>qw", "<cmd>w<CR>", opts)
map("n", "<leader>qs", "<cmd>SaveSession<CR>", opts)
map("n", "<leader>qr", "<cmd>RestoreSession<CR>", opts)
map("n", "<leader>ql", "<cmd>Telescope session-lens search_session<CR>", opts)

-- toggle
map("n", "<leader>od", "<cmd>Dashboard<CR>", opts)
map("n", "<leader>oe", "<cmd>NvimTreeToggle<CR>", opts)
map("n", "<leader>om", "<cmd>MinimapToggle<CR>", opts)
map("n", "<leader>os", "<cmd>SymbolsOutline<CR>", opts)
map("n", "<leader>ot", "<cmd>ToggleTerm<CR>", opts)

-- git
map("n", "<leader>go", "<cmd>LazyGit<CR>", opts)
map("n", "<leader>gl", '<cmd>TermExec cmd="git pull"<CR>', opts)
map("n", "<leader>gp", '<cmd>TermExec cmd="git push"<CR>', opts)
map("n", "<leader>gs", "<cmd>Telescope git_status<CR>", opts)
map("n", "<leader>gB", "<cmd>Telescope git_branches<CR>", opts)
map("n", "<leader>gc", "<cmd>Telescope git_commits<CR>", opts)

-- lsp
-- map("n", "<leader>cla", "<cmd>Lspsaga code_action<CR>", opts)
map("n", "<leader>cli", "<cmd>LspInfo<CR>", opts)
-- map("n", "<leader>cld", "<cmd>lua vim.lsp.buf.type_definition()<CR>", opts)
-- map("n", "<leader>cll", "<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>", opts)
-- map("n", "<leader>clL", "<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>", opts)

-- vi-viz
map("n", "<right>", "<cmd>lua require('vi-viz').vizInit()<CR>", opts)
-- expand and contract
map("x", "<right>", "<cmd>lua require('vi-viz').vizExpand()<CR>", opts)
map("x", "<left>", "<cmd>lua require('vi-viz').vizContract()<CR>", opts)
-- expand and contract by 1 char either side
map("x", "=", "<cmd>lua require('vi-viz').vizExpand1Chr()<CR>", opts)
map("x", "-", "<cmd>lua require('vi-viz').vizContract1Chr()<CR>", opts)
-- good use for the r key in visual mode
map("x", "r", "<cmd>lua require('vi-viz').vizPattern()<CR>", opts)
-- nice to have to get dot repeat on single words
map("x", "c", "<cmd>lua require('vi-viz').vizChange()<CR>", opts)
-- nice to have to insert before and after

map("x", "ii", "<cmd>lua require('vi-viz').vizInsert()<CR>", opts)

map("x", "aa", "<cmd>lua require('vi-viz').vizAppend()<CR>", opts)
