local o = vim.o
local wo = vim.wo
local opt = vim.opt
local g = vim.g
local tab = 2

-- defaults
opt.fileencoding = "utf-8"
g.mapleader = " "
o.clipboard = "unnamed,unnamedplus"
o.mouse = "a"
wo.conceallevel = 2

-- tabs
opt.expandtab = true
opt.shiftwidth = tab
opt.softtabstop = tab
opt.tabstop = tab

-- ?
o.breakindent = true

-- searching
opt.ignorecase = true
opt.smartcase = true
opt.inccommand = "split"

-- display
vim.opt.cmdheight = 1
vim.opt.list = true
-- vim.opt.listchars = {
-- 	tab = "▓░",
-- 	trail = "↔",
-- 	eol = "⏎",
-- 	extends = "→",
-- 	precedes = "←",
-- 	nbsp = "␣",
-- }
vim.opt.ruler = false
vim.opt.showmode = false
vim.opt.number = true
vim.opt.numberwidth = 3
vim.opt.relativenumber = true
vim.opt.showmatch = true
vim.opt.showtabline = 2
vim.opt.completeopt:remove("preview")
-- vim.opt.fillchars = {
-- 	diff = "░",
-- 	eob = "‣",
-- 	fold = "░",
-- 	foldopen = "▾",
-- 	foldsep = "│",
-- 	foldclose = "▸",
-- }
vim.opt.pumblend = 30
vim.opt.shada = [[!,'1000,<50,s10,h]] -- Store 1000 entries on oldfiles

if vim.fn.exists("*setcellwidths") == 1 then
	vim.fn.setcellwidths({
		--{0x2329, 0x2329, 1}, -- 〈
		--{0x232a, 0x232a, 1}, -- 〈
		{ 0x23be, 0x23cc, 2 }, -- ⎾  .. ⏌
		{ 0x2469, 0x24e9, 2 }, -- ⑩ .. ⓩ
		{ 0x24eb, 0x24fe, 2 }, -- ⓫ .. ⓾
		{ 0x2600, 0x266f, 2 }, -- ☀ .. ♯

		-- {0x23cf, 0x23cf, 2}, -- ⏏
		-- {0x23fb, 0x23fe, 2}, -- ⏻  .. ⏾ -- power-symbols-1
		{ 0x2b58, 0x2b58, 2 }, -- ⭘ -- power-symbols-2
		{ 0xe000, 0xe00a, 2 }, --  ..  -- pomicons
		-- {0xe0a0, 0xe0a2, 1}, --  ..  -- powerline-1
		-- {0xe0b0, 0xe0b3, 1}, --  ..  -- powerline-2
		-- {0xe0a3, 0xe0a3, 1}, --  -- powerline-extra-1
		-- {0xe0b4, 0xe0b7, 1}, --  ..  -- powerline-extra-2
		{ 0xe0b8, 0xefff, 2 }, --  ..  -- material-1
		{ 0xf500, 0xf546, 2 }, --  ..  -- material-2
	})
end

-- indents
vim.opt.breakindent = true
-- TODO: cannot set formatoptions?
--vim.opt.formatoptions:append{'n', 'm', 'M', 'j'}
vim.o.formatoptions = vim.o.formatoptions .. "nmMj"
vim.opt.formatlistpat = [[^\s*\%(\d\+\|[-a-z]\)\%(\ -\|[]:.)}\t]\)\?\s\+]]
vim.opt.fixendofline = false
vim.opt.showbreak = [[→]]
vim.opt.smartindent = true
