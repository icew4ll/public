-- Automatically generated packer.nvim plugin loader code

if vim.api.nvim_call_function('has', {'nvim-0.5'}) ~= 1 then
  vim.api.nvim_command('echohl WarningMsg | echom "Invalid Neovim version for packer.nvim! | echohl None"')
  return
end

vim.api.nvim_command('packadd packer.nvim')

local no_errors, error_msg = pcall(function()

  local time
  local profile_info
  local should_profile = false
  if should_profile then
    local hrtime = vim.loop.hrtime
    profile_info = {}
    time = function(chunk, start)
      if start then
        profile_info[chunk] = hrtime()
      else
        profile_info[chunk] = (hrtime() - profile_info[chunk]) / 1e6
      end
    end
  else
    time = function(chunk, start) end
  end
  
local function save_profiles(threshold)
  local sorted_times = {}
  for chunk_name, time_taken in pairs(profile_info) do
    sorted_times[#sorted_times + 1] = {chunk_name, time_taken}
  end
  table.sort(sorted_times, function(a, b) return a[2] > b[2] end)
  local results = {}
  for i, elem in ipairs(sorted_times) do
    if not threshold or threshold and elem[2] > threshold then
      results[i] = elem[1] .. ' took ' .. elem[2] .. 'ms'
    end
  end

  _G._packer = _G._packer or {}
  _G._packer.profile_output = results
end

time([[Luarocks path setup]], true)
local package_path_str = "/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?.lua;/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/share/lua/5.1/?/init.lua;/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?.lua;/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/luarocks/rocks-5.1/?/init.lua"
local install_cpath_pattern = "/home/ice/.cache/nvim/packer_hererocks/2.1.0-beta3/lib/lua/5.1/?.so"
if not string.find(package.path, package_path_str, 1, true) then
  package.path = package.path .. ';' .. package_path_str
end

if not string.find(package.cpath, install_cpath_pattern, 1, true) then
  package.cpath = package.cpath .. ';' .. install_cpath_pattern
end

time([[Luarocks path setup]], false)
time([[try_loadstring definition]], true)
local function try_loadstring(s, component, name)
  local success, result = pcall(loadstring(s), name, _G.packer_plugins[name])
  if not success then
    vim.schedule(function()
      vim.api.nvim_notify('packer.nvim: Error running ' .. component .. ' for ' .. name .. ': ' .. result, vim.log.levels.ERROR, {})
    end)
  end
  return result
end

time([[try_loadstring definition]], false)
time([[Defining packer_plugins]], true)
_G.packer_plugins = {
  LuaSnip = {
    after = { "cmp_luasnip" },
    config = { "\27LJ\2\n�\1\0\0\5\0\t\0\0206\0\0\0006\2\1\0'\3\2\0B\0\3\3\14\0\0\0X\2\1�K\0\1\0009\2\3\0019\2\4\0025\4\5\0B\2\2\0016\2\1\0'\4\6\0B\2\2\0029\2\a\2B\2\1\0016\2\1\0'\4\b\0B\2\2\1K\0\1\0\18packers.snips\tload luasnip/loaders/from_vscode\1\0\2\fhistory\2\17updateevents\29TextChanged,TextChangedI\15set_config\vconfig\fluasnip\frequire\npcall\0" },
    load_after = {
      ["nvim-cmp"] = true
    },
    loaded = false,
    needs_bufread = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/LuaSnip",
    url = "https://github.com/L3MON4D3/LuaSnip",
    wants = { "friendly-snippets" }
  },
  ["agrp.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/agrp.nvim",
    url = "https://github.com/delphinus/agrp.nvim"
  },
  ["cmp-buffer"] = {
    after = { "cmp-path" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-buffer/after/plugin/cmp_buffer.lua" },
    load_after = {
      ["cmp-nvim-lsp"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-buffer",
    url = "https://github.com/hrsh7th/cmp-buffer"
  },
  ["cmp-cmdline"] = {
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-cmdline/after/plugin/cmp_cmdline.lua" },
    load_after = {
      ["nvim-cmp"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-cmdline",
    url = "https://github.com/hrsh7th/cmp-cmdline"
  },
  ["cmp-emoji"] = {
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-emoji/after/plugin/cmp_emoji.lua" },
    load_after = {
      ["compe-tmux"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-emoji",
    url = "https://github.com/hrsh7th/cmp-emoji"
  },
  ["cmp-nvim-lsp"] = {
    after = { "cmp-buffer" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-nvim-lsp/after/plugin/cmp_nvim_lsp.lua" },
    load_after = {
      ["cmp-nvim-lua"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-nvim-lsp",
    url = "https://github.com/hrsh7th/cmp-nvim-lsp"
  },
  ["cmp-nvim-lua"] = {
    after = { "cmp-nvim-lsp" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-nvim-lua/after/plugin/cmp_nvim_lua.lua" },
    load_after = {
      cmp_luasnip = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-nvim-lua",
    url = "https://github.com/hrsh7th/cmp-nvim-lua"
  },
  ["cmp-path"] = {
    after = { "compe-tmux" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-path/after/plugin/cmp_path.lua" },
    load_after = {
      ["cmp-buffer"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp-path",
    url = "https://github.com/hrsh7th/cmp-path"
  },
  cmp_luasnip = {
    after = { "cmp-nvim-lua" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp_luasnip/after/plugin/cmp_luasnip.lua" },
    load_after = {
      LuaSnip = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cmp_luasnip",
    url = "https://github.com/saadparwaiz1/cmp_luasnip"
  },
  ["cokeline.nvim"] = {
    config = { "\27LJ\2\n*\0\1\3\0\3\0\5'\1\0\0009\2\1\0009\2\2\2&\1\2\1L\1\2\0\ticon\fdevicon\6 !\0\1\2\0\2\0\0039\1\0\0009\1\1\1L\1\2\0\ncolor\fdevicon\29\0\1\2\0\1\0\0029\1\0\0L\1\2\0\18unique_prefix\"\0\1\3\0\2\0\0049\1\0\0'\2\1\0&\1\2\1L\1\2\0\6 \rfilename�\3\1\0\v\0\31\0B6\0\0\0'\2\1\0B\0\2\0029\0\2\0006\1\0\0'\3\3\0B\1\2\0029\1\4\0015\3\14\0005\4\n\0005\5\a\0\18\6\0\0'\b\5\0'\t\6\0B\6\3\2=\6\b\5\18\6\0\0'\b\t\0'\t\b\0B\6\3\2=\6\6\5=\5\v\0045\5\f\0\18\6\0\0'\b\t\0'\t\b\0B\6\3\2=\6\b\5\18\6\0\0'\b\5\0'\t\6\0B\6\3\2=\6\6\5=\5\r\4=\4\15\0034\4\6\0005\5\17\0003\6\16\0=\6\18\0055\6\20\0003\a\19\0=\a\b\6=\6\21\5>\5\1\0045\5\23\0003\6\22\0=\6\18\0055\6\25\0\18\a\0\0'\t\24\0'\n\b\0B\a\3\2=\a\b\6=\6\21\5>\5\2\0045\5\27\0003\6\26\0=\6\18\5>\5\3\0045\5\28\0>\5\4\0045\5\29\0>\5\5\4=\4\30\3B\1\2\1K\0\1\0\15components\1\0\1\ttext\6 \1\0\2\ttext\b delete_buffer_on_left_click\2\1\0\0\0\1\0\1\nstyle\vitalic\fComment\1\0\0\0\ahl\1\0\0\0\ttext\1\0\0\0\15default_hl\1\0\0\14unfocused\1\0\0\ffocused\1\0\0\vNormal\afg\1\0\0\abg\16ColorColumn\nsetup\rcokeline\fget_hex\19cokeline/utils\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/cokeline.nvim",
    url = "https://github.com/noib3/cokeline.nvim"
  },
  ["compe-tmux"] = {
    after = { "cmp-emoji" },
    after_files = { "/home/ice/.local/share/nvim/site/pack/packer/opt/compe-tmux/after/plugin/compe_tmux.vim" },
    load_after = {
      ["cmp-path"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/compe-tmux",
    url = "https://github.com/andersevenrud/compe-tmux"
  },
  ["elvish.vim"] = {
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/elvish.vim",
    url = "https://github.com/dmix/elvish.vim"
  },
  ["fm-nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/fm-nvim",
    url = "https://github.com/is0n/fm-nvim"
  },
  ["friendly-snippets"] = {
    after = { "nvim-cmp" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/friendly-snippets",
    url = "https://github.com/rafamadriz/friendly-snippets"
  },
  ["gitsigns.nvim"] = {
    config = { "\27LJ\2\n�\2\0\0\5\0\18\0\0216\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\14\0005\3\4\0005\4\3\0=\4\5\0035\4\6\0=\4\a\0035\4\b\0=\4\t\0035\4\n\0=\4\v\0035\4\f\0=\4\r\3=\3\15\0025\3\16\0=\3\17\2B\0\2\1K\0\1\0\28current_line_blame_opts\1\0\1\ndelay\3\n\nsigns\1\0\3\23current_line_blame\2\14word_diff\2\nnumhl\2\17changedelete\1\0\2\ttext\b•\ahl\19GitSignsChange\14topdelete\1\0\2\ttext\b↑\ahl\19GitSignsDelete\vdelete\1\0\2\ttext\b✗\ahl\19GitSignsDelete\vchange\1\0\1\ahl\19GitSignsChange\badd\1\0\0\1\0\1\ahl\16GitSignsAdd\nsetup\rgitsigns\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/gitsigns.nvim",
    url = "https://github.com/lewis6991/gitsigns.nvim"
  },
  ["gruvbox-flat.nvim"] = {
    config = { "\27LJ\2\nF\0\0\3\0\4\0\0066\0\0\0009\0\1\0009\0\2\0'\2\3\0B\0\2\1K\0\1\0\22colo gruvbox-flat\17nvim_command\bapi\bvim\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/gruvbox-flat.nvim",
    url = "https://github.com/eddyekofo94/gruvbox-flat.nvim"
  },
  ["impatient.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/impatient.nvim",
    url = "https://github.com/lewis6991/impatient.nvim"
  },
  ["indent-blankline.nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/indent-blankline.nvim",
    url = "https://github.com/lukas-reineke/indent-blankline.nvim"
  },
  ["ion-vim"] = {
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/ion-vim",
    url = "https://github.com/vmchale/ion-vim"
  },
  ["lightspeed.nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/lightspeed.nvim",
    url = "https://github.com/ggandor/lightspeed.nvim"
  },
  ["lsp_signature.nvim"] = {
    config = { "\27LJ\2\n�\2\0\0\6\0\a\0\f6\0\0\0006\2\1\0'\3\2\0B\0\3\3\15\0\0\0X\2\5�9\2\3\0015\4\4\0005\5\5\0=\5\6\4B\2\2\1K\0\1\0\17handler_opts\1\0\1\vborder\vsingle\1\0\f\16hint_enable\2\17hi_parameter\vSearch\16hint_scheme\vString\ffix_pos\2\14doc_lines\3\0\20floating_window\2\14max_width\3x\15max_height\3\22\16hint_prefix\t \tbind\2\fpadding\5\vzindex\3�\1\nsetup\18lsp_signature\frequire\npcall\0" },
    load_after = {
      ["nvim-lspconfig"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/lsp_signature.nvim",
    url = "https://github.com/ray-x/lsp_signature.nvim"
  },
  ["lspkind-nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/lspkind-nvim",
    url = "https://github.com/onsails/lspkind-nvim"
  },
  ["lualine.nvim"] = {
    config = { "\27LJ\2\n/\0\0\3\0\2\0\0046\0\0\0'\2\1\0B\0\2\1K\0\1\0\20packers.lualine\frequire\0" },
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/lualine.nvim",
    url = "https://github.com/nvim-lualine/lualine.nvim"
  },
  ["mappy.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/mappy.nvim",
    url = "https://github.com/delphinus/mappy.nvim"
  },
  ["null-ls.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/null-ls.nvim",
    url = "https://github.com/jose-elias-alvarez/null-ls.nvim"
  },
  ["nvim-autopairs"] = {
    config = { "\27LJ\2\n�\2\0\0\n\0\15\0\0256\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0005\3\3\0=\3\5\2B\0\2\0016\0\0\0'\2\6\0B\0\2\0026\1\0\0'\3\a\0B\1\2\0029\2\b\1\18\4\2\0009\2\t\2'\5\n\0009\6\v\0005\b\r\0005\t\f\0=\t\14\bB\6\2\0A\2\2\1K\0\1\0\rmap_char\1\0\0\1\0\1\btex\5\20on_confirm_done\17confirm_done\aon\nevent\bcmp\"nvim-autopairs.completion.cmp\21disable_filetype\1\0\0\1\3\0\0\20TelescopePrompt\bvim\nsetup\19nvim-autopairs\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-autopairs",
    url = "https://github.com/windwp/nvim-autopairs"
  },
  ["nvim-cmp"] = {
    after = { "LuaSnip", "cmp-cmdline" },
    config = { "\27LJ\2\nC\0\1\4\0\4\0\a6\1\0\0'\3\1\0B\1\2\0029\1\2\0019\3\3\0B\1\2\1K\0\1\0\tbody\15lsp_expand\fluasnip\frequire�\1\0\2\b\0\v\0\0186\2\1\0009\2\2\2'\4\3\0006\5\4\0'\a\5\0B\5\2\0029\5\6\0059\6\0\0018\5\6\0059\6\0\1B\2\4\2=\2\0\0015\2\b\0009\3\t\0009\3\n\0038\2\3\2=\2\a\1L\1\2\0\tname\vsource\1\0\a\fluasnip\t📜\nemoji\t🍵\ttmux\t💻\vbuffer\t🔮\rnvim_lua\t🌙\tpath\t🧭\rnvim_lsp\t🏹\tmenu\nicons\20packers.lspkind\frequire\n%s %s\vformat\vstring\tkind�\2\0\1\t\1\f\0!-\1\0\0009\1\0\1B\1\1\2\15\0\1\0X\2\4�-\1\0\0009\1\1\1B\1\1\1X\1\23�6\1\2\0'\3\3\0B\1\2\0029\1\4\1B\1\1\2\15\0\1\0X\2\14�6\1\5\0009\1\6\0019\1\a\0016\3\5\0009\3\b\0039\3\t\3'\5\n\0+\6\2\0+\a\2\0+\b\2\0B\3\5\2'\4\v\0B\1\3\1X\1\2�\18\1\0\0B\1\1\1K\0\1\0\1�\5!<Plug>luasnip-expand-or-jump\27nvim_replace_termcodes\bapi\rfeedkeys\afn\bvim\23expand_or_jumpable\fluasnip\frequire\21select_next_item\fvisible�\2\0\1\t\1\f\0\"-\1\0\0009\1\0\1B\1\1\2\15\0\1\0X\2\4�-\1\0\0009\1\1\1B\1\1\1X\1\24�6\1\2\0'\3\3\0B\1\2\0029\1\4\1)\3��B\1\2\2\15\0\1\0X\2\14�6\1\5\0009\1\6\0019\1\a\0016\3\5\0009\3\b\0039\3\t\3'\5\n\0+\6\2\0+\a\2\0+\b\2\0B\3\5\2'\4\v\0B\1\3\1X\1\2�\18\1\0\0B\1\1\1K\0\1\0\1�\5\28<Plug>luasnip-jump-prev\27nvim_replace_termcodes\bapi\rfeedkeys\afn\bvim\rjumpable\fluasnip\frequire\21select_prev_item\fvisible�\5\1\0\n\0001\0S6\0\0\0006\2\1\0'\3\2\0B\0\3\3\14\0\0\0X\2\1�2\0K�6\2\3\0009\2\4\2'\3\6\0=\3\5\0029\2\a\0015\4\v\0005\5\t\0003\6\b\0=\6\n\5=\5\f\0045\5\14\0003\6\r\0=\6\15\5=\5\16\0045\5\19\0009\6\17\0019\6\18\6B\6\1\2=\6\20\0059\6\17\0019\6\21\6B\6\1\2=\6\22\0059\6\17\0019\6\23\6)\b��B\6\2\2=\6\24\0059\6\17\0019\6\23\6)\b\4\0B\6\2\2=\6\25\0059\6\17\0019\6\26\6B\6\1\2=\6\27\0059\6\17\0019\6\28\6B\6\1\2=\6\29\0059\6\17\0019\6\30\0065\b!\0009\t\31\0019\t \t=\t\"\bB\6\2\2=\6#\0053\6$\0=\6%\0053\6&\0=\6'\5=\5\17\0044\5\t\0005\6(\0>\6\1\0055\6)\0>\6\2\0055\6*\0>\6\3\0055\6+\0>\6\4\0055\6,\0>\6\5\0055\6-\0>\6\6\0055\6.\0>\6\a\0055\6/\0>\6\b\5=\0050\4B\2\2\0012\0\0�K\0\1\0K\0\1\0\fsources\1\0\1\tname\forgmode\1\0\1\tname\nemoji\1\0\1\tname\ttmux\1\0\1\tname\tpath\1\0\1\tname\rnvim_lua\1\0\1\tname\vbuffer\1\0\1\tname\fluasnip\1\0\1\tname\rnvim_lsp\f<S-Tab>\0\n<Tab>\0\t<CR>\rbehavior\1\0\1\vselect\2\fReplace\20ConfirmBehavior\fconfirm\n<C-e>\nclose\14<C-Space>\rcomplete\n<C-f>\n<C-d>\16scroll_docs\n<C-n>\21select_next_item\n<C-p>\1\0\0\21select_prev_item\fmapping\15formatting\vformat\1\0\0\0\fsnippet\1\0\0\vexpand\1\0\0\0\nsetup\21menuone,noselect\16completeopt\bopt\bvim\bcmp\frequire\npcall\0" },
    load_after = {
      ["friendly-snippets"] = true
    },
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-cmp",
    url = "https://github.com/hrsh7th/nvim-cmp"
  },
  ["nvim-colorizer.lua"] = {
    config = { "\27LJ\2\nA\0\0\3\0\4\0\a6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\3\0B\0\2\1K\0\1\0\1\2\0\0\6*\nsetup\14colorizer\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-colorizer.lua",
    url = "https://github.com/norcalli/nvim-colorizer.lua"
  },
  ["nvim-comment"] = {
    config = { "\27LJ\2\n�\2\0\0\4\0\n\0(6\0\0\0009\0\1\0009\0\2\0)\2\0\0'\3\3\0B\0\3\2\a\0\4\0X\0\5�6\0\5\0'\2\6\0B\0\2\0029\0\a\0B\0\1\0016\0\0\0009\0\1\0009\0\2\0)\2\0\0'\3\3\0B\0\3\2\a\0\b\0X\0\5�6\0\5\0'\2\6\0B\0\2\0029\0\a\0B\0\1\0016\0\0\0009\0\1\0009\0\2\0)\2\0\0'\3\3\0B\0\3\2\a\0\t\0X\0\5�6\0\5\0'\2\6\0B\0\2\0029\0\a\0B\0\1\1K\0\1\0\20javascriptreact\20typescriptreact\25update_commentstring&ts_context_commentstring.internal\frequire\bvue\rfiletype\24nvim_buf_get_option\bapi\bvimO\1\0\4\0\6\0\t6\0\0\0'\2\1\0B\0\2\0029\0\2\0005\2\4\0003\3\3\0=\3\5\2B\0\2\1K\0\1\0\thook\1\0\0\0\nsetup\17nvim_comment\frequire\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-comment",
    url = "https://github.com/terrortylor/nvim-comment"
  },
  ["nvim-lsp-ts-utils"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/nvim-lsp-ts-utils",
    url = "https://github.com/jose-elias-alvarez/nvim-lsp-ts-utils"
  },
  ["nvim-lspconfig"] = {
    after = { "lsp_signature.nvim" },
    config = { "\27LJ\2\nU\0\0\6\0\5\0\n6\0\0\0006\2\1\0009\2\2\0026\4\1\0009\4\3\0049\4\4\4B\4\1\0A\2\0\0A\0\0\1K\0\1\0\20buf_get_clients\blsp\finspect\bvim\nprintk\0\0\4\0\6\0\r6\0\0\0009\0\1\0009\0\2\0006\2\0\0009\2\1\0029\2\3\2B\2\1\0A\0\0\0016\0\0\0009\0\4\0'\2\5\0B\0\2\1K\0\1\0\tedit\bcmd\23get_active_clients\16stop_client\blsp\bvimj\0\0\5\1\b\0\v6\0\0\0009\0\1\0009\0\2\0009\0\3\0005\2\6\0005\3\4\0-\4\0\0=\4\5\3=\3\a\2B\0\2\1K\0\1\0\1\0\15popup_opts\1\0\0\vborder\1\0\0\14goto_next\15diagnostic\blsp\bvimj\0\0\5\1\b\0\v6\0\0\0009\0\1\0009\0\2\0009\0\3\0005\2\6\0005\3\4\0-\4\0\0=\4\5\3=\3\a\2B\0\2\1K\0\1\0\1\0\15popup_opts\1\0\0\vborder\1\0\0\14goto_prev\15diagnostic\blsp\bvim�\1\0\0\2\0\a\0\0246\0\0\0009\0\1\0009\0\2\0\15\0\0\0X\1\6�6\0\0\0009\0\3\0009\0\4\0009\0\5\0B\0\1\1X\0\5�6\0\0\0009\0\3\0009\0\4\0009\0\6\0B\0\1\0016\0\0\0009\0\1\0006\1\0\0009\1\1\0019\1\2\1\19\1\1\0=\1\2\0K\0\1\0\fdisable\venable\15diagnostic\blsp\29lsp_diagnostics_disabled\6b\bvim5\0\0\2\0\3\0\0056\0\0\0009\0\1\0009\0\2\0B\0\1\1K\0\1\0\15open_float\15diagnostic\bvimP\0\0\3\0\6\0\n6\0\0\0009\0\1\0'\2\2\0B\0\2\0016\0\0\0009\0\3\0009\0\4\0009\0\5\0B\0\1\1K\0\1\0\15definition\bbuf\blsp\nsplit\bcmd\bvim�\4\1\0\5\3\"\0_-\0\0\0009\0\0\0'\2\1\0005\3\2\0003\4\3\0B\0\4\1-\0\0\0009\0\0\0'\2\1\0005\3\4\0003\4\5\0B\0\4\1-\0\0\0009\0\6\0'\2\a\0003\3\b\0B\0\3\1-\0\0\0009\0\6\0'\2\t\0003\3\n\0B\0\3\1-\0\2\0\14\0\0\0X\0E�-\0\0\0009\0\6\0'\2\v\0006\3\f\0009\3\r\0039\3\14\0039\3\15\3B\0\3\1-\0\0\0009\0\6\0'\2\16\0006\3\f\0009\3\r\0039\3\14\0039\3\17\3B\0\3\0016\0\f\0009\0\18\0009\0\19\0\18\2\0\0009\0\20\0B\0\2\2\6\0\21\0X\0\r�-\0\0\0009\0\6\0'\2\22\0006\3\f\0009\3\r\0039\3\14\0039\3\23\3B\0\3\1-\0\0\0009\0\6\0'\2\24\0003\3\25\0B\0\3\1-\0\0\0009\0\6\0'\2\26\0006\3\f\0009\3\r\0039\3\14\0039\3\27\3B\0\3\1-\0\0\0009\0\6\0'\2\28\0006\3\f\0009\3\r\0039\3\14\0039\3\29\3B\0\3\1-\0\0\0009\0\6\0'\2\30\0006\3\f\0009\3\r\0039\3\14\0039\3\31\3B\0\3\1-\0\0\0009\0\6\0'\2 \0006\3\f\0009\3\r\0039\3\14\0039\3!\3B\0\3\1K\0\1\0\0\0\1\0\2\0\15references\agf\16declaration\age\vrename\agr\16code_action\aga\0\15<C-w><C-]>\15definition\n<C-]>\thelp\bget\rfiletype\bopt\20type_definition\agd\nhover\bbuf\blsp\bvim\agk\0\agl\0\r<Space>E\rnnoremap\0\1\3\0\0\n<A-K>\14<A-S->\0\1\3\0\0\n<A-J>\r<A-S-Ô>\6n\tbind�\2\1\2\b\3\r\0\0276\2\0\0'\4\1\0\18\6\4\0009\4\2\4\18\a\1\0B\4\3\0A\2\0\0019\2\3\0009\2\4\2\15\0\2\0X\3\4�9\2\3\0009\2\4\2+\3\2\0=\3\5\0026\2\6\0009\2\a\0029\2\b\2\18\4\1\0'\5\t\0'\6\n\0B\2\4\1-\2\0\0009\2\v\0023\4\f\0B\2\2\1K\0\1\0\0\0\1\0\0�\0\20add_buffer_maps\27v:lua.vim.lsp.omnifunc\romnifunc\24nvim_buf_set_option\bapi\bvim\27allow_incremental_sync\nflags\vconfig\vformat\28LSP started: bufnr = %d\nprint\24\1\1\2\2\1\0\0033\1\0\0002\0\0�L\1\2\0\0�\4�\0n\0\1\a\1\5\0\18-\1\0\0009\1\0\0019\1\1\1\18\3\0\0B\1\2\2\15\0\1\0X\2\n�-\1\0\0009\1\0\0019\1\2\1-\3\0\0009\3\0\0039\3\3\3\18\5\0\0'\6\4\0B\3\3\0A\1\0\2L\1\2\0\a�\t.git\tjoin\vexists\vis_dir\tpath�\1\0\1\v\0\6\0\21\18\3\0\0009\1\0\0'\4\1\0'\5\2\0B\1\4\0026\2\3\0005\4\4\0B\2\2\4X\5\b�\18\t\1\0009\a\5\1\18\n\6\0B\a\3\2\15\0\a\0X\b\2�+\a\2\0L\a\2\0E\5\3\3R\5�+\2\1\0L\2\2\0\nmatch\1\b\0\0\n^deno\t^ddc\16^cmp%-look$\16^neco%-vim$\17^git%-vines$\f^murus$\16^skkeleton$\vipairs\5\b.*/\tgsub�\3\0\2\a\0\n\0\0176\2\0\0'\4\1\0B\2\2\0029\3\2\0025\5\3\0005\6\4\0=\6\5\0054\6\0\0=\6\6\0054\6\0\0=\6\a\5B\3\2\0019\3\b\2\18\5\0\0B\3\2\0015\3\t\0K\0\1\0\1\0\1\vsilent\2\17setup_client#filter_out_diagnostics_by_code'filter_out_diagnostics_by_severity\26import_all_priorities\1\0\4\14same_file\3\1\16local_files\3\2\19buffer_content\3\3\fbuffers\3\4\1\0\n enable_import_on_completion\1\26inlay_hints_highlight\fComment\21auto_inlay_hints\2\23import_all_timeout\3�'\27update_imports_on_move\1\28import_all_scan_buffers\3d\21disable_commands\1!require_confirmation_on_move\1\ndebug\1\29import_all_select_source\1\nsetup\22nvim-lsp-ts-utils\frequire3\0\1\4\2\0\0\n-\1\0\0\18\3\0\0B\1\2\2\15\0\1\0X\2\4�-\1\1\0\18\3\0\0B\1\2\2\19\1\1\0L\1\2\0\1\0\2\0007\1\1\5\3\2\0\6-\1\0\0009\1\0\1\18\3\0\0003\4\1\0002\0\0�D\1\3\0\a�\b�\t�\0\21search_ancestors�\5\0\0\v\1)\0A6\0\0\0009\0\1\0009\0\2\0B\0\1\2'\1\3\0&\0\1\0'\1\4\0\18\3\1\0009\1\5\1\18\4\0\0006\5\0\0009\5\1\0059\5\6\5B\5\1\0029\5\a\5\a\5\b\0X\5\2�'\5\t\0X\6\1�'\5\n\0B\1\4\0025\2\v\0-\3\0\0B\3\1\2=\3\f\0025\3\r\0>\1\1\3\18\4\0\0'\5\14\0&\4\5\4>\4\3\3=\3\15\0025\3&\0005\4\21\0005\5\16\0006\6\0\0009\6\17\0066\b\18\0009\b\19\b'\t\20\0B\6\3\2=\6\19\5=\5\22\0045\5\23\0=\5\24\0045\5\25\0005\6\26\0=\6\27\5=\5\28\0045\5\"\0005\6 \0006\a\0\0009\a\29\a9\a\30\a'\t\31\0+\n\2\0B\a\3\2=\a!\6=\6!\5=\5#\0045\5$\0=\5%\4=\4'\3=\3(\2L\2\2\0\5�\rsettings\bLua\1\0\0\14telemetry\1\0\1\venable\1\14workspace\1\0\0\flibrary\1\0\1\20checkThirdParty\1\5\26nvim_get_runtime_file\bapi\16diagnostics\fglobals\1\b\0\0\bvim\rdescribe\ait\16before_each\15after_each\19packer_plugins\ahs\1\0\1\venable\2\15completion\1\0\1\19keywordSnippet\fDisable\fruntime\1\0\0\6;\tpath\fpackage\nsplit\1\0\1\fversion\vLuaJIT\bcmd\14/main.lua\1\3\0\0\0\a-E\14on_attach\1\0\0\nLinux\nmacOS\vDarwin\fsysname\ros_uname\vformat\"%s/bin/%s/lua-language-server#/bin/build/lua-language-server\15os_homedir\tloop\bvim�\20\1\0\21\0t\1�\0026\0\0\0'\2\1\0B\0\2\0026\1\2\0009\1\3\0019\1\4\1B\1\1\2'\2\5\0&\1\2\1\18\2\1\0'\3\6\0&\2\3\0026\3\2\0009\3\a\0039\3\b\3\18\4\3\0'\6\t\0005\a\n\0B\4\3\1\18\4\3\0'\6\v\0005\a\f\0B\4\3\1\18\4\3\0'\6\r\0005\a\14\0B\4\3\1\18\4\3\0'\6\15\0005\a\16\0B\4\3\0016\4\17\0003\5\19\0=\5\18\0046\4\17\0003\5\21\0=\5\20\0046\4\2\0009\4\22\0049\4\23\4'\6\24\0+\a\1\0B\4\3\0014\4\t\0005\5\25\0>\5\1\0045\5\26\0>\5\2\0045\5\27\0>\5\3\0045\5\28\0>\5\4\0045\5\29\0>\5\5\0045\5\30\0>\5\6\0045\5\31\0>\5\a\0045\5 \0>\5\b\0043\5!\0006\6\2\0009\6\"\0069\6#\0066\a\2\0009\a\"\a9\a%\a6\t\2\0009\t\"\t9\t&\t9\t'\t5\n(\0B\a\3\2=\a$\0066\6\2\0009\6\"\0069\6#\0066\a\2\0009\a\"\a9\a%\a6\t\2\0009\t\"\t9\t#\t9\t*\t5\n+\0=\4,\nB\a\3\2=\a)\0066\6\2\0009\6\"\0069\6#\0066\a\2\0009\a\"\a9\a%\a6\t\2\0009\t\"\t9\t#\t9\t.\t5\n/\0=\4,\nB\a\3\2=\a-\0066\6\0\0'\b0\0B\6\2\0026\a\0\0'\t1\0B\a\2\0023\b2\0003\t3\0006\n\0\0'\f4\0B\n\2\0026\v\2\0009\v\3\v9\v\4\vB\v\1\0024\f\v\0009\r5\n9\r6\r9\r7\r9\r%\r5\0159\0005\0168\0=\16:\15B\r\2\2>\r\1\f9\r5\n9\r6\r9\r;\r9\r%\r5\15=\0005\16<\0=\16:\15B\r\2\2>\r\2\f9\r5\n9\r6\r9\r>\r9\r%\r5\15@\0005\16?\0=\16:\15B\r\2\2>\r\3\f9\r5\n9\rA\r9\rB\r9\r%\r5\15D\0005\16C\0=\16:\15B\r\2\2>\r\4\f9\r5\n9\r6\r9\rE\r9\r%\r5\15G\0005\16F\0=\16:\15B\r\2\2>\r\5\f9\r5\n9\r6\r9\rH\r9\r%\r5\15J\0005\16I\0=\16:\15B\r\2\2>\r\6\f9\r5\n9\r6\r9\rK\r9\r%\r5\15M\0005\16L\0=\16:\15B\r\2\2>\r\a\f9\r5\n9\rA\r9\rN\r9\r%\r5\15P\0005\16O\0=\16:\0154\16\3\0'\17Q\0\18\18\v\0'\19R\0&\17\19\17>\17\1\16=\16S\15B\r\2\2>\r\b\f9\r5\n9\rA\r9\rT\r9\r%\r5\15V\0005\16U\0=\16:\15B\r\2\2>\r\t\f9\r5\n9\rA\r9\rW\r9\r%\r5\15Y\0005\16X\0=\16:\15B\r\2\0?\r\0\0009\rZ\n5\15[\0=\f\\\15B\r\2\0016\r]\0005\15`\0005\16^\0\18\17\5\0B\17\1\2=\17_\16=\16a\0155\16b\0\18\17\5\0B\17\1\2=\17_\16=\16c\0155\16d\0004\17\3\0>\2\1\17=\17e\16\18\17\5\0B\17\1\2=\17_\16=\16f\0155\16g\0\18\17\5\0B\17\1\2=\17_\16=\16h\0155\16i\0\18\17\5\0B\17\1\2=\17_\16=\16j\0155\16m\0006\17\0\0'\19k\0B\17\2\0029\17l\17=\17l\0163\17n\0=\17_\0163\17o\0=\17p\16=\16q\0153\16r\0B\16\1\2=\16s\15B\r\2\4H\16\4�8\18\16\0069\18Z\18\18\20\17\0B\18\2\1F\16\3\3R\16�2\0\0�K\0\1\0\16sumneko_lua\0\rtsserver\rroot_dir\0\0\1\0\0\17init_options\22nvim-lsp-ts-utils\veslint\1\0\0\fpyright\1\0\0\bvls\bcmd\1\0\0\18rust_analyzer\1\0\0\vbashls\1\0\0\14on_attach\1\0\0\npairs\fsources\1\0\0\nsetup\1\0\0\1\2\0\0\tyaml\ryamllint\1\0\0\1\2\0\0\ash\15shellcheck\15extra_args!/.config/null-ls/selene.toml\14--config \1\0\0\1\2\0\0\blua\vselene\1\0\0\1\t\0\0\thtml\tjson\tyaml\rmarkdown\fgraphql\bcss\tscss\tless\14prettierd\1\0\0\1\2\0\0\ash\nshfmt\1\0\0\1\2\0\0\trust\frustfmt\1\0\0\1\2\0\0\vpython\vpylint\16diagnostics\1\0\0\1\2\0\0\vpython\27reorder_python_imports\1\0\0\1\2\0\0\vpython\nblack\14filetypes\1\0\0\1\2\0\0\blua\vstylua\15formatting\rbuiltins\fnull-ls\0\0\19lspconfig.util\14lspconfig\1\0\0\19signature_help\31textDocument/signatureHelp\vborder\1\0\0\nhover\23textDocument/hover\1\0\3\14underline\2\17virtual_text\2\nsigns\2\27on_publish_diagnostics\15diagnostic\twith$textDocument/publishDiagnostics\rhandlers\blsp\0\1\3\0\0\b⡇\18LspBorderLeft\1\3\0\0\b⠉\20LspBorderBottom\1\3\0\0\b⠉\20LspBorderBottom\1\3\0\0\b⠉\20LspBorderBottom\1\3\0\0\b⢸\19LspBorderRight\1\3\0\0\b⣀\17LspBorderTop\1\3\0\0\b⣀\17LspBorderTop\1\3\0\0\b⣀\17LspBorderTop�\1        hi LspBorderTop guifg=#5d9794 guibg=#2e3440\n        hi LspBorderLeft guifg=#5d9794 guibg=#3b4252\n        hi LspBorderRight guifg=#5d9794 guibg=#3b4252\n        hi LspBorderBottom guifg=#5d9794 guibg=#2e3440\n      \14nvim_exec\bapi\0\22ReloadLSPSettings\0\20ShowLSPSettings\a_G\1\0\2\vtexthl\23DiagnosticSignHint\ttext\t🌵\23DiagnosticSignHint\1\0\2\vtexthl\23DiagnosticSignInfo\ttext\t🌵\23DiagnosticSignInfo\1\0\2\vtexthl\23DiagnosticSignWarn\ttext\t🍯\23DiagnosticSignWarn\1\0\2\vtexthl\24DiagnosticSignError\ttext\t🔮\24DiagnosticSignError\16sign_define\afn\r/bin/vls\19/bin/build/vls\15os_homedir\tloop\bvim\nmappy\frequire\21����\4\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-lspconfig",
    url = "https://github.com/neovim/nvim-lspconfig"
  },
  ["nvim-treesitter"] = {
    config = { "\27LJ\2\n�\v\0\0\a\0007\0G6\0\0\0'\2\1\0B\0\2\0029\0\2\0B\0\1\0025\1\a\0005\2\4\0005\3\5\0=\3\6\2=\2\b\1=\1\3\0005\1\f\0005\2\n\0005\3\v\0=\3\6\2=\2\b\1=\1\t\0006\1\0\0'\3\r\0B\1\2\0029\1\14\0015\3\20\0005\4\15\0005\5\16\0=\5\17\0045\5\18\0=\5\19\4=\4\21\0035\4\22\0005\5\23\0=\5\24\4=\4\25\0035\4\26\0=\4\27\0035\4\28\0=\4\29\0035\4 \0005\5\30\0005\6\31\0=\6\24\5=\5!\4=\4\"\0035\4#\0=\4$\0035\4%\0=\4&\0035\4'\0=\4(\3B\1\2\0016\1\0\0'\3)\0B\1\2\0029\1\14\0015\3+\0005\4*\0=\4,\0035\4-\0=\4.\0035\0040\0005\5/\0=\0051\4=\0042\3B\1\2\0016\1\0\0'\0033\0B\1\2\0029\0014\1'\0035\0'\0046\0B\1\3\1K\0\1\0,<Cmd>TSHighlightCapturesUnderCursor<CR>\r<Space>h\rnnoremap\nmappy\rmappings\vglobal\1\0\0\1\0\2\15org_agenda\age\16org_capture\agt\21org_agenda_files\1\2\0\0\f~/m/vim\27org_todo_keyword_faces\1\0\1\27org_default_notes_file\24~/m/vim/default.org\1\0\3\14DELEGATED4:background #FFFFFF :slant italic :underline on\fWAITING\":foreground blue :weight bold\tTODO(:background #000000 :foreground red\forgmode\frainbow\1\0\1\venable\2\fautotag\1\0\1\venable\2\26context_commentstring\1\0\1\venable\2\rrefactor\17smart_rename\1\0\0\1\0\1\17smart_rename\agr\1\0\1\venable\2\21ensure_installed\1\19\0\0\borg\tjson\bcss\15typescript\15javascript\thtml\btsx\tyaml\trust\vpython\tbash\6v\blua\nregex\bvue\fgraphql\ttoml\ago\vindent\1\0\1\venable\2\26incremental_selection\fkeymaps\1\0\3\21node_decremental\6,\19init_selection\agi\21node_incremental\6.\1\0\1\venable\2\14highlight\1\0\0&additional_vim_regex_highlighting\1\2\0\0\borg\fdisable\1\2\0\0\borg\1\0\1\venable\2\nsetup\28nvim-treesitter.configs\1\0\1\rfiletype\nvlang\1\2\0\0\17src/parser.c\1\0\2\burl-https://github.com/nedpals/tree-sitter-v\rrevision\vmaster\6v\17install_info\1\0\1\rfiletype\borg\nfiles\1\3\0\0\17src/parser.c\19src/scanner.cc\1\0\2\burl0https://github.com/milisims/tree-sitter-org\rrevision\tmain\borg\23get_parser_configs\28nvim-treesitter.parsers\frequire\0" },
    load_after = {
      ["nvim-treesitter-context"] = true,
      ["nvim-treesitter-refactor"] = true,
      ["nvim-ts-autotag"] = true,
      ["nvim-ts-context-commentstring"] = true,
      ["nvim-ts-rainbow"] = true,
      ["treesitter-unit"] = true
    },
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-treesitter",
    url = "https://github.com/nvim-treesitter/nvim-treesitter"
  },
  ["nvim-treesitter-context"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-treesitter-context",
    url = "https://github.com/romgrk/nvim-treesitter-context"
  },
  ["nvim-treesitter-refactor"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-treesitter-refactor",
    url = "https://github.com/nvim-treesitter/nvim-treesitter-refactor"
  },
  ["nvim-ts-autotag"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-ts-autotag",
    url = "https://github.com/windwp/nvim-ts-autotag"
  },
  ["nvim-ts-context-commentstring"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-ts-context-commentstring",
    url = "https://github.com/JoosepAlviste/nvim-ts-context-commentstring"
  },
  ["nvim-ts-rainbow"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/nvim-ts-rainbow",
    url = "https://github.com/p00f/nvim-ts-rainbow"
  },
  ["nvim-web-devicons"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/nvim-web-devicons",
    url = "https://github.com/kyazdani42/nvim-web-devicons"
  },
  ["orgmode.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/orgmode.nvim",
    url = "https://github.com/kristijanhusak/orgmode.nvim"
  },
  ["packer.nvim"] = {
    loaded = false,
    needs_bufread = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/packer.nvim",
    url = "https://github.com/wbthomason/packer.nvim"
  },
  ["plenary.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/plenary.nvim",
    url = "https://github.com/nvim-lua/plenary.nvim"
  },
  ["symbols-outline.nvim"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/symbols-outline.nvim",
    url = "https://github.com/simrat39/symbols-outline.nvim"
  },
  ["telescope-fzf-native.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/telescope-fzf-native.nvim",
    url = "https://github.com/nvim-telescope/telescope-fzf-native.nvim"
  },
  ["telescope-media-files.nvim"] = {
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/telescope-media-files.nvim",
    url = "https://github.com/nvim-telescope/telescope-media-files.nvim"
  },
  ["telescope.nvim"] = {
    config = { "\27LJ\2\n1\0\0\3\0\2\0\0046\0\0\0'\2\1\0B\0\2\1K\0\1\0\22packers.telescope\frequire\0" },
    loaded = true,
    path = "/home/ice/.local/share/nvim/site/pack/packer/start/telescope.nvim",
    url = "https://github.com/nvim-telescope/telescope.nvim"
  },
  ["treesitter-unit"] = {
    after = { "nvim-treesitter" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/treesitter-unit",
    url = "https://github.com/David-Kunz/treesitter-unit"
  },
  ["trouble.nvim"] = {
    commands = { "TroubleToggle", "Trouble" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/trouble.nvim",
    url = "https://github.com/folke/trouble.nvim"
  },
  ["v-vim"] = {
    loaded = false,
    needs_bufread = true,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/v-vim",
    url = "https://github.com/ollykel/v-vim"
  },
  ["vi-viz"] = {
    config = { "\27LJ\2\n�\1\0\4\v\0\a\0\0246\4\0\0009\4\1\0049\4\2\4\18\6\0\0\18\a\1\0\18\b\2\0005\t\3\0009\n\4\3\14\0\n\0X\v\1�+\n\1\0=\n\4\t9\n\5\3\14\0\n\0X\v\1�+\n\1\0=\n\5\t9\n\6\3\14\0\n\0X\v\1�+\n\1\0=\n\6\tB\4\5\1K\0\1\0\vscript\texpr\vsilent\1\0\1\fnoremap\2\20nvim_set_keymap\bapi\bvim�\5\1\0\b\0\21\00095\0\0\0003\1\1\0\18\2\1\0'\4\2\0'\5\3\0'\6\4\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\3\0'\6\6\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\a\0'\6\b\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\t\0'\6\n\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\v\0'\6\f\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\r\0'\6\14\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\15\0'\6\16\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\17\0'\6\18\0\18\a\0\0B\2\5\1\18\2\1\0'\4\5\0'\5\19\0'\6\20\0\18\a\0\0B\2\5\1K\0\1\0/<cmd>lua require('vi-viz').vizAppend()<CR>\aaa/<cmd>lua require('vi-viz').vizInsert()<CR>\aii/<cmd>lua require('vi-viz').vizChange()<CR>\6c0<cmd>lua require('vi-viz').vizPattern()<CR>\6r5<cmd>lua require('vi-viz').vizContract1Chr()<CR>\6-3<cmd>lua require('vi-viz').vizExpand1Chr()<CR>\6=1<cmd>lua require('vi-viz').vizContract()<CR>\v<left>/<cmd>lua require('vi-viz').vizExpand()<CR>\6x-<cmd>lua require('vi-viz').vizInit()<CR>\f<right>\6n\0\1\0\1\vsilent\2\0" },
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/vi-viz",
    url = "https://github.com/olambo/vi-viz"
  },
  ["vim-tmux-runner"] = {
    loaded = false,
    needs_bufread = false,
    only_cond = false,
    path = "/home/ice/.local/share/nvim/site/pack/packer/opt/vim-tmux-runner",
    url = "https://github.com/christoomey/vim-tmux-runner"
  }
}

time([[Defining packer_plugins]], false)
-- Config for: lualine.nvim
time([[Config for lualine.nvim]], true)
try_loadstring("\27LJ\2\n/\0\0\3\0\2\0\0046\0\0\0'\2\1\0B\0\2\1K\0\1\0\20packers.lualine\frequire\0", "config", "lualine.nvim")
time([[Config for lualine.nvim]], false)
-- Config for: telescope.nvim
time([[Config for telescope.nvim]], true)
try_loadstring("\27LJ\2\n1\0\0\3\0\2\0\0046\0\0\0'\2\1\0B\0\2\1K\0\1\0\22packers.telescope\frequire\0", "config", "telescope.nvim")
time([[Config for telescope.nvim]], false)

-- Command lazy-loads
time([[Defining lazy-load commands]], true)
pcall(vim.cmd, [[command -nargs=* -range -bang -complete=file Trouble lua require("packer.load")({'trouble.nvim'}, { cmd = "Trouble", l1 = <line1>, l2 = <line2>, bang = <q-bang>, args = <q-args>, mods = "<mods>" }, _G.packer_plugins)]])
pcall(vim.cmd, [[command -nargs=* -range -bang -complete=file TroubleToggle lua require("packer.load")({'trouble.nvim'}, { cmd = "TroubleToggle", l1 = <line1>, l2 = <line2>, bang = <q-bang>, args = <q-args>, mods = "<mods>" }, _G.packer_plugins)]])
time([[Defining lazy-load commands]], false)

vim.cmd [[augroup packer_load_aucmds]]
vim.cmd [[au!]]
  -- Filetype lazy-loads
time([[Defining lazy-load filetype autocommands]], true)
vim.cmd [[au FileType typescript ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "typescript" }, _G.packer_plugins)]]
vim.cmd [[au FileType css ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "css" }, _G.packer_plugins)]]
vim.cmd [[au FileType dockerfile ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "dockerfile" }, _G.packer_plugins)]]
vim.cmd [[au FileType vue ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "vue" }, _G.packer_plugins)]]
vim.cmd [[au FileType json ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "json" }, _G.packer_plugins)]]
vim.cmd [[au FileType vlang ++once lua require("packer.load")({'v-vim', 'nvim-lspconfig'}, { ft = "vlang" }, _G.packer_plugins)]]
vim.cmd [[au FileType typescriptreact ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "typescriptreact" }, _G.packer_plugins)]]
vim.cmd [[au FileType sh ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "sh" }, _G.packer_plugins)]]
vim.cmd [[au FileType rust ++once lua require("packer.load")({'nvim-colorizer.lua', 'nvim-lspconfig'}, { ft = "rust" }, _G.packer_plugins)]]
vim.cmd [[au FileType javascriptreact ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "javascriptreact" }, _G.packer_plugins)]]
vim.cmd [[au FileType ion ++once lua require("packer.load")({'elvish.vim', 'ion-vim'}, { ft = "ion" }, _G.packer_plugins)]]
vim.cmd [[au FileType python ++once lua require("packer.load")({'nvim-colorizer.lua', 'nvim-lspconfig'}, { ft = "python" }, _G.packer_plugins)]]
vim.cmd [[au FileType lua ++once lua require("packer.load")({'nvim-colorizer.lua', 'nvim-lspconfig'}, { ft = "lua" }, _G.packer_plugins)]]
vim.cmd [[au FileType javascript ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "javascript" }, _G.packer_plugins)]]
vim.cmd [[au FileType yaml ++once lua require("packer.load")({'nvim-lspconfig'}, { ft = "yaml" }, _G.packer_plugins)]]
time([[Defining lazy-load filetype autocommands]], false)
  -- Event lazy-loads
time([[Defining lazy-load event autocommands]], true)
vim.cmd [[au BufEnter * ++once lua require("packer.load")({'elvish.vim', 'cokeline.nvim', 'v-vim', 'lightspeed.nvim', 'ion-vim', 'indent-blankline.nvim'}, { event = "BufEnter *" }, _G.packer_plugins)]]
vim.cmd [[au BufReadPre * ++once lua require("packer.load")({'symbols-outline.nvim'}, { event = "BufReadPre *" }, _G.packer_plugins)]]
vim.cmd [[au BufRead * ++once lua require("packer.load")({'nvim-treesitter-refactor', 'nvim-ts-autotag', 'nvim-comment', 'nvim-colorizer.lua', 'nvim-treesitter', 'vim-tmux-runner', 'treesitter-unit', 'vi-viz', 'nvim-ts-rainbow', 'nvim-ts-context-commentstring', 'gruvbox-flat.nvim', 'gitsigns.nvim', 'fm-nvim', 'nvim-treesitter-context'}, { event = "BufRead *" }, _G.packer_plugins)]]
vim.cmd [[au CursorHold * ++once lua require("packer.load")({'trouble.nvim', 'nvim-lspconfig'}, { event = "CursorHold *" }, _G.packer_plugins)]]
vim.cmd [[au InsertEnter * ++once lua require("packer.load")({'nvim-autopairs', 'lspkind-nvim', 'friendly-snippets'}, { event = "InsertEnter *" }, _G.packer_plugins)]]
vim.cmd [[au BufNewFile * ++once lua require("packer.load")({'nvim-treesitter-refactor', 'nvim-ts-autotag', 'nvim-treesitter', 'treesitter-unit', 'nvim-ts-rainbow', 'nvim-ts-context-commentstring', 'nvim-treesitter-context'}, { event = "BufNewFile *" }, _G.packer_plugins)]]
vim.cmd [[au FocusLost * ++once lua require("packer.load")({'trouble.nvim', 'nvim-lspconfig'}, { event = "FocusLost *" }, _G.packer_plugins)]]
time([[Defining lazy-load event autocommands]], false)
vim.cmd("augroup END")
vim.cmd [[augroup filetypedetect]]
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/elvish.vim/ftdetect/elvish.vim]], true)
vim.cmd [[source /home/ice/.local/share/nvim/site/pack/packer/opt/elvish.vim/ftdetect/elvish.vim]]
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/elvish.vim/ftdetect/elvish.vim]], false)
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/v-vim/ftdetect/vlang.vim]], true)
vim.cmd [[source /home/ice/.local/share/nvim/site/pack/packer/opt/v-vim/ftdetect/vlang.vim]]
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/v-vim/ftdetect/vlang.vim]], false)
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/ion-vim/ftdetect/ion.vim]], true)
vim.cmd [[source /home/ice/.local/share/nvim/site/pack/packer/opt/ion-vim/ftdetect/ion.vim]]
time([[Sourcing ftdetect script at: /home/ice/.local/share/nvim/site/pack/packer/opt/ion-vim/ftdetect/ion.vim]], false)
vim.cmd("augroup END")
if should_profile then save_profiles(1) end

end)

if not no_errors then
  vim.api.nvim_command('echohl ErrorMsg | echom "Error in packer_compiled: '..error_msg..'" | echom "Please check your config for correctness" | echohl None')
end
