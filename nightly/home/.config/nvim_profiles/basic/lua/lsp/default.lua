-- define signs
local sign_define = vim.fn.sign_define
sign_define("DiagnosticSignError", { text = "🔮", texthl = "DiagnosticSignError" })
sign_define("DiagnosticSignWarn", { text = "🍯", texthl = "DiagnosticSignWarn" })
sign_define("DiagnosticSignInfo", { text = "🌵", texthl = "DiagnosticSignInfo" })
sign_define("DiagnosticSignHint", { text = "🌵", texthl = "DiagnosticSignHint" })
-- sign_define("DiagnosticSignError", { text = "", texthl = "DiagnosticSignError" })
-- sign_define("DiagnosticSignWarn", { text = "", texthl = "DiagnosticSignWarn" })
-- sign_define("DiagnosticSignInfo", { text = "", texthl = "DiagnosticSignInfo" })
-- sign_define("DiagnosticSignHint", { text = "", texthl = "DiagnosticSignHint" })

function _G.ShowLSPSettings()
	print(vim.inspect(vim.lsp.buf_get_clients()))
end
function _G.ReloadLSPSettings()
	vim.lsp.stop_client(vim.lsp.get_active_clients())
	vim.cmd([[edit]])
end

vim.api.nvim_exec(
	[[
        hi LspBorderTop guifg=#5d9794 guibg=#2e3440
        hi LspBorderLeft guifg=#5d9794 guibg=#3b4252
        hi LspBorderRight guifg=#5d9794 guibg=#3b4252
        hi LspBorderBottom guifg=#5d9794 guibg=#2e3440
      ]],
	false
)
