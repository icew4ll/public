require("null")
require("default")
local m = require("mappy")
local vls_root_path = vim.loop.os_homedir() .. "/bin/build/vls"
local vls_binary = vls_root_path .. "/bin/vls"

local border = {
	{ "⣀", "LspBorderTop" },
	{ "⣀", "LspBorderTop" },
	{ "⣀", "LspBorderTop" },
	{ "⢸", "LspBorderRight" },
	{ "⠉", "LspBorderBottom" },
	{ "⠉", "LspBorderBottom" },
	{ "⠉", "LspBorderBottom" },
	{ "⡇", "LspBorderLeft" },
}

local lsp_on_attach = function(diag_maps_only)
	return function(client, bufnr)
		print(("LSP started: bufnr = %d"):format(bufnr))
		--require'completion'.on_attach()

		if client.config.flags then
			client.config.flags.allow_incremental_sync = true
		end

		vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")

		-- ignore errors when executed multi times
		m.add_buffer_maps(function()
			m.bind("n", { "<A-J>", "<A-S-Ô>" }, function()
				vim.lsp.diagnostic.goto_next({
					popup_opts = { border = border },
				})
			end)
			m.bind("n", { "<A-K>", "<A-S->" }, function()
				vim.lsp.diagnostic.goto_prev({
					popup_opts = { border = border },
				})
			end)
			m.nnoremap("<Space>E", function()
				if vim.b.lsp_diagnostics_disabled then
					vim.lsp.diagnostic.enable()
				else
					vim.lsp.diagnostic.disable()
				end
				vim.b.lsp_diagnostics_disabled = not vim.b.lsp_diagnostics_disabled
			end)
			m.nnoremap("<Space>e", function()
				vim.lsp.diagnostic.show_line_diagnostics({ border = border })
			end)
			m.nnoremap("<Space>q", vim.lsp.diagnostic.set_loclist)
			if not diag_maps_only then
				m.nnoremap("K", vim.lsp.buf.hover)
				m.nnoremap("1gD", vim.lsp.buf.type_definition)
				if vim.opt.filetype:get() ~= "help" then
					m.nnoremap("<C-]>", vim.lsp.buf.definition)
					m.nnoremap("<C-w><C-]>", function()
						vim.cmd([[split]])
						vim.lsp.buf.definition()
					end)
				end
				-- m.nnoremap("<C-x><C-k>", vim.lsp.buf.signature_help)
				m.nnoremap("g0", vim.lsp.buf.document_symbol)
				m.nnoremap("g=", vim.lsp.buf.formatting)
				m.nnoremap("gA", vim.lsp.buf.code_action)
				m.nnoremap("gD", vim.lsp.buf.implementation)
				m.nnoremap("gR", vim.lsp.buf.rename)
				m.nnoremap("gW", vim.lsp.buf.workspace_symbol)
				m.nnoremap("gd", vim.lsp.buf.declaration)
				m.nnoremap("gli", vim.lsp.buf.incoming_calls)
				m.nnoremap("glo", vim.lsp.buf.outgoing_calls)
				m.nnoremap("gr", vim.lsp.buf.references)
			end
		end)
	end
end

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
	underline = true,
	virtual_text = true,
	signs = true,
})
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = border })
vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, { border = border })

local lsp = require("lspconfig")
local util = require("lspconfig.util")

local function is_git_root(p)
	return util.path.is_dir(p) and util.path.exists(util.path.join(p, ".git"))
end
local function is_deno_dir(p)
	local base = p:gsub([[.*/]], "")
	for _, r in ipairs({
		[[^deno]],
		[[^ddc]],
		[[^cmp%-look$]],
		[[^neco%-vim$]],
		[[^git%-vines$]],
		[[^murus$]],
		[[^skkeleton$]],
	}) do
		if base:match(r) then
			return true
		end
	end
	return false
end

for name, config in pairs({
	-- clangd = { on_attach = lsp_on_attach() },
	-- cssls = { on_attach = lsp_on_attach() },
	-- dockerls = { on_attach = lsp_on_attach() },
	-- html = { on_attach = lsp_on_attach() },
	-- intelephense = { on_attach = lsp_on_attach() },
	--jsonls = {on_attach = lsp_on_attach()},
	--perlls = {on_attach = lsp_on_attach()},
	-- pyright = { on_attach = lsp_on_attach() },
	-- solargraph = { on_attach = lsp_on_attach() },
	-- sourcekit = { on_attach = lsp_on_attach() },
	-- terraformls = { on_attach = lsp_on_attach() },
	-- vimls = { on_attach = lsp_on_attach() },
	-- yamlls = { on_attach = lsp_on_attach() },
	pyright = { on_attach = lsp_on_attach() },
	vls = { cmd = { vls_binary }, on_attach = lsp_on_attach() },
	["null-ls"] = { on_attach = lsp_on_attach() },

	sumneko_lua = (function()
		local sumneko_root_path = vim.loop.os_homedir() .. "/bin/build/lua-language-server"
		local sumneko_binary = ("%s/bin/%s/lua-language-server"):format(
			sumneko_root_path,
			vim.loop.os_uname().sysname == "Darwin" and "macOS" or "Linux"
		)
		return {
			on_attach = lsp_on_attach(),
			cmd = { sumneko_binary, "-E", sumneko_root_path .. "/main.lua" },
			settings = {
				Lua = {
					runtime = {
						version = "LuaJIT",
						path = vim.split(package.path, ";"),
					},
					completion = {
						keywordSnippet = "Disable",
					},
					diagnostics = {
						enable = true,
						globals = {
							"vim",
							"describe",
							"it",
							"before_each",
							"after_each",
							"packer_plugins",
							"hs",
						},
					},
					workspace = {
						library = {
							library = vim.api.nvim_get_runtime_file("", true),
							checkThirdParty = false,
						},
					},
					telemetry = { enable = false },
				},
			},
		}
	end)(),
}) do
	lsp[name].setup(config)
end
