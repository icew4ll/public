return {
	{
		"hrsh7th/nvim-cmp",
		requires = {
			{ "hrsh7th/cmp-nvim-lsp", after = "nvim-cmp" },
			{ "saadparwaiz1/cmp_luasnip", after = "cmp-nvim-lsp" },
			{ "hrsh7th/cmp-buffer", after = "cmp_luasnip" },
			{ "hrsh7th/cmp-cmdline", after = "nvim-cmp" },
			{ "hrsh7th/cmp-nvim-lua", after = "cmp-buffer" },
			{ "hrsh7th/cmp-path", after = "cmp-nvim-lua" },
			{ "hrsh7th/cmp-emoji", after = "cmp-path" },
			{
				"andersevenrud/compe-tmux",
				branch = "cmp",
				after = "cmp-emoji",
			},
			{
				"windwp/nvim-autopairs",
				config = function()
					require("nvim-autopairs").setup({
						disable_filetype = { "TelescopePrompt", "vim" },
					})
					local cmp_autopairs = require("nvim-autopairs.completion.cmp")
					local cmp = require("cmp")
					cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))
				end,
				after = "compe-tmux",
			},
		},
		after = "lspkind-nvim",
		config = function()
			local cmp = require("cmp")

			local has_words_before = function()
				local line, col = unpack(vim.api.nvim_win_get_cursor(0))
				return col ~= 0
					and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
			end

			cmp.setup({
				formatting = {
					format = require("lspkind").cmp_format({
						with_text = true,
						menu = {
							buffer = "🔮",
							nvim_lsp = "🏹",
							nvim_lua = "🌙",
							path = "🧭",
							luasnip = "📜",
							tmux = "💻",
							emoji = "🍵",
							-- treesitter = "[tree]",
							-- look = "[Look]",
							-- spell = "[Spell]",
							-- calc = "[Calc]",
							-- emoji = "[Emoji]",
							-- rg = "[RG]",
							-- ultisnips = "[UltiSnips]",
							-- cmp_tabnine = "[TabNine]",
							-- neorg = "[Neorg]",
						},
					}),
				},
				mapping = {
					["<C-n>"] = cmp.mapping.select_next_item(),
					["<C-p>"] = cmp.mapping.select_prev_item(),
					["<C-d>"] = cmp.mapping(cmp.mapping.scroll_docs(-4), { "i", "c" }),
					["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(4), { "i", "c" }),
					["<CR>"] = cmp.mapping.confirm({ select = true }),
					["<Tab>"] = cmp.mapping(function(fallback)
						if cmp.visible() then
							cmp.select_next_item()
						elseif require("luasnip").expand_or_jumpable() then
							vim.fn.feedkeys(
								vim.api.nvim_replace_termcodes("<Plug>luasnip-expand-or-jump", true, true, true),
								""
							)
						elseif has_words_before() then
							cmp.complete()
						else
							fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
						end
					end, {
						"i",
						"s",
					}),

					["<S-Tab>"] = cmp.mapping(function()
						if cmp.visible() then
							cmp.select_prev_item()
						elseif require("luasnip").jumpable(-1) then
							vim.fn.feedkeys(
								vim.api.nvim_replace_termcodes("<Plug>luasnip-jump-prev", true, true, true),
								""
							)
						end
					end, {
						"i",
						"s",
					}),
				},
				snippet = {
					expand = function(args)
						-- vim.fn["UltiSnips#Anon"](args.body)
						require("luasnip").lsp_expand(args.body) -- For `luasnip` users.
					end,
				},
				sources = {
					-- { name = "buffer", keyword_length = 3 },
					{ name = "buffer" },
					{ name = "nvim_lsp" },
					{ name = "nvim_lua" },
					{ name = "path" },
					{ name = "luasnip" }, -- For luasnip users.
					{ name = "tmux" },
					{ name = "emoji" },
					-- { name = "calc" },
					-- { name = "spell" },
					-- { name = "emoji" },
					-- { name = "rg" },
					-- { name = "look", keyword_length = 2, opts = { convert_case = true, loud = true } },
					-- { name = "look" },
					-- { name = "neorg" },
					-- { name = "crates" },
					-- { name = "ultisnips" },
					-- { name = "cmp_tabnine" },
				},
				completion = { completeopt = "menu,menuone,noinsert" },
				experimental = { native_menu = false, ghost_text = false },
			})

			-- If you want insert `(` after select function or method item
			local cmp_autopairs = require("nvim-autopairs.completion.cmp")
			cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done({ map_char = { tex = "" } }))

			-- TabNine
			-- local tabnine = require("cmp_tabnine.config")
			-- tabnine:setup({ max_lines = 1000, max_num_results = 20, sort = true })

			-- Use cmdline & path source for ':'.
			cmp.setup.cmdline(":", {
				sources = cmp.config.sources({
					{ name = "path" },
				}, {
					{ name = "cmdline" },
				}),
			})

			-- lsp_document_symbols
			cmp.setup.cmdline("/", {
				sources = cmp.config.sources({
					{ name = "nvim_lsp_document_symbol" },
				}, {
					{ name = "buffer" },
				}),
			})
		end,
	},
	{
		"onsails/lspkind-nvim",
		event = "InsertEnter",
	},
	{
		"L3MON4D3/LuaSnip",
		after = "nvim-cmp",
		requires = {
			"rafamadriz/friendly-snippets",
		},
		config = function()
			local present, luasnip = pcall(require, "luasnip")
			if not present then
				return
			end

			luasnip.config.set_config({
				history = true,
				updateevents = "TextChanged,TextChangedI",
			})

			require("luasnip/loaders/from_vscode").load()
			require("packers.snips")
		end,
	},
}

