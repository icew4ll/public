local ls = require("luasnip")
local s = ls.s
local p = require("luasnip.extras").partial
local t = ls.text_node
local i = ls.insert_node
-- local f = ls.function_node

ls.snippets = {
	all = {
		s({ trig = "ymd", name = "Current date", dscr = "Insert the current date" }, {
			p(os.date, "%Y-%m-%d"),
		}),
	},
	python = {
		s({ trig = "doc", name = "doc", dscr = "doc" }, {
			t({ '"""', "" }),
			i(1),
			t({ "", '"""', "", "" }),
			i(0),
		}),
		s({ trig = "init", name = "Init", dscr = "Init" }, {
			t({ '"""', "" }),
			i(1),
			t({ "", '"""', "", "", "" }),
			t({ "def main():", "\t\t" }),
			t({ '"""main"""', "\t\t" }),
			i(2),
			t({ "", "", "", "" }),
			t({ 'if __name__ == "__main__":', "\t\t" }),
			i(0),
			t("main()"),
		}),
	},
	org = {
		s({ trig = "lk", name = "Link", dscr = "Link" }, {
			t("[["),
			i(1),
			t("]["),
			i(2),
			i(0),
			t("]]"),
		}),
		s({ trig = "ch", name = "Checklist", dscr = "Checklist" }, {
			t("- [ ] "),
			i(1),
			i(0),
		}),
	},
}
