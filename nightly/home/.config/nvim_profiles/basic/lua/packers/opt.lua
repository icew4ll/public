return {
	{ "wbthomason/packer.nvim", opt = true },
	{
		"vmchale/ion-vim",
		ft = "ion",
		opt = true,
		event = "BufEnter",
	},
	{
		"dmix/elvish.vim",
		ft = "ion",
		opt = true,
		event = "BufEnter",
	},
	{
		"ollykel/v-vim",
		ft = "vlang",
		opt = true,
		event = "BufEnter",
	},
	{
		"noib3/cokeline.nvim",
		event = "BufEnter",
		requires = "kyazdani42/nvim-web-devicons", -- If you want devicons
		config = function()
			local get_hex = require("cokeline/utils").get_hex

			require("cokeline").setup({
				default_hl = {
					focused = {
						fg = get_hex("ColorColumn", "bg"),
						bg = get_hex("Normal", "fg"),
					},
					unfocused = {
						fg = get_hex("Normal", "fg"),
						bg = get_hex("ColorColumn", "bg"),
					},
				},

				components = {
					{
						text = function(buffer)
							return " " .. buffer.devicon.icon
						end,
						hl = {
							fg = function(buffer)
								return buffer.devicon.color
							end,
						},
					},
					{
						text = function(buffer)
							return buffer.unique_prefix
						end,
						hl = {
							fg = get_hex("Comment", "fg"),
							style = "italic",
						},
					},
					{
						text = function(buffer)
							return buffer.filename .. " "
						end,
					},
					{
						text = "",
						delete_buffer_on_left_click = true,
					},
					{
						text = " ",
					},
				},
			})
		end,
	},
	{
		"terrortylor/nvim-comment",
		opt = true,
		event = "BufRead",
		config = function()
			require("nvim_comment").setup({
				hook = function()
					if vim.api.nvim_buf_get_option(0, "filetype") == "vue" then
						require("ts_context_commentstring.internal").update_commentstring()
					end
					if vim.api.nvim_buf_get_option(0, "filetype") == "typescriptreact" then
						require("ts_context_commentstring.internal").update_commentstring()
					end
					if vim.api.nvim_buf_get_option(0, "filetype") == "javascriptreact" then
						require("ts_context_commentstring.internal").update_commentstring()
					end
				end,
			})
		end,
	},
	{
		"eddyekofo94/gruvbox-flat.nvim",
		opt = true,
		event = "BufRead",
		config = function()
			vim.api.nvim_command("colo gruvbox-flat")
		end,
	},
	{
		"lewis6991/gitsigns.nvim",
		opt = true,
		-- event = { "FocusLost", "CursorHold" },
		event = "BufRead",
		config = function()
			require("gitsigns").setup({
				signs = {
					add = { hl = "GitSignsAdd" },
					change = { hl = "GitSignsChange" },
					delete = { hl = "GitSignsDelete", text = "✗" },
					topdelete = { hl = "GitSignsDelete", text = "↑" },
					changedelete = { hl = "GitSignsChange", text = "•" },
				},
				numhl = true,
				current_line_blame = true,
				current_line_blame_opts = {
					delay = 10,
				},
				word_diff = true,
			})
		end,
	},
	{
		"is0n/fm-nvim",
		opt = true,
		event = "BufRead",
	},
	{
		"christoomey/vim-tmux-runner",
		opt = true,
		event = "BufRead",
	},
	{
		"simrat39/symbols-outline.nvim",
		opt = true,
		event = "BufReadPre",
	},
	{
		"norcalli/nvim-colorizer.lua",
		opt = true,
		ft = { "lua", "python", "rust" },
		event = "BufRead",
		config = function()
			require("colorizer").setup({
				"*", -- enable on all files
				-- css = { rgb_fn = true },
				-- scss = { rgb_fn = true },
				-- sass = { rgb_fn = true },
				-- stylus = { rgb_fn = true },
				-- vim = { names = true },
				-- tmux = { names = false },
				-- "javascript",
				-- "javascriptreact",
				-- "typescript",
				-- "typescriptreact",
				-- "conf",
				-- lua = { rgb_fn = true },
			})
		end,
	},
	{
		"olambo/vi-viz",
		opt = true,
		event = "BufRead",
		-- event = "BufReadPre",
		config = function()
			local opts = { silent = true }

			local function map(mode, key, result, optz)
				vim.api.nvim_set_keymap(mode, key, result, {
					noremap = true,
					silent = optz.silent or false,
					expr = optz.expr or false,
					script = optz.script or false,
				})
			end
			map("n", "<right>", "<cmd>lua require('vi-viz').vizInit()<CR>", opts)
			-- expand and contract
			map("x", "<right>", "<cmd>lua require('vi-viz').vizExpand()<CR>", opts)
			map("x", "<left>", "<cmd>lua require('vi-viz').vizContract()<CR>", opts)
			-- expand and contract by 1 char either side
			map("x", "=", "<cmd>lua require('vi-viz').vizExpand1Chr()<CR>", opts)
			map("x", "-", "<cmd>lua require('vi-viz').vizContract1Chr()<CR>", opts)
			-- good use for the r key in visual mode
			map("x", "r", "<cmd>lua require('vi-viz').vizPattern()<CR>", opts)
			-- nice to have to get dot repeat on single words
			map("x", "c", "<cmd>lua require('vi-viz').vizChange()<CR>", opts)
			-- nice to have to insert before and after

			map("x", "ii", "<cmd>lua require('vi-viz').vizInsert()<CR>", opts)

			map("x", "aa", "<cmd>lua require('vi-viz').vizAppend()<CR>", opts)
		end,
	},
	-- {
	-- 	"David-Kunz/treesitter-unit",
	-- 	after = "nvim-lspconfig",
	-- opt = true,
	-- event = "BufReadPre",
	-- config = function()
	-- 	require("packers.statusline")
	-- end,
	-- },
	-- {
	-- 	"ray-x/lsp_signature.nvim",
	-- 	after = "nvim-lspconfig",
	-- opt = true,
	-- event = "BufReadPre",
	-- config = function()
	-- 	require("packers.statusline")
	-- end,
	-- },
	-- { "windwp/nvim-ts-autotag" },
	--{
	--	"famiu/feline.nvim",
	--	opt = true,
	--	event = "BufReadPre",
	--	branch = "develop",
	--	config = function()
	--		-- require("feline").setup()
	--		-- require("packers.statusline")
	--		require("packers.feline")
	--	end,
	--},
	{
		"nvim-lualine/lualine.nvim",
		branch = "master",
		-- opt = true,
		-- event = "BufReadPre",
		config = function()
			require("packers.lualine")
		end,
	},
	{
		"nvim-telescope/telescope.nvim",
		-- opt = true,
		-- event = "BufReadPre",
		requires = {
			{
				"nvim-telescope/telescope-fzf-native.nvim",
				run = "make",
			},
			{
				"nvim-telescope/telescope-media-files.nvim",
			},
		},
		config = function()
			require("packers.telescope")
		end,
	},
	{
		"folke/trouble.nvim",
		opt = true,
		-- event = "BufReadPre",
		event = { "FocusLost", "CursorHold" },
		cmd = { "TroubleToggle", "Trouble" },
		requires = "kyazdani42/nvim-web-devicons",
	},
	-- {
	-- 	"glepnir/galaxyline.nvim",
	-- 	branch = "main",
	-- 	-- opt = true,
	-- 	-- event = "BufReadPre",
	-- 	config = function()
	-- 		require("packers.galaxy")
	-- 	end,
	-- },
	{
		"lukas-reineke/indent-blankline.nvim",
		opt = true,
		event = "BufEnter",
	},
	{
		"ggandor/lightspeed.nvim",
		opt = true,
		event = "BufEnter",
	},
}
