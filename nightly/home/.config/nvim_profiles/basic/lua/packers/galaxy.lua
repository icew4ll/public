local galaxyline = require("galaxyline")
local condition = require("galaxyline.condition")

local icons = {
	sep = {
		-- left = "",
		-- right = "",
		-- left = "⚙️",
		-- right = "⚙️",
		left = " ",
		right = " ",
		space = " ",
	},
	diagnostic = {
		error = "🔮 ",
		warn = "🍯 ",
		info = "🌵 ",
	},
	diff = {
		add = " ",
		modified = " ",
		remove = " ",
	},
	ro = "🔐",
	edit = "🪶",
	-- git = ""
	-- git = " ",
	git = "🌱 ",
	-- lsp = "🖼️"
	lsp = "📖",
	-- lsp = "🦉"
	-- lsp = "⚡️"
}

local colors = {
	base00 = "#32302f",
	base01 = "#cdd3de",
	base02 = "#a4b063",
	base03 = "#a7adba",
	base04 = "#65737e",
	base05 = "#4f5b66",
	base06 = "#343d46",
	base07 = "#1b2b34",
	red = "#b40b11",
	orange = "#b4713d",
	yellow = "#a48c32",
	green = "#869235",
	cyan = "#5b9c90",
	blue = "#526f93",
	purple = "#896a98",
	brown = "#9a806d",
	white = "#ffffff",
	light01 = "#E8EBF0",
}

colors.bg_active = colors.base00

-- shorten lines for these filetypes
galaxyline.short_line_list = {
	"LuaTree",
	"floaterm",
	"dbui",
	"startify",
	"term",
	"nerdtree",
	"fugitive",
	"fugitiveblame",
}

local modes = {
	n = { "NORMAL", colors.green },
	v = { "VISUAL", colors.pink },
	V = { "V-LINE", colors.pink },
	[""] = { "V-BLOCK", colors.pink },
	s = { "SELECT", colors.orange },
	S = { "S-LINE", colors.orange },
	[""] = { "S-BLOCK", colors.orange },
	i = { "INSERT", colors.soil },
	R = { "REPLACE", colors.red },
	c = { "COMMAND", colors.blue },
	r = { "PROMPT", colors.brown },
	["!"] = { "EXTERNAL", colors.purple },
	t = { "TERMINAL", colors.purple },
}

local get_vim_mode_style = function()
	local vim_mode = vim.fn.mode()
	return modes[vim_mode]
end

local get_filename = function()
	return vim.fn.expand("%:h:t") .. "/" .. vim.fn.expand("%:t")
end

local file_readonly = function(readonly_icon)
	if vim.bo.filetype == "help" then
		return ""
	end
	local icon = readonly_icon or icons.ro
	if vim.bo.readonly == true then
		return " " .. icon .. " "
	end
	return ""
end

local current_file_name_provider = function()
	local file = get_filename()
	if vim.fn.empty(file) == 1 then
		return ""
	end
	if string.len(file_readonly()) ~= 0 then
		return file .. file_readonly()
	end
	local icon = icons.edit
	if vim.bo.modifiable then
		if vim.bo.modified then
			return file .. " " .. icon .. "  "
		end
	end
	return file .. " "
end

local sectionCount = {
	left = 0,
	mid = 0,
	right = 0,
	short_line_left = 0,
	short_line_right = 0,
}

local nextSectionNum = function(sectionKind)
	local num = sectionCount[sectionKind] + 1
	sectionCount[sectionKind] = num
	return num
end

local addSection = function(sectionKind, section)
	local num = nextSectionNum(sectionKind)
	local id = sectionKind .. "_" .. num .. "_" .. section.name
	-- note: this is needed since id's get mapped to highlights name `Galaxy<Id>`
	if section.useNameAsId == true then
		id = section.name
	end
	galaxyline.section[sectionKind][num] = {
		[id] = section,
	}
end

local addSections = function(sectionKind, sections)
	for _, section in pairs(sections) do
		addSection(sectionKind, section)
	end
end

local string_provider = function(str)
	return function()
		return str
	end
end

local createSpaceSection = function(color)
	return {
		name = "whitespace",
		provider = string_provider(" "),
		highlight = { color, color },
	}
end

addSections("left", {
	{
		name = "leftCap",
		provider = string_provider(icons.sep.left),
		condition = condition.buffer_not_empty,
		highlight = { colors.soil, colors.bg_active },
	},
	{
		name = "fileIcon",
		provider = { "FileIcon" },
		condition = condition.buffer_not_empty,
		highlight = { colors.base01, "bold" },
		separator = "",
		separator_highlight = { colors.base07, colors.base02 },
	},
	{
		name = "fileName",
		provider = current_file_name_provider,
		condition = condition.buffer_not_empty,
		highlight = { colors.base01 },
		separator = "",
		separator_highlight = { colors.base07, colors.base02 },
	},
	{
		name = "gitBranch",
		icon = icons.git,
		provider = "GitBranch",
		-- condition = function()
		--     local remainingWidth = vim.fn.winwidth(0) - get_filename():len()
		--     return (remainingWidth >= 83) and condition.check_git_workspace()
		-- end,
		highlight = { colors.base01 },
		separator = " ",
		separator_highlight = { colors.base07 },
	},
	{
		name = "diagnostic",
		provider = "DiagnosticWarn",
		icon = icons.sep.space .. icons.diagnostic.warn,
		condition = condition.check_active_lsp,
		highlight = { colors.light01 },
	},
	{
		name = "diagnostic",
		provider = "DiagnosticInfo",
		icon = icons.sep.space .. icons.diagnostic.info,
		condition = condition.check_active_lsp,
		highlight = { colors.light01 },
	},
	{
		name = "diagnostic",
		provider = "DiagnosticError",
		icon = icons.sep.space .. icons.diagnostic.error,
		condition = condition.check_active_lsp,
		highlight = { colors.light01 },
	},
	{
		name = "lsp_status",
		provider = string_provider(icons.lsp),
		condition = condition.check_active_lsp,
		highlight = { colors.base07 },
		separator = "",
		separator_highlight = { colors.base07 },
	},
	{
		name = "fileRightCap",
		provider = string_provider(icons.sep.right),
		condition = condition.buffer_not_empty,
		highlight = { colors.soil, colors.bg_active },
	},
	createSpaceSection(colors.bg_active),
})

addSections("right", {
	{
		name = "leftCap",
		provider = string_provider(icons.sep.left),
		highlight = { colors.soil, colors.bg_active },
	},
	{
		name = "ViMode",
		useNameAsId = true,
		provider = function()
			-- auto change color according the vim mode
			local modeStyle = get_vim_mode_style()
			vim.api.nvim_command("hi GalaxyViMode guibg=" .. modeStyle[2])
			return icons.sep.space .. modeStyle[1] .. icons.sep.space
		end,
		highlight = { colors.light01, colors.base02, "bold" },
	},
	{
		icon = " ",
		name = "linePercent",
		provider = "LinePercent",
		condition = condition.buffer_not_empty,
		highlight = { colors.light01, colors.soil },
	},
	{
		name = "fileRightCap",
		provider = string_provider(icons.sep.right),
		condition = condition.buffer_not_empty,
		highlight = { colors.soil, colors.bg_active },
	},
})
