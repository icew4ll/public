local lsp = require("feline.providers.lsp")
local colors = {
	-- base00 = "#32302f",
	-- base00 = "#2e3440",
	-- base00 = "#212121",
	base00 = "#282c34",
	base01 = "#cdd3de",
	base02 = "#a4b063",
	base03 = "#1c1c1c",
	base04 = "#565c64",
	base05 = "#c8ccd4",
	base06 = "#32302f",
	base07 = "#30343c",
	base08 = "#23272f",
	pink = "#b686ae",
	warn = "#f9929b",
	success = "#A3BE8C",
	error = "#BF616A",
	red = "#b40b11",
	orange = "#b4713d",
	soil = "#6b4e44",
	green = "#4a7533",
	cyan = "#5b9c90",
	blue = "#526f93",
	gold = "#fdbb12",
	purple = "#896a98",
	brown = "#9a806d",
	teal = "#9a806d",
	white = "#ffffff",
	light01 = "#E8EBF0",
}

local icon_styles = {
	default = {
		left = "",
		right = " ",
		main_icon = "  ",
		vi_mode_icon = " ",
		position_icon = " ",
	},
	arrow = {
		left = "",
		right = "",
		main_icon = "  ",
		vi_mode_icon = " ",
		position_icon = " ",
	},

	block = {
		left = " ",
		right = " ",
		main_icon = "   ",
		vi_mode_icon = "  ",
		position_icon = "  ",
	},

	round = {
		left = "",
		right = "",
		main_icon = "  ",
		vi_mode_icon = " ",
		position_icon = " ",
	},

	slant = {
		left = " ",
		right = " ",
		main_icon = "  ",
		vi_mode_icon = " ",
		position_icon = " ",
	},
}

-- local config = require("core.utils").load_config().plugins.options.statusline
-- statusline style
-- local user_statusline_style = config.style
local user_statusline_style = "default"
local statusline_style = icon_styles[user_statusline_style]
-- if show short statusline on small screens
-- local shortline = config.shortline == false and true
local shortline = true

-- Initialize the components table
local components = {
	active = {},
	inactive = {},
}

-- Initialize left, mid and right
table.insert(components.active, {})
table.insert(components.active, {})
table.insert(components.active, {})

components.active[1][1] = {
	provider = statusline_style.main_icon,

	hl = {
		fg = colors.base00,
		bg = colors.base05,
	},

	right_sep = { str = statusline_style.right, hl = {
		fg = colors.base05,
		bg = colors.lightbg,
	} },
}

components.active[1][2] = {
	provider = function()
		local filename = vim.fn.expand("%:t")
		local extension = vim.fn.expand("%:e")
		local icon = require("nvim-web-devicons").get_icon(filename, extension)
		if icon == nil then
			icon = " "
			return icon
		end
		return " " .. icon .. " " .. filename .. " "
	end,
	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 70
	end,
	hl = {
		fg = colors.white,
		bg = colors.lightbg,
	},

	right_sep = { str = statusline_style.right, hl = { fg = colors.lightbg, bg = colors.lightbg2 } },
}

components.active[1][3] = {
	provider = function()
		local dir_name = vim.fn.fnamemodify(vim.fn.getcwd(), ":t")
		return "  " .. dir_name .. " "
	end,

	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 80
	end,

	hl = {
		fg = colors.grey_fg2,
		bg = colors.lightbg2,
	},
	right_sep = {
		str = statusline_style.right,
		hi = {
			fg = colors.lightbg2,
			bg = colors.base00,
		},
	},
}

components.active[1][4] = {
	provider = "git_diff_added",
	hl = {
		fg = colors.grey_fg2,
		bg = colors.base00,
	},
	icon = " ",
}
-- diffModfified
components.active[1][5] = {
	provider = "git_diff_changed",
	hl = {
		fg = colors.grey_fg2,
		bg = colors.base00,
	},
	icon = "   ",
}
-- diffRemove
components.active[1][6] = {
	provider = "git_diff_removed",
	hl = {
		fg = colors.grey_fg2,
		bg = colors.base00,
	},
	icon = "  ",
}

components.active[1][7] = {
	provider = "diagnostic_errors",
	enabled = function()
		return lsp.diagnostics_exist("Error")
	end,

	hl = { fg = colors.red },
	icon = "  ",
}

components.active[1][8] = {
	provider = "diagnostic_warnings",
	enabled = function()
		return lsp.diagnostics_exist("Warning")
	end,
	hl = { fg = colors.yellow },
	icon = "  ",
}

components.active[1][9] = {
	provider = "diagnostic_hints",
	enabled = function()
		return lsp.diagnostics_exist("Hint")
	end,
	hl = { fg = colors.grey_fg2 },
	icon = "  ",
}

components.active[1][10] = {
	provider = "diagnostic_info",
	enabled = function()
		return lsp.diagnostics_exist("Information")
	end,
	hl = { fg = colors.green },
	icon = "  ",
}

components.active[2][1] = {
	provider = function()
		local Lsp = vim.lsp.util.get_progress_messages()[1]
		if Lsp then
			local msg = Lsp.message or ""
			local percentage = Lsp.percentage or 0
			local title = Lsp.title or ""
			local spinners = {
				"",
				"",
				"",
			}

			local success_icon = {
				"",
				"",
				"",
			}

			local ms = vim.loop.hrtime() / 1000000
			local frame = math.floor(ms / 120) % #spinners

			if percentage >= 70 then
				return string.format(" %%<%s %s %s (%s%%%%) ", success_icon[frame + 1], title, msg, percentage)
			else
				return string.format(" %%<%s %s %s (%s%%%%) ", spinners[frame + 1], title, msg, percentage)
			end
		end
		return ""
	end,
	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 80
	end,
	hl = { fg = colors.green },
}

components.active[3][1] = {
	provider = function()
		if next(vim.lsp.buf_get_clients()) ~= nil then
			return "  LSP"
		else
			return ""
		end
	end,
	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 70
	end,
	hl = { fg = colors.grey_fg2, bg = colors.base00 },
}

components.active[3][2] = {
	provider = "git_branch",
	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 70
	end,
	hl = {
		fg = colors.grey_fg2,
		bg = colors.base00,
	},
	icon = "  ",
}

components.active[3][3] = {
	provider = " " .. statusline_style.left,
	hl = {
		fg = colors.base02,
		bg = colors.base00,
	},
}

local mode_colors = {
	["n"] = { "NORMAL", colors.red },
	["no"] = { "N-PENDING", colors.red },
	["i"] = { "INSERT", colors.purple },
	["ic"] = { "INSERT", colors.purple },
	["t"] = { "TERMINAL", colors.green },
	["v"] = { "VISUAL", colors.cyan },
	["V"] = { "V-LINE", colors.cyan },
	[""] = { "V-BLOCK", colors.cyan },
	["R"] = { "REPLACE", colors.orange },
	["Rv"] = { "V-REPLACE", colors.orange },
	["s"] = { "SELECT", colors.base05 },
	["S"] = { "S-LINE", colors.base05 },
	[""] = { "S-BLOCK", colors.base05 },
	["c"] = { "COMMAND", colors.pink },
	["cv"] = { "COMMAND", colors.pink },
	["ce"] = { "COMMAND", colors.pink },
	["r"] = { "PROMPT", colors.teal },
	["rm"] = { "MORE", colors.teal },
	["r?"] = { "CONFIRM", colors.teal },
	["!"] = { "SHELL", colors.green },
}

local chad_mode_hl = function()
	return {
		fg = mode_colors[vim.fn.mode()][2],
		bg = colors.base01,
	}
end

components.active[3][4] = {
	provider = statusline_style.left,
	hl = function()
		return {
			fg = mode_colors[vim.fn.mode()][2],
			bg = colors.base02,
		}
	end,
}

components.active[3][5] = {
	provider = statusline_style.vi_mode_icon,
	hl = function()
		return {
			fg = colors.base00,
			bg = mode_colors[vim.fn.mode()][2],
		}
	end,
}

components.active[3][6] = {
	provider = function()
		return " " .. mode_colors[vim.fn.mode()][1] .. " "
	end,
	hl = chad_mode_hl,
}

components.active[3][7] = {
	provider = statusline_style.left,
	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 90
	end,
	hl = {
		fg = colors.grey,
		bg = colors.base01,
	},
}

components.active[3][8] = {
	provider = statusline_style.left,
	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 90
	end,
	hl = {
		fg = colors.green,
		bg = colors.grey,
	},
}

components.active[3][9] = {
	provider = statusline_style.position_icon,
	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 90
	end,
	hl = {
		fg = colors.black,
		bg = colors.green,
	},
}

components.active[3][10] = {
	provider = function()
		local current_line = vim.fn.line(".")
		local total_line = vim.fn.line("$")

		if current_line == 1 then
			return " Top "
		elseif current_line == vim.fn.line("$") then
			return " Bot "
		end
		local result, _ = math.modf((current_line / total_line) * 100)
		return " " .. result .. "%% "
	end,

	enabled = shortline or function(winid)
		return vim.api.nvim_win_get_width(winid) > 90
	end,

	hl = {
		fg = colors.green,
		bg = colors.base01,
	},
}

require("feline").setup({
	colors = {
		bg = colors.base00,
		fg = colors.base04,
	},
	components = components,
})
