local function undo()
	local o = vim.o
	local opt = vim.opt
	local fn = vim.fn
	local cmd = vim.cmd
	local base = vim.fn.stdpath("config")
	local backup_dir = base .. "/tmp/backup"
	local swap_dir = base .. "/tmp/swap"
	local undo_dir = base .. "/tmp/undo"

	if fn.empty(fn.glob(backup_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. backup_dir)
	end

	if fn.empty(fn.glob(swap_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. swap_dir)
	end

	if fn.empty(fn.glob(undo_dir)) > 0 then
		cmd("!mkdir -p" .. " " .. undo_dir)
	end

	opt.swapfile = false
	o.undofile = true
	o.backup = true
	o.writebackup = true
	o.backupdir = backup_dir
	o.undodir = undo_dir
end

local function prepare()
	vim.g.loaded_gzip = false
	vim.g.loaded_matchit = false
	-- vim.g.loaded_netrwPlugin = false -- needed to open files in org mode
	vim.g.loaded_tarPlugin = false
	vim.g.loaded_zipPlugin = false
	vim.g.loaded_man = false
	vim.g.loaded_2html_plugin = false
	vim.g.loaded_remote_plugins = false
	-- termguicolors must be declared for feline
	vim.opt.termguicolors = true
	-- leader must be declared for org mode
	vim.g.mapleader = " "
end

-- TODO: impatient.nvim
if not pcall(require, "impatient") then
	-- local dir = vim.fn.stdpath("data") .. "/site/pack/packer/start/impatient.nvim"
	local dir = vim.fn.stdpath("data") .. "/site/pack/cheovim/base/start/impatient.nvim"
	os.execute("git clone https://github.com/lewis6991/impatient.nvim " .. dir)
	vim.opt.runtimepath:append(dir)
	require("impatient")
end

pcall(require, "packer_compiled") -- TODO: impatient.nvim

prepare()

vim.defer_fn(function()
	require("setup")
	require("filetypes")
	require("settings")
	require("packers")
	-- require("mapper")
	require("mapping").setup()
end, 0)

undo()
