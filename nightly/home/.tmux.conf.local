# source "{repository_root}/powerline/bindings/tmux/powerline.conf"
# run-shell "powerline-daemon -q"
# source "$HOME/m/powerline/powerline/bindings/tmux/powerline.conf"
# source "$HOME/m/powerline/powerline/bindings/tmux/powerline.conf"

# COPY & PASTE
# Copy tmux buffer to X clipboard
# run -b runs a shell command in background
# bind C-w run -b "tmux show-buffer | xclip -selection clipboard -i"
# bind C-w run -b "tmux show-buffer | xclip -i"

# Paste from X clipboard into tmux; also replace newline characters with
# space characters when pasting
# bind C-y run -b "exec </dev/null; xclip -o | awk 1 ORS=' ' | tmux load-buffer - ; tmux paste-buffer"
# Same as `C-y' binding except that the newline chars are not replaced with space
# bind Y run -b "exec </dev/null; xclip -o | tmux load-buffer - ; tmux paste-buffer"
# mouse mode
set -g mouse on
# set -g set-clipboard external
# bind -T root MouseUp2Pane paste
# copy and paste next
bind-key -n -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -i -sel p -f | xclip -i -sel c'
bind-key -n -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel 'xclip -i -sel p -f | xclip -i -sel c'
# bind -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "xsel -i --clipboard"
set -g set-clipboard on

# Set bind key to reload configuration file
bind r source-file ~/.tmux.conf.local \; display ​"Reloaded!"

# Undercurl
set -g default-terminal "${TERM}"
set -as terminal-overrides ',*:Smulx=\E[4::%p1%dm'  # undercurl support
set -as terminal-overrides ',*:Setulc=\E[58::2::%p1%{65536}%/%d::%p1%{256}%/%{255}%&%d::%p1%{255}%&%d%;m'  # underscore colours - needs tmux-3.0

# prefix
set -g prefix C-o
unbind C-b
unbind C-a
bind C-o send-prefix

# alt arrow keys to switch panes
bind -n M-l select-pane -L
bind -n M-h select-pane -R
bind -n M-k select-pane -U
bind -n M-j select-pane -D

# copy mode
bind -n M-tab copy-mode
# bind -n M-enter copy-mode

# bind -n M-Left select-pane -L
# bind -n M-Right select-pane -R
# bind -n M-Up select-pane -U
# bind -n M-Down select-pane -D

# ctrl arrow keys to resize
bind -n M-Left resize-pane -L
bind -n M-Right resize-pane -R
bind -n M-Up resize-pane -U
bind -n M-Down resize-pane -D

# alt arrow keys to switch panes
bind -n M-1 select-window -t 1
bind -n M-2 select-window -t 2
bind -n M-3 select-window -t 3
bind -n M-4 select-window -t 4
bind -n M-5 select-window -t 5
bind -n M-6 select-window -t 6
bind -n M-7 select-window -t 7

# default term
#set-option -g default-terminal "screen-256color"
#set-option -ga terminal-overrides ',*-256color*:Tc'
#set -g default-terminal screen-256color
#set -ag terminal-overrides ",xterm-256color:Tc"
set-option -ga terminal-overrides ",xterm-256color:Tc"

# vi mode
# setw -g mode-keys vi
set -g status-keys vi
set -g mode-keys vi

# active monitor
# d3d0c9
# set -g window-style 'fg=colour247,bg=colour236'
# set -g window-active-style 'fg=colour250,bg=black'
# setw -g monitor-activity on
# set -g visual-activity on

test='#1c1c1c'
base='#32302f'
tmux_conf_theme_status_left='↑#{?uptime_d, #{uptime_d}d,}#{?uptime_h, #{uptime_h}h,}#{?uptime_m, #{uptime_m}m,} '
tmux_conf_theme_prefix='🐢'            # U+2328
tmux_conf_battery_status_charging='🔌 '    # U+1F50C
tmux_conf_battery_status_discharging='🔋 ' # U+1F50B
#tmux_conf_battery_bar_symbol_full='◼'
#tmux_conf_battery_bar_symbol_empty='◻'
tmux_conf_battery_bar_symbol_full='⚡️'
tmux_conf_battery_bar_symbol_empty='·'
#color: #d3d0c9;
#background-color: #3d3012;

# status line style
tmux_conf_theme_message_fg="$test"                    # black
tmux_conf_theme_message_bg="$test"                    # gray
tmux_conf_theme_message_attr='bold'

# window modes style
tmux_conf_theme_mode_fg='#d3d0c9'                       # gray
tmux_conf_theme_mode_bg="$test"                       # light gray
tmux_conf_theme_mode_attr='bold'

# status line style
tmux_conf_theme_status_fg='#8a8a8a'                     # light gray
tmux_conf_theme_status_bg="$base"                     # dark gray
tmux_conf_theme_status_attr='none'

# status line command style (<prefix> : Escape)
tmux_conf_theme_message_command_fg='#ffff00'            # yellow
tmux_conf_theme_message_command_bg="$test"            # black
tmux_conf_theme_message_command_attr='bold'

# window bell status style
tmux_conf_theme_window_status_bell_fg='#ffff00'         # yellow
tmux_conf_theme_window_status_bell_bg=$test
tmux_conf_theme_window_status_bell_attr='blink,bold'
tmux_conf_theme_window_status_current_fg=$test      # black
tmux_conf_theme_window_status_current_bg='#d3d0c9'      # light blue
tmux_conf_theme_window_status_last_fg='#d3d0c9'         # light blue
tmux_conf_theme_window_status_last_bg='default'
tmux_conf_theme_window_status_last_attr='none'

# pane borders colours:
tmux_conf_theme_pane_border='#444444'                   # gray
tmux_conf_theme_pane_active_border='#d3d0c9'            # light blue

# pane indicator colours
tmux_conf_theme_pane_indicator='#d3d0c9'                # light blue
tmux_conf_theme_pane_active_indicator='#d3d0c9'         # light blue

# status left style
tmux_conf_theme_status_left_fg="#e4e4e4,#e4e4e4,#e4e4e4"  # black, white , white
tmux_conf_theme_status_left_bg="$test,$test,$test"  # yellow, pink, white blue
tmux_conf_theme_status_left_attr='bold,none,none'

# status right style
tmux_conf_theme_status_right_fg='#8a8a8a,#e4e4e4,#e4e4e4' # light gray, white, black
tmux_conf_theme_status_right_bg="$test,$test,$test" # dark gray, red, white
tmux_conf_theme_status_right_attr='none,none,bold'
